<?php
/**
 * Plugin Name: Chubb Woocommerce
 * Plugin URI:  
 * Description: Adds chubb plugin to Woocommerce e-commerce
 * Author:      Giovanny Yánez
 * Author URI:  
 * Developer:   Giovanny Yánez
 * Version:     1.0
 *
 *
 * @package Chubb/Woocomerce_Chubb
 *
 * @author Soporte <np_p_i@hotmail.es>
 * @copyright (c) 2019 G
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Return instance of \Chubb\Woocomerce_Chubb
 *
 * @return \Chubb\ChubbPlugin\Woocomerce_Chubb
 */
function wc_chubb()
{

    load_plugin_textdomain('chubb-for-woocomerce', false, dirname(plugin_basename(__FILE__)) . '/languages/');
    add_filter('woocommerce_locate_template', 'wooAddonView', 201, 3);

    /**
     * @param $template
     * @param $templateName
     * @param $templatePath
     * @return string
     */
    function wooAddonView($template, $templateName, $templatePath)
    {
        global $woocommerce;

        $_template = $template;

        if (!$templatePath) {
            $templatePath = $woocommerce->template_url;
        }

        $pluginPath = untrailingslashit(plugin_dir_path(__FILE__)) . '/woocommerce/';

        // Look within passed path within the theme - this is priority
        $template = locate_template([
            $templatePath . $templateName,
            $templateName
        ]);

        if (!$template && file_exists($pluginPath . $templateName)) {
            $template = $pluginPath . $templateName;
        }

        if (!$template) {
            $template = $_template;
        }

        return $template;
    }


    require_once(__DIR__ . '/vendor/autoload.php');
    return \Chubb\ChubbPlugin\Woocomerce_Chubb::getInstance('1.0', __FILE__);
}

add_action('plugins_loaded', 'wc_chubb', 0);
	