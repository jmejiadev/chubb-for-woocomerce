<?php

namespace Chubb\ChubbPlugin\Constants;

/**
 * Interface Environment
 * @package Chubb\ChubbPlugin\Constants
 */

interface Environment
{
    const PROD = 'prod';

    const TEST = 'test';
}