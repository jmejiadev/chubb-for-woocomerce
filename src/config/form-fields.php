<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * @var \Chubb\ChubbPlugin\Functions $this
 */

/**
 * This file will be included into of Functions class
 * @package \Chubb;
 */
return [
    'enabled' => [
        'title' => __('Enable/Disable', 'chubb-for-woocomerce'),
        'type' => 'checkbox',
        'label' => __('Enable PlacetoPay payment method.', 'chubb-for-woocomerce'),
        'default' => 'no',
        'description' => __('Show in the Payment List as a payment option', 'chubb-for-woocomerce')
    ],
    'fill_buyer_information' => [
        'title' => __('Predicting the buyer\'s information?', 'chubb-for-woocomerce'),
        'type' => 'checkbox',
        'label' => __('Enable to preload the buyer\'s information on the PlacetoPay platform.',
            'chubb-for-woocomerce'),
        'default' => 'yes',
    ],
    'allow_to_pay_with_pending_orders' => [
        'title' => __('Allow to pay with pending orders', 'chubb-for-woocomerce'),
        'type' => 'checkbox',
        'label' => __('If it is selected, it will allow the user to pay even if he has orders in pending status.',
            'chubb-for-woocomerce'),
        'default' => 'yes',
        'description' => __('If it is disabled, it displays a message when paying if the user has a pending order',
            'chubb-for-woocomerce'),
    ],
    'title' => [
        'title' => __('Title:', 'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => __('PlacetoPay', 'chubb-for-woocomerce'),
        'description' => __('This controls the title which the user sees during checkout.',
            'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'description' => [
        'title' => __('Description:', 'chubb-for-woocomerce'),
        'type' => 'textarea',
        'default' => __('Pay securely through PlacetoPay.', 'chubb-for-woocomerce'),
        'description' => __('This controls the description which the user sees during checkout.',
            'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'login' => [
        'title' => __('Login', 'chubb-for-woocomerce'),
        'type' => 'text',
        'description' => __('Given to login by PlacetoPay', 'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'tran_key' => [
        'title' => __('Transactional Key', 'chubb-for-woocomerce'),
        'type' => 'password',
        'description' => __('Given to transactional key by PlacetoPay', 'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'enviroment_mode' => [
        'title' => __('Mode', 'chubb-for-woocomerce'),
        'type' => 'select',
        'class' => 'wc-enhanced-select',
        'default' => 'dev',
        'options' => $this->getEnvironments(),
        'description' => __('Enable the environment PlacetoPay for testing or production transactions.<br />Note: <b>By default is "Development Test", if WP_DEBUG is activated</b>',
            'chubb-for-woocomerce')
    ],
    'endpoint' => [
        'title' => __('Notification url. EndPoint (WP >= 4.6)', 'chubb-for-woocomerce'),
        'type' => 'text',
        'class' => 'readonly',
        'description' => __('Url of notification where PlacetoPay will send a notification of a transaction for Woocommerce.<br />If your Wordpress not support REST-API, please visit: https://wordpress.org/plugins/rest-api/',
            'chubb-for-woocomerce')
    ],
    'merchant_phone' => [
        'title' => __('Phone number', 'chubb-for-woocomerce'),
        'description' => __('Provide the phone number used for the inquiries or support in your shop',
            'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => '',
        'desc_tip' => true,
    ],
    'merchant_email' => [
        'title' => __('Email', 'chubb-for-woocomerce'),
        'description' => __('Provide contact email', 'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => '',
        'desc_tip' => true,
    ],
    'msg_approved' => [
        'title' => __('Message for approved transaction', 'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => __('PlacetoPay Payment Approved', 'chubb-for-woocomerce'),
        'description' => __('Message for approved transaction', 'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'msg_pending' => [
        'title' => __('Message for pending transaction', 'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => __('Payment pending', 'chubb-for-woocomerce'),
        'description' => __('Message for pending transaction', 'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'msg_cancel' => [
        'title' => __('Message for cancel transaction', 'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => __('Transaction Canceled.', 'chubb-for-woocomerce'),
        'description' => __('Message for cancel transaction', 'chubb-for-woocomerce'),
        'desc_tip' => true
    ],
    'msg_declined' => [
        'title' => __('Message for rejected transaction', 'chubb-for-woocomerce'),
        'type' => 'text',
        'default' => __('Payment rejected via PlacetoPay.', 'chubb-for-woocomerce'),
        'description' => __('Message for rejected transaction', 'chubb-for-woocomerce'),
        'desc_tip' => true
    ]
];
