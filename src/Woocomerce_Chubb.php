<?php

namespace Chubb\ChubbPlugin;

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once(ABSPATH . 'wp-settings.php');

include "Services/BrokerService/autoload.php";
include "Services/S6TransactionExternal/autoload.php";
include "Services/RegistroCivil/autoload.php";
include "Services/ExternalWSCMS/autoload.php";

use Automattic\WooCommerce\Admin\API\Data;
use Exception;
use WC_HTTPS;
use WC_Order;

/**
 * @package Chubb\ChubbPlugin;
 */
class Woocomerce_Chubb
{

    /**
     * Constant key for chubb
     * @var string
     */

    const VERSION_VALUE = "1.0.0.0";                            //This version is sent in the request

    const ACCESS_KEY_NAME = "AccessKey";                        //Name of message in the header

    const TIME_STAMP_NAME = "Timestamp";                        //Name of message in timestamp

    const SIGNATURE_NAME = "Signature";                         //Name of message in the header

    const VERSION_NAME = "Version";                             //Name of message in the header

    private $accessId;
    private $secretKey;
    private $environment;
    private $splitKey;

    private $expiration;

    private $namespace;   //This namespace is sent in the request

    //ExternalCmsWsUAT
    private $actionGetSplitInfo;
    private $actionGetMessagesByCategory;

    //ExternalS6TransactionWSUAT
    private $actionProcessTransaction;
    private $actionGetTransactionStatus;

    //registroCivil
    private $actionGetPersonInformation;

    //urls
    private $ExternalWSCMS;
    private $S6TransactionExternal;
    private $brokerservice;
    private $RegistroCivil;

    //mensajes de error
    private $msgNoRegister;
    private $msgLegalIssues;
    private $msgInvalid;

    public $splitInfo;

    private $minAge;
    private $maxAge;

    public $idChubbTitular;
    public $idChubbTitularEsposa;

    /**
     * @return int
     */
    public function getIdChubbTitular(): int
    {
        return $this->idChubbTitular;
    }

    /**
     * @param int $idChubbTitular
     */
    public function setIdChubbTitular(int $idChubbTitular)
    {
        $this->idChubbTitular = $idChubbTitular;
    }

    /**
     * @return int
     */
    public function getIdChubbTitularEsposa(): int
    {
        return $this->idChubbTitularEsposa;
    }

    /**
     * @param int $idChubbTitularEsposa
     */
    public function setIdChubbTitularEsposa(int $idChubbTitularEsposa)
    {
        $this->idChubbTitularEsposa = $idChubbTitularEsposa;
    }

    private $statusOrder;
    /**
     * Unique instance of self
     * @var Woocomerce_Chubb
     */
    private static $instance = null;


    /**
     * idChubbTitularEsposa
     *
     * @access private
     * @param string $file Filepath of main plugin file
     * @param string $version
     */
    public function __construct($version, $file)
    {
        if (!$this->checkDependencies()) {
            return null;
        }

        add_filter('plugin_action_links_' . plugin_basename($file), [$this, 'actionLinksChubb']);
        add_action('admin_menu', [$this, 'my_settings_page']);

        $this->addProduct();

        /*
         * Add chubb to cart
         *
         */

       // add_action( 'woocommerce_checkout_before_order_review', [$this, 'chubb_check_seguro'] );

        add_action('woocommerce_review_order_after_order_total', [$this, 'addDiv']);

        add_action('woocommerce_review_order_after_order_total', [$this, 'addListener']);

        //add_action( 'woocommerce_payment_complete', [$this, 'chubb_set_payment']);
        //Hooks de pagos
        //add_action( 'woocommerce_order_status_completed', [$this, 'chubb_set_payment']);
        //add_action( 'woocommerce_order_status_changed', [$this, 'chubb_set_payment']);
        add_action( 'woocommerce_order_status_processing', [$this, 'chubb_set_payment']);

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        global $wpdb;
        $nombreTabla = $wpdb->prefix . "chubb_orders";

        $created = dbDelta("
            CREATE TABLE $nombreTabla (
                ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                order_id         varchar(60) NOT NULL DEFAULT '',
                document_id      varchar(60) NOT NULL DEFAULT '',
                order_date_time timestamp                       ,
                order_vigency_date timestamp                    ,
                order_status    varchar(150) NOT NULL DEFAULT '',
                order_client    varchar(150) NOT NULL DEFAULT '',
                product_owned   varchar(150) NOT NULL DEFAULT '',
                policy_number   varchar(25 ) NOT NULL DEFAULT '',
                s6_tran_id      varchar(70)  NOT NULL DEFAULT '',
                     PRIMARY KEY (ID)
            )   CHARACTER SET utf8 COLLATE utf8_general_ci;
        "
        );

    }



    /**
     * Settings Options
     * @return void
     */
    public function initFormFields()
    {
        $this->form_fields = include(__DIR__ . '/config/form-fields.php');
       // $this->init_settings();
    }

    /**
     * Add the links to show aside of the plugin
     * @param  array $links
     * @return array
     */
    public function actionLinksChubb($links)
    {
        $customLinks = [
            'settings' => sprintf(
                '<a href="%s">%s</a>',
                admin_url('admin.php?page=chubb-for-woocomerce%2Fsrc%2FWoocomerce_Chubb.php'),
                __('Configuración', 'chubb-for-woocomerce')
            )
        ];
        return array_merge($links, $customLinks);
    }
	
	 /**
     * Method to implement a singleton pattern
     *
     * @param null $version
     * @param null $file
     * @return Woocomerce_Chubb
     */
    public static function getInstance($version = null, $file = null)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self($version, $file);
        }

        return self::$instance;
    }
    
    /**
     * Verify if woocommerce plugin is installed
     * @return bool
     */
    public function checkDependencies()
    {
        if (!function_exists('WC')) {
            add_action('admin_notices', function () {
                echo '<div class="error fade">
                    <p>
                        <strong>
                            [WooCommerce Chubb] plugin requires WooCommerce to run
                        </strong>
                    </p>
                </div>';
            });

            return false;
        }

        return true;
    }

    public function my_settings_page()
    {
        add_menu_page(
                "Chubb",
                "Chubb",
                'manage_options',
                __FILE__,
                [$this, 'chubb_my_settings_page_html'],
                'dashicons-smiley',
            51
        );

        add_submenu_page(
                __FILE__,
                'Historial',
                'Historial',
                'manage_options',
                __FILE__.'/history',
                [$this, 'chubb_order_history']
        );

        add_action('admin_init', [$this, 'register_settings_chubb']);


    }

    public function register_settings_chubb(){
        register_setting('chubb_group', 'chubb' );
    }

    public function chubb_modal() {
        ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

        <div id="ex1" class="modal">
            <iframe src="<?= get_permalink($this->getIdChubbTitular()); ?>" alt="">
            <a href="#" rel="modal:close">Close</a>
        </div>

        <div id="ex2" class="modal">
            <img src="https://miro.medium.com/max/800/1*3zTTejN8RKtodF2pkycGow.gif" alt="">
            <a href="#" rel="modal:close">Close</a>
        </div>

        <!-- Link to open the modal
        <p><a href="#ex1" rel="modal:open">Open Modal</a></p>
        -->
        <?php
    }


    public function chubb_verify_db( $value, $default ){
        if ( !isset(get_option('chubb') [$value] )){
            return $default;
        } else {
            return get_option('chubb') [$value];
        }
    }

    public function chubb_my_settings_page_html() {

        $this->accessId = $this->chubb_verify_db('accessId', '');
        $this->secretKey = $this->chubb_verify_db('secretKey', '');
        $this->ambiente = $this->chubb_verify_db('ambiente', 'test');
        $this->splitKey = $this->chubb_verify_db('splitKey', '');

        $this->expiration = $this->chubb_verify_db('expiration', 3);

        $this->getEnvironment($this->environment);

        $this->splitInfo = json_decode( file_get_contents(__DIR__."/SplitInfo/GetSplitInfo.json"),  true);

        if ($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['splitKey'] != $this->splitKey && $this->splitKey != "" ){
            $this->consumeService ("GetSplitInfo", $this->splitKey);
        }

        $this->actionGetSplitInfo = $this->chubb_verify_db('actionGetSplitInfo', 'http://tempuri.org/IExternalWSCMS/GetSplitInfo');
        $this->actionGetMessagesByCategory = $this->chubb_verify_db('actionGetMessagesByCategory', 'http://tempuri.org/IExternalWSCMS/GetMessagesByCategory');
        $this->actionGetPersonInformation = $this->chubb_verify_db('actionGetPersonInformation', 'http://tempuri.org/IEcuadorListWebserviceExternal/GetPersonInformation');
        $this->actionProcessTransaction = $this->chubb_verify_db('actionProcessTransaction', 'http://tempuri.org/IS6TransactionExternal/ProcessTransaction');
        $this->actionGetTransactionStatus = $this->chubb_verify_db('actionGetTransactionStatus', 'http://tempuri.org/IS6TransactionExternal/GetTransactionStatus');

        $this->namespace = $this->chubb_verify_db ('namespace', 'http://www.acelatinamerica.com/ACELA') ;

        $this->msgNoRegister = $this->chubb_verify_db("PersonNotFoundInCivilRegistration", 'Sólo se permite asignar el seguro a una cédula');
        $this->msgLegalIssues = $this->chubb_verify_db("PersonHasLegalRestriction", 'Usuario con restricciones legales');
        $this->msgInvalid = $this->chubb_verify_db("InvalidPersonalId", 'Documento de identidad no es correcto');

        $this->testExternalWSCMS = $this->chubb_verify_db("testExternalWSCMS", 'https://testwebservices.chubblatinamerica.com/ExternalCmsWsUAT/ExternalWSCMS.svc?singlewsdl');
        $this->testS6TransactionExternal = $this->chubb_verify_db("testS6TransactionExternal", 'https://testwebservices.chubblatinamerica.com/ExternalS6TransactionWSUAT/S6TransactionExternal.svc?singlewsdl');
        $this->testbrokerservice = $this->chubb_verify_db("testbrokerservice", 'https://s6broker-ext-uat.acegroup.com/brokerservice.svc?singlewsdl');
        $this->testRegistroCivil = $this->chubb_verify_db("testRegistroCivil", 'https://testwebservices.chubblatinamerica.com/ExternalEcuadorListWSUAT/RegistroCivil.svc?singlewsdl');
        $this->prodExternalWSCMS = $this->chubb_verify_db("prodExternalWSCMS", '');
        $this->prodS6TransactionExternal = $this->chubb_verify_db("prodS6TransactionExternal", '');
        $this->prodbrokerservice = $this->chubb_verify_db("prodbrokerservice", '');
        $this->prodRegistroCivil = $this->chubb_verify_db("prodRegistroCivil", '');

?>
        <style>

        </style>
        <div id="wrap">
            <h2>Chubb</h2>
            <p>Como expertos de seguros, nos sentimos orgullosos de nuestro trabajo y firmamos cada pieza elaborada por Chubb como “Chubb. Insured.</p>
            <form method="POST" action="options.php">

                <?php settings_fields('chubb_group'); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row">Access Id</th>
                        <td>
                            <input type="text" name="chubb[accessId]" value="<?= $this->accessId; ?>" style="width: 70%;" />
                        </td>
                        <th scope="row">Secret Key</th>
                        <td>
                            <input type="text" name="chubb[secretKey]" value="<?= $this->secretKey; ?>" style="width: 70%;" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Namespace</th>
                        <td>
                            <input type="text" name="chubb[namespace]" value="<?= $this->namespace ?>" style="width: 70%;"/>
                        </td>
                        <th scope="row">Ambiente</th>
                        <td>
                            <select name="chubb[ambiente]" id="chubb[ambiente]" class="center large" style="width: 70%;" >
                                <option value="test" <?php if ($this->environment == "test") print "selected"; ?> >Pruebas</option>
                                <option value="prod" <?php if ($this->environment == "prod") print "selected"; ?> >Producción</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Split Key</th>
                        <td>
                            <input type="text" name="chubb[splitKey]" value="<?= $this->splitKey; ?>" style="width: 70%;" />
                        </td>
                        <th scope="row">Tiempo de expiración</th>
                        <td>
                            <select name="chubb[expiration]" id="chubb[expiration]">
                                <option value="1" >1 Mes</option>
                                <option value="2" >2 Meses</option>
                                <option value="3" >3 Meses</option>
                                <option value="4" >4 Meses</option>
                                <option value="5" >5 Meses</option>
                                <option value="6" >6 Meses</option>
                                <option value="7" >7 Meses</option>
                                <option value="8" >8 Meses</option>
                                <option value="9" >9 Meses</option>
                                <option value="10">10 Meses</option>
                                <option value="11">11 Meses</option>
                                <option value="12">1 Año</option>
                            </select>
                            <script>
                                var sel = <?= $this->expiration ?>;
                                document.getElementById('chubb[expiration]').getElementsByTagName('option')[sel-1].selected = 'selected';

                            </script>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2">
                            Logo:
                            <center>
                            <div class="container">
                                <form method="post" action="" enctype="multipart/form-data" id="myform">
                                    <div class='preview'>
                                        <img src="../wp-content/plugins/chubb-for-woocomerce/src/logo_chubb.png" class="logo-seguro-checkout" id="img" width="150" >
                                    </div>
                                    <div >
                                        <input type="file" id="file" name="file" accept="image/x-png" />
                                    </div>
                                </form>
                            </div>
                            </center>
                        </td>
                    </tr>
                    <tr valign="top" style="display: none">
                        <th scope="row">ID Chubb Titular </th>
                        <td>
                            <input type="number" min="0" name="chubb[idChubbTitular]" value="<?= $this->idChubbTitular; ?>" style="width: 70%;" />
                        </td>
                        <th scope="row">ID Chubb Titular + Esposa</th>
                        <td>
                            <input type="number" min="0" name="chubb[idChubbTitularEsposa]" value="<?= $this->idChubbTitularEsposa; ?>" style="width: 70%;" />
                        </td>
                    </tr>


                    <tr valign="top">
                        <th scope="row">GetSplitInfo Action</th>
                        <td>
                            <input type="text" name="chubb[actionGetSplitInfo]" value="<?= $this->actionGetSplitInfo ?>" style="width: 70%;" />
                        </td>
                        <th scope="row">GetMessagesByCategory Action</th>
                        <td>
                            <input type="text" name="chubb[actionGetMessagesByCategory]" value="<?= $this->actionGetMessagesByCategory ?>" style="width: 70%;" />
                        </td>
                    </tr>

                    </tr
                    <tr valign="top">
                        <th scope="row">GetPersonInformation  Action</th>
                        <td>
                            <input type="text" name="chubb[actionGetPersonInformation]" value="<?= $this->actionGetPersonInformation ?>" style="width: 70%;" />
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">ProcessTransaction Action</th>
                        <td>
                            <input type="text" name="chubb[actionProcessTransaction]" value="<?= $this->actionProcessTransaction ?>" style="width: 70%;"/>
                        </td>
                        <th scope="row">GetTransactionStatus Action</th>
                        <td>
                            <input type="text" name="chubb[actionGetTransactionStatus]" value="<?= $this->actionGetTransactionStatus ?>" style="width: 70%;"/>
                        </td>
                    </tr>
                    <tr>
                        <td><legend>Urls de conexión</legend> </td>
                    </tr>
                    <tr>
                        <td><legend>Pruebas</legend></td>
                    </tr>
                    <tr>
                        <th scope="row">ExternalWSCMS</th>
                        <td>
                            <input type="text" name="chubb[testExternalWSCMS]" value="<?= $this->testExternalWSCMS ?>" style="width: 95%;"/>
                        </td>
                        <th scope="row">S6TransactionExternal</th>
                        <td>
                            <input type="text" name="chubb[testS6TransactionExternal]" value="<?= $this->testS6TransactionExternal ?>" style="width: 95%;"/>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Broker Service</th>
                        <td>
                            <input type="text" name="chubb[testbrokerservice]" value="<?= $this->testbrokerservice ?>" style="width: 95%;"/>
                        </td>
                        <th scope="row">Registro civil</th>
                        <td>
                            <input type="text" name="chubb[testRegistroCivil]" value="<?= $this->testRegistroCivil ?>" style="width: 95%;"/>
                        </td>
                    </tr>
                    <td><legend>Producción</legend></td>
                    <tr>
                        <th scope="row">ExternalWSCMS</th>
                        <td>
                            <input type="text" name="chubb[prodExternalWSCMS]" value="<?= $this->prodExternalWSCMS ?>" style="width: 95%;"/>
                        </td>
                        <th scope="row">S6TransactionExternal</th>
                        <td>
                            <input type="text" name="chubb[prodS6TransactionExternal]" value="<?= $this->prodS6TransactionExternal ?>" style="width: 95%;"/>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Broker Service</th>
                        <td>
                            <input type="text" name="chubb[prodbrokerservice]" value="<?= $this->prodbrokerservice ?>" style="width: 95%;"/>
                        </td>
                        <th scope="row">Registro civil</th>
                        <td>
                            <input type="text" name="chubb[prodRegistroCivil]" value="<?= $this->prodRegistroCivil ?>" style="width: 95%;"/>
                        </td>
                    </tr>
                    <td><legend>Mensajes de error</legend></td>
                    <tr>
                        <th scope="row">Documento de identidad no es cédula</th>
                        <td>
                            <input type="text" name="chubb[PersonNotFoundInCivilRegistration]" value="<?= $this->msgNoRegister ?>" style="width: 70%;"/>
                        </td>
                        <th scope="row">Restricciones legales</th>
                        <td>
                            <input type="text" name="chubb[PersonHasLegalRestriction]" value="<?= $this->msgLegalIssues ?>" style="width: 70%;"/>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">ID invalida</th>
                        <td>
                            <input type="text" name="chubb[InvalidPersonalId]" value="<?= $this->msgInvalid ?>" style="width: 70%;"/>
                        </td>
                    </tr>

                </table>
                <p class="submit">
                    <input type="submit" class="button-primary" value="Guardar cambios" />
                </p>
            </form>
        </div>
        <script>

            jQuery(document).ready(function($){
                $(document).ready(function(){
                    $("#file").change(function(){
                        var fd = new FormData();
                        var files = $('#file')[0].files[0];
                        fd.append('file',files);

                        $.ajax({
                            url: '../wp-content/plugins/chubb-for-woocomerce/src/upload.php',
                            type: 'post',
                            data: fd,
                            contentType: false,
                            processData: false,
                            success: function(response){
                                if(response != 0){
                                    $("#img").attr("src","../wp-content/plugins/chubb-for-woocomerce/src/"+response);
                                    $(".preview img").show(); // Display image element
                                }else{
                                    alert('No se ha subido el archivo');
                                    $("#img").attr("src","../wp-content/plugins/chubb-for-woocomerce/src/logo_chubb.png");
                                }
                            },
                        });
                    });
                });
            });
        </script>
        <?php
        echo '<form method="post" action="options.php">';
        settings_fields( 'chubb_group' );
    }

    private function consumeService ($service, $data){

        $arrContextOptions=[
            "ssl"=> [
                "verify_peer"=>false,
                "verify_peer_name"=>false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT
            ]
        ];

        $options = [
            'soap_version'=>SOAP_1_1,
            'exceptions'=>true,
            'trace'=>true,
            'cache_wsdl'=>WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($arrContextOptions),
            'encoding' => 'UTF-8',
            'keep_alive' => false,

            'use' => SOAP_LITERAL,
            'style' => SOAP_DOCUMENT,
        ];

        switch ($service){
            case "GetSplitInfo":
                $url = $this->ExternalWSCMS;
                $action = $this->actionGetSplitInfo;
                break;
            case "GetMessagesByCategory":
                $url = $this->ExternalWSCMS;
                $action = $this->actionGetMessagesByCategory;
                break;
            case "GetPersonInformation":
                $url = $this->RegistroCivil;
                $action = $this->actionGetPersonInformation;
                break;
            case "ProcessTransaction":
                $url = $this->S6TransactionExternal;
                $action = $this->actionProcessTransaction;
                break;
            case "GetTransactionStatus":
                $url = $this->S6TransactionExternal;
                $action = $this->actionGetTransactionStatus;
                break;
            default:
                break;
        }

        date_default_timezone_set ( "America/Guayaquil" );
        $date = date('c');

        $hashed = hash_hmac ( "sha1" , $action . $date , $this->secretKey, true );
        $signature = base64_encode($hashed);

        try{
            $header[] = new \SoapHeader( $this->namespace , self::ACCESS_KEY_NAME, $this->accessId   , false );
            $header[] = new \SoapHeader( $this->namespace , self::TIME_STAMP_NAME, $date             , false  );
            $header[] = new \SoapHeader( $this->namespace , self::SIGNATURE_NAME , $signature        , false  );
            $header[] = new \SoapHeader( $this->namespace , self::VERSION_NAME   , self::VERSION_VALUE, false  );

            $client = new \SoapClient($url, $options);
            $client->__setSoapHeaders($header);

            switch ($service){
                case "GetPersonInformation":
                    $GetPersonInformation = new \GetPersonInformation( $this->accessId, $data );
                    $response = json_decode( json_encode ( $client->GetPersonInformation( $GetPersonInformation ) ), true);
                    $option = "none";
                    if ( trim($response['GetPersonInformationResult']['Status']) == "PersonSuccessfullyValidated" ){
                        //continuar proceso seguro
                        $birthDate = explode("/", $response['GetPersonInformationResult']['Person']['Birthdate'] );
                        $age = (date("dm", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                            ? ((date("Y") - $birthDate[2]) - 1)
                            : (date("Y") - $birthDate[2]));

                        $this->minAge = $this->splitInfo['GetSplitInfoResult']['SplitInfo']['products']['product']['miMinAge'];
                        $this->maxAge = $this->splitInfo['GetSplitInfoResult']['SplitInfo']['products']['product']['miMaxAge'];

                        if ($this->minAge <= $age && $this->maxAge >= $age){
                            $mensaje = "Usted es apto para el seguro Chubb";
                            iF ( $response['GetPersonInformationResult']['Person']['MaritalStatus']['Code'] != "CASADO" ){
                                print "<h5 style='color: red '>$mensaje <a  href=\"#ex1\" rel=\"modal:open\" style='color: grey'>Ver detalle</a> </h5>
                                    <center>
                                           <img src=\"../wp-content/plugins/chubb-for-woocomerce/src/logo_chubb.png\" class=\"logo-seguro-checkout\" id=\"img\">                     
                                    </center>
                                    ";
                                woocommerce_form_field( 'titular', array(
                                    'type'          => 'checkbox',
                                    'class'         => array('form-row privacy'),
                                    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
                                    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
                                    'required'      => false,
                                    'label'         => 'Seleccione para añadir Plan Titular de Chubb',
                                ));
                                $option = "titular";
                            }

                            if ( $response['GetPersonInformationResult']['Person']['MaritalStatus']['Code'] == "CASADO" ){
                                $mensaje = "Por favor verifique su carrito y seleccione el seguro que desee";
                                print "<h5 style='color: red '>$mensaje <a  href=\"#ex1\" rel=\"modal:open\" style='color: grey'>Ver detalle</a> </h5>
                                    <center>
                                           <img src=\"../wp-content/plugins/chubb-for-woocomerce/src/logo_chubb.png\" class=\"logo-seguro-checkout\" id=\"img\" width=\"100\" height=\"100\">                     
                                    </center>";
                                woocommerce_form_field( 'titular', array(
                                    'type'          => 'checkbox',
                                    'class'         => array('form-row privacy'),
                                    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
                                    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
                                    'required'      => false,
                                    'label'         => 'Seleccione para añadir Plan Titular de Chubb',
                                ));

                                woocommerce_form_field( 'familiar', array(
                                    'type'          => 'checkbox',
                                    'class'         => array('form-row privacy'),
                                    'label_class'   => array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
                                    'input_class'   => array('woocommerce-form__input woocommerce-form__input-checkbox input-checkbox'),
                                    'required'      => false,
                                    'label'         => 'Seleccione para añadir Plan Titular + Esposa de Chubb',
                                    'onBlur'
                                ));
                                if ( !$this->chubb_product_in_cart($this->idChubbTitularEsposa) ){
                                    WC()->cart->add_to_cart( $this->idChubbTitularEsposa );
                                }

                                $option = "familiar";
                            } else {
                                if ( !$this->chubb_product_in_cart($this->idChubbTitular) ){
                                    WC()->cart->add_to_cart( $this->idChubbTitular );
                                }
                            }
                        } else {
                            $mensaje = "No se encuentra en el rango de edades.";
                        }
                    } else if ( trim($response['GetPersonInformationResult']['Status']) == "PersonNotFoundInCivilRegistration" ){
                        $mensaje = $this->msgNoRegister;
                    } else if ( trim($response['GetPersonInformationResult']['Status']) == "PersonHasLegalRestriction" ){
                        $mensaje = $this->msgLegalIssues;
                    } else if ( trim($response['GetPersonInformationResult']['Status']) == "InvalidPersonalId" ){
                        $mensaje = $this->msgInvalid;
                    }
                    if ($option != "none" ){
                        print "
                        <script>
                            var event = new Event('change');
                            document.getElementById( '$option' ).checked = true;
                            setTimeout(function() {
                               document.getElementById('$option').dispatchEvent(event);
                            }, 50);
                        </script>";
                        $titular_description = get_post( $this->idChubbTitular )->post_content;
                        $titular_esposa_description = get_post( $this->idChubbTitularEsposa )->post_content;
                        print "
                            <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js\"></script>
                            <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css\" />
                   
                            <div id=\"ex1\" class=\"modal modal-seguro-checkout\" style='min-width: 600px; min-height: 600px; margin-top: 30px' >
                                <div id='infoTitular' >
                                    $titular_description
                                </div>
                            ";
                        if ( $option == "familiar" ){
                            print "
                                <div id='infoTEsposa'>
                                <hr>
                                    $titular_esposa_description
                                </div>";
                        }
                        print "
                            </div>
                            <style>
                            .blocker{
                                background-color: rgba(0,0,0,0.5)
                            }
                            .jquery-modal{
                                z-index: 9999999999
                            }
                            </style>";
                    } else {
                        print "
                        <script>
                            setTimeout(function() {
                               $( 'body' ).trigger( 'update_checkout' );
                            }, 50);
                        </script>
                        ";
                    }

                    //return $mensaje;
                    break;
                case "GetSplitInfo":
                    $GetSplitInfoRequest = new \GetSplitInfoRequest( );
                    $GetSplitInfoRequest->setLanguageCode   (230);
                    $GetSplitInfoRequest->setLoadBankInfo   (true);
                    $GetSplitInfoRequest->setSplitKey       ($data);
                    $GetSplitInfo = new \GetSplitInfo($this->accessId, $GetSplitInfoRequest);
                    $result = $client->GetSplitInfo( $GetSplitInfo );
                    file_get_contents(__DIR__ . '/SplitInfo/GetSplitInfo.json', base64_encode( $result ));
                    break;
                case "ProcessTransaction":
                    $ProcessTransaction = new \ProcessTransaction($this->accessId, $data[0]);
                    $result = $client->ProcessTransaction( $ProcessTransaction );
                    $result = json_encode($result, true);
                    ERROR_LOG("respuesta". PHP_EOL.$result. PHP_EOL. "peticion ". PHP_EOL.$client->__getLastRequest(), 3, ABSPATH."logs.log");
                    $result = json_decode($result, true);
                    if ( $result['ProcessTransactionResult'] ['S6TransactionResult']['statusCode'] == "PROCESSED_OK"){
                        $policyNumber = $result['ProcessTransactionResult'] ['S6TransactionResult']['policyNumber'];
                        $s6TransactionID = $result['ProcessTransactionResult'] ['S6TransactionResult']['s6TransactionID'];
                        global $wpdb;
                        $nombreTabla = $wpdb->prefix . "chubb_orders";
                        $date = $data[3]->format('Y-m-d H:i:s');
                        $wpdb->update(
                            $nombreTabla,
                            [
                                'policy_number'     => $policyNumber,
                                's6_tran_id'        => $s6TransactionID,
                                'order_status'      => "VIGENTE",
                                'order_vigency_date'=> date("c", strtotime($date))
                            ],
                            [
                                'document_id'   => $data[1],
                                'order_id'      => $data[2]
                            ]
                        );

                    }

                default:
                    break;
            }
            return $result;
        }  catch (Exception $e) {
            echo "<h2>Exception Error! $url</h2>";
            echo $e -> getMessage () . "\n";
            print "configurar el plugin";
            echo "REQUEST:\n" . $client->__getLastRequest() . "\n";

            return $client;
        }
    }

    /**
     * Return urls of environments for selection
     *
     * @return array
     */
    protected function getEnvironment( $environment)
    {
        $this->ExternalWSCMS = $this->chubb_verify_db($environment."ExternalWSCMS", 'https://testwebservices.chubblatinamerica.com/ExternalCmsWsUAT/ExternalWSCMS.svc?singlewsdl');
        $this->S6TransactionExternal = $this->chubb_verify_db($environment."S6TransactionExternal", 'https://testwebservices.chubblatinamerica.com/ExternalS6TransactionWSUAT/S6TransactionExternal.svc?singlewsdl');
        $this->brokerservice = $this->chubb_verify_db($environment."brokerservice", 'https://s6broker-ext-uat.acegroup.com/brokerservice.svc?singlewsdl');
        $this->RegistroCivil = $this->chubb_verify_db($environment."RegistroCivil", 'https://testwebservices.chubblatinamerica.com/ExternalEcuadorListWSUAT/RegistroCivil.svc?singlewsdl');
    }

    /**
     * Verifys and adds product if not exist to woocomerce
     *
     * @return array
     */
    protected function addProduct(){
        require_once(ABSPATH . 'wp-admin/includes/post.php');
        $post_title = 'Chubb Plan Titular';
        $post_id = @get_option('chubb_product_1');
        if (!post_exists($post_title)) { // Determine if a post exists based on title, content, and date
            $this->chubb_addProduct($post_title, 'Edit..', 'Edit..', 'CHUBB01',3.00, "chubb_product_1");
        } else {
            $product = wc_get_product($post_id);
            $this->idChubbTitular = get_option("chubb_product_1" );
        }

        $post_id = @get_option('chubb_product_2');
        $post_title = 'Chubb Plan Titular + Esposa';
        if (!post_exists($post_title)) { // Determine if a post exists based on title, content, and date
            $this->chubb_addProduct($post_title, 'Edit..', 'Edit..', 'CHUBB02',5.50, "chubb_product_2");
        } else {
            $product = wc_get_product($post_id);
            $this->idChubbTitularEsposa = get_option("chubb_product_2");
        }
    }

    protected function chubb_addProduct($post_title, $content, $excerpt, $sku, $price, $name_db){
        $data = [
            'post_title'    => $post_title,
            'post_type'     => 'product',
            'post_status'   => 'publish',
            'post_content'  => $content,
            'post_excerpt'  => $excerpt,
            'sku'           => $sku,
            "tax_query"     => [
                    'taxonomy' =>   'product_visibility',
                    'field'    =>   'slug',
                    'terms'    =>   array('exclude-from-search', 'exclude-from-catalog'),
                    'operator' =>   'NOT IN',
            ]
        ];
        $post_id = wp_insert_post($data, true );
        //update_post_meta( $post_id, '_visibility', '_visibility_hidden' );
        update_post_meta( $post_id, '_price', $price );

        add_option($name_db, $post_id );
        update_option($name_db, $post_id );

        //Set product hidden:
        $terms = array( 'exclude-from-catalog', 'exclude-from-search' );
        wp_set_object_terms( $post_id, $terms, 'product_visibility' );

    }
    /**
     * Verificar si puede acceder al seguro
     * @return void
     */
    public function chubb_check_seguro( $cedula )
    {
        $this->accessId = get_option('chubb') ["accessId"];
        $this->secretKey = get_option('chubb') ["secretKey"];
        $this->environment = get_option('chubb') ["ambiente"];
        $this->splitKey = get_option('chubb') ["splitKey"];

        $this->getEnvironment($this->environment);

        $this->splitInfo = json_decode( file_get_contents(__DIR__."/SplitInfo/GetSplitInfo.json"),  true);

        if ($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['splitKey'] != $this->splitKey ){
            $this->consumeService ("GetSplitInfo", $this->splitKey);
        }

        $this->actionGetSplitInfo = get_option('chubb') ["actionGetSplitInfo"];
        $this->actionGetMessagesByCategory = get_option  ('chubb') ["actionGetMessagesByCategory"];
        $this->actionGetPersonInformation = get_option('chubb') ["actionGetPersonInformation"];
        $this->actionProcessTransaction = get_option('chubb') ["actionProcessTransaction"];
        $this->actionGetTransactionStatus = get_option('chubb') ["actionGetTransactionStatus"];

        $this->namespace = get_option('chubb') ["namespace"];

        $this->msgNoRegister = get_option('chubb') ["PersonNotFoundInCivilRegistration"];
        $this->msgLegalIssues = get_option('chubb') ["PersonHasLegalRestriction"];
        $this->msgInvalid = get_option('chubb') ["InvalidPersonalId"];

        $this->consumeService ("GetPersonInformation", $cedula);
    }

    public function chubb_set_payment( $order_id ){
        require_once( ABSPATH . 'wp-config.php' );
        $this->accessId = get_option('chubb') ["accessId"];
        $this->secretKey = get_option('chubb') ["secretKey"];
        $this->environment = get_option('chubb') ["ambiente"];
        $this->splitKey = get_option('chubb') ["splitKey"];
        $this->expiration = $this->chubb_verify_db('expiration', 3);

        $this->getEnvironment($this->environment);
        $this->splitInfo = json_decode( file_get_contents(__DIR__."/SplitInfo/GetSplitInfo.json"),  true);
        $this->actionGetSplitInfo = get_option('chubb') ["actionGetSplitInfo"];
        $this->actionGetMessagesByCategory = get_option  ('chubb') ["actionGetMessagesByCategory"];
        $this->actionGetPersonInformation = get_option('chubb') ["actionGetPersonInformation"];
        $this->actionProcessTransaction = get_option('chubb') ["actionProcessTransaction"];
        $this->actionGetTransactionStatus = get_option('chubb') ["actionGetTransactionStatus"];
        $this->namespace = get_option('chubb') ["namespace"];
        $this->msgNoRegister = get_option('chubb') ["PersonNotFoundInCivilRegistration"];
        $this->msgLegalIssues = get_option('chubb') ["PersonHasLegalRestriction"];
        $this->msgInvalid = get_option('chubb') ["InvalidPersonalId"];

        $this->splitInfo = json_decode( file_get_contents(__DIR__."/SplitInfo/GetSplitInfo.json"),  true);

        $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
        mysqli_select_db($conn, DB_NAME);

        $order = wc_get_order( $order_id );
        $user = $order->get_user();

        $items = $order->get_items();

        $orderArray =  json_decode (  $order , true );
        for ($i=0; $i< count($orderArray["meta_data"]); $i++){
            if ( $orderArray["meta_data"][$i]["key"] == "_billing_ruc"  ){
                $doc_id = trim(substr( $orderArray["meta_data"][$i]["value"] ,0,10));
            }
            if ( $orderArray["meta_data"][$i]["key"] == "_billing_cedula"  ){
                $doc_id = $orderArray["meta_data"][$i]["value"] ;
            }
        }

        foreach ( $items as $item ) {
            $product_name[] = $item->get_name();
            $product_id[] = $item->get_product_id();
            $product_variation_id[] = $item->get_variation_id();
        }

        $titular = 0;
        $titularEsposa = 0;
        for ($i=0; $i<count($product_id); $i++){
            if ( $product_id[$i] == $this->idChubbTitular ||  $product_name[$i] == "Chubb Plan Titular") {
                $titular=1;
            }
            if ( $product_id[$i] == $this->idChubbTitularEsposa ||  $product_name[$i] == "Chubb Plan Titular + Esposa") {
                $titularEsposa=1;
            }
        }


        global $wpdb;
        $nombreTabla = $wpdb->prefix . "chubb_orders";

        $orders = $wpdb->get_results('Select * from '.$nombreTabla. ' where document_id="'.$doc_id.'" and order_status="VIGENTE"');

        if ( $titular == 1 || $titularEsposa == 1){

            $titular == 1 ? $productos = "Chubb Plan Titular" : $productos = "Chubb Plan Titular + Esposa";

            date_default_timezone_set ( "America/Guayaquil" );

            if (count($orders) < 3){
                $wpdb->insert(
                    $nombreTabla,
                    array(
                        'order_id'          => $order_id,
                        'document_id'       => $doc_id,
                        'order_date_time'   => date( 'c' ),
                        'order_status'      => "PENDIENTE",
                        'order_client'      => strtoupper($user->user_firstname. " ".$user->user_lastname),
                        'product_owned'     => strtoupper($productos),
                    )
                );
            }
        } else {
            return;
        }

        $arrContextOptions=[
            "ssl"=> [
                "verify_peer"=>false,
                "verify_peer_name"=>false,
                'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT
            ]
        ];

        $options = [
            'soap_version'=>SOAP_1_1,
            'exceptions'=>true,
            'trace'=>true,
            'cache_wsdl'=>WSDL_CACHE_NONE,
            'stream_context' => stream_context_create($arrContextOptions),
            'encoding' => 'UTF-8',
            'keep_alive' => false,

            'use' => SOAP_LITERAL,
            'style' => SOAP_DOCUMENT,
        ];

        $url = $this->RegistroCivil;
        $action = $this->actionGetPersonInformation;

        date_default_timezone_set ( "America/Guayaquil" );
        $date = date('c');

        $hashed = hash_hmac ( "sha1" , $action . $date , $this->secretKey, true );
        $signature = base64_encode($hashed);
        $header[] = new \SoapHeader( $this->namespace , self::ACCESS_KEY_NAME, $this->accessId   , false );
        $header[] = new \SoapHeader( $this->namespace , self::TIME_STAMP_NAME, $date             , false  );
        $header[] = new \SoapHeader( $this->namespace , self::SIGNATURE_NAME , $signature        , false  );
        $header[] = new \SoapHeader( $this->namespace , self::VERSION_NAME   , self::VERSION_VALUE, false  );
        $client = new \SoapClient($url, $options);
        $client->__setSoapHeaders($header);

        $GetPersonInformation = new \GetPersonInformation( $this->accessId, $doc_id );
        $infoUser = json_decode( json_encode ( $client->GetPersonInformation( $GetPersonInformation ), true ), true);

        ERROR_LOG("user with id-".$doc_id. PHP_EOL. json_encode ( $infoUser, true ), 3, ABSPATH."user.log");

        $ProcessTransactionRequest = new \ProcessTransactionRequest();
        $s6Transaction = new \s6Transaction();

        $s6Transaction->setTranType("NEW");
        //$s6Transaction->setChgType("ADI");
        $s6Transaction->setLineNum(1);
        $s6Transaction->setCountryCd("EC");
        $s6Transaction->setCampaign($this->splitKey);
        $paymentInfo = new \paymentInfo();
        /*
         * Tarjeta de Crédito - 882
         * Débito bancario - 882
         * Facturación directa - 884
         * Bordereaux - 885
         * GIRO 886
         * Método No Estándar de Pago - 887
         * Descuento de nómina - 888
         */
        $paymentInfo->setPayMethod($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['payments'] ['payment'] ['paymentMethodCode']);
        /*
         * 871 - Weekly - 1 Invoice every Week
           872 -  Bi-Weekly - 1 Inv every 2 Weeks,26/Yr.
           873 - Anual
           874 - Display Bi-Weekly - 1 Monthly Invoice
           875 - Bi-Weekly - 2 Inv every Month,24/Yr.
           876 - Pago Unico
           877 - Mensual
           879 - Trimestral
           880 - Semestral
           881 - Display Weekly - 1 Monthly invoice
           1909 - Single Payment
           1003817 - Bimensual
         */
        $paymentInfo->setPayFreq($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['paymentFrequencies'] ['paymentFrequency'] ['paymentFrequencyCode']);


        $paymentInfo->setCollCd($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['payments'] ['payment'] ['collectors'] ['collector'] ['collectorCode']);
        /*
        $paymentInfo->setTaxAppCd(null);
        $paymentInfo->setNameOnBill("nombre del cliente"); //edit
        $paymentInfo->setTaxJurisCd(null);
        $paymentInfo->setEmployeeNum(null);
        $paymentInfo->setDebitDay(null);
        $paymentInfo->setBillAccNum(null);
        $paymentInfo->setBankCd(null);
        $paymentInfo->setBranchCd(null);
        $paymentInfo->setCcCd(null);
        $paymentInfo->setExpDate(date("m/Y")); //edit
        $paymentInfo->setLast5BillAccNum(null);
        */
        $s6Transaction->setPaymentInfo($paymentInfo);
        $products = new \ArrayOfproduct();
        $product[0] = new \product();
        $product[0]->setProdCd($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['products'] ['product'] ['productId']);
        /*
         * 1680 - Main Insured Only
         * 1681 - Main Insured and Spouse
         * 1682 - Main Insured and Family
         * 1683 - Spouse Only
         * 1684 - Spouse and Dependent
         * 1685 - Dependent Only
         * 1686 - Main Insured and Dependents
         */
        if ($titular == 1){
            $product[0]->setCoverageCd(1680);
        }
        else if ($titularEsposa == 1){
            $product[0]->setCoverageCd(1681);
        }
        $products->setProduct($product);
        $s6Transaction->setProducts($products);
        $correspondance =  new \correspondenceType();
        $s6Transaction->setCorrespondence( $correspondance::Email );
        $s6Transaction->setBill(2051);

        $costumers = new \ArrayOfcustomer();
        $costumer[0] = new \customer();
        $costumer[0]->setCustId($doc_id);
        $costumer[0]->setCustType( "MI" );
        $costumer[0]->setPolHolder(true);
        $costumer[0]->setPolPayer(true);
        $costumer[0]->setFirstName(strtoupper ($order->get_billing_first_name() ));
        $costumer[0]->setLastName(strtoupper ($order->get_billing_last_name() ));
        $costumer[0]->setPersonalId($doc_id);
        $birdthday = explode("/",$infoUser['GetPersonInformationResult']['Person']['Birthdate']);
        $birdthday = $birdthday[2]."-".$birdthday[1]."-".$birdthday[0];
        $dataT = new \DateTime($birdthday);
        $dataT->format("Y-m-dTH:i:s");
        $costumer[0]->setBirthDate($dataT);
        $costumer[0]->setEmailAddr($order->get_billing_email());
        $costumer[0]->setEmailFulfillment("Y");
            $custProds = new \ArrayOfcustProd();
            $custProd[0] = new \custProd();
            $custProd[0]->setProdCd($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['products'] ['product'] ['productId']);
            $custProd[0]->setBenLv($this->splitInfo ['GetSplitInfoResult'] ['SplitInfo'] ['products'] ['product'] ['benefitLevels'] ['benefitLevel'] ['benefitLevelCode']);
        $custProds->setCustProd($custProd);
        $costumer[0]->setCustProds($custProds);
        $address = $order->get_billing_address_1(). " ".$order->get_billing_address_2() ;
            $custAdds = new \custAdds();
            $custAdds->setCustAdd(  [   sha1($address)  ]   );
        $costumer[0]->setCustAdds( $custAdds );
        $costumers->setCustomer($costumer);
        $s6Transaction->setCustomers($costumers);

        $expiration = new \DateTime();
        $expiration->format(\DateTime::ATOM);
        $expiration->modify('+'.$this->expiration.' month');
        $s6Transaction->setPolExpDate($expiration);
        $ArrayOfaddress = new \ArrayOfaddress();
            /*
             * 219 => Home
             * 220 => Mail
             * 221 => Work
             */
            $addressArray[0] = new \address();
            $addressArray[0]->setAddrId( sha1($address) );
            $addressArray[0]->setAddrType(219);
            $addressArray[0]->setLine1( $address );
            $addressArray[0]->setPostalCd( $order->get_billing_postcode() );
            /*
             * 100966 => Orellana
               101403 => Canar
               102428 => Azuay
               102881 => Manabi
               103310 => Chimborazo
               103403 => Zamora-Chinchipe
               103695 => Pastaza
               104249 => Bolivar
               104353 => Sucumbios
               104650 => Loja
               104791 => Imbabura
               105934 => Cotopaxi
               106564 => Morona-Santiago
               106580 => Napo
               106710 => Esmeraldas
               106954 => Carchi
               107422 => Pichincha
               107476 => Los Rios
               107721 => Galapagos
               108618 => El Oro
               108668 => Guayas
               109063 => Tungurahua
               1002081 => Santo Domingo de los Tsachilas
               1002082 => Santa Elena
             */
            $addressArray[0]->setProvinceCd(107422);
            $addressArray[0]->setCountryCd( "EC" );

            $ArrayOfaddress->setAddress($addressArray);
        $s6Transaction->setAddresses($ArrayOfaddress);

        $ProcessTransactionRequest->setTransaction($s6Transaction);
        $ProcessTransactionRequest->setStoreInQueue(true);      // true
        $ProcessTransactionRequest->setShouldCommit(true);      // test en false y prod true
        $ProcessTransactionRequest->setShouldReplace(true);
        $ProcessTransactionRequest->setAllowDuplicate(true);

        $data = [
            $ProcessTransactionRequest,
            $doc_id,
            $order_id,
            $expiration
        ];
        $this->consumeService("ProcessTransaction", $data);
    }

    public function chubb_product_in_cart($product_id) {
        global $woocommerce;

        foreach($woocommerce->cart->get_cart() as $key => $val ) {
            $_product = $val['data'];

            if($product_id == $_product->id ) {
                return true;
            }
        }

        return false;
    }

    public function addListener(){
        //change paste keyup blur
        print "
        <script>
        var oldData = '';
        var event = new Event('keyup');
            jQuery(document).ready(function($){
                $('#billing_cedula').on('keyup', function () {
                    if ( $('#billing_cedula').val().length == 10  ){
                        $('#place_order').attr('disabled', true);
                        if ( oldData != $('#billing_cedula').val() ){
                            $.post(
                                '../wp-content/plugins/chubb-for-woocomerce/src/check_chubb_availability.php',
                                 { data: $('#billing_cedula').val() },
                                function(data) {
                                    if (data == 'Seguro ya adquirido'){
                                         $( '#chubb-div' ).html('');
                                        $( 'body' ).trigger( 'update_checkout' );
                                    } else {
                                        $( '#chubb-div' ).html(data); 
                                        $( 'body' ).trigger( 'update_checkout' );
                                        $('#titular').on( 'change', function(){
                                            if ($('#familiar').length != 0){
                                                $('#familiar').prop('checked', '');
                                            }
                                            $.post(
                                                '../wp-content/plugins/chubb-for-woocomerce/src/check_options_chubb.php',
                                                { data: 'titular', idFamiliar: $this->idChubbTitularEsposa, idTitular: $this->idChubbTitular, act: $('#titular').prop('checked') },
                                                function(data) {
                                                    console.log(data);
                                                    $( 'body' ).trigger( 'update_checkout' );
                                                }
                                            ) ;
                                        }); 
                                        $('#familiar').on( 'change', function(){
                                            $('#titular').prop('checked', '');
                                            $.post(
                                                '../wp-content/plugins/chubb-for-woocomerce/src/check_options_chubb.php',
                                                { data: 'familiar', idFamiliar: $this->idChubbTitularEsposa, idTitular: $this->idChubbTitular, act: $('#familiar').prop('checked') },
                                                function(data) {
                                                    console.log(data);            
                                                    $( 'body' ).trigger( 'update_checkout' );
                                                }
                                            ) ;
                                        }); 
                                    }
                                }
                            );
                            oldData =  $('#billing_cedula').val();
                        }
                            
                    } 
                    
                }
                
                );
                $('#billing_ruc').on('keyup', function () {
                    if ( $('#billing_ruc').val().length == 13 ){
                        $('#place_order').attr('disabled', true);
                        if ( oldData != $('#billing_ruc').val() ){
                            $.post(
                                '../wp-content/plugins/chubb-for-woocomerce/src/check_chubb_availability.php',
                                 { data: $('#billing_ruc').val() },
                                function(data) {
                                    if (data == 'Seguro ya adquirido'){
                                         $( '#chubb-div' ).html('');
                                        $( 'body' ).trigger( 'update_checkout' );
                                    } else {
                                         $( '#chubb-div' ).html(data); 
                                        $( 'body' ).trigger( 'update_checkout' );
                                        
                                        $('#titular').on( 'change', function(){
                                            if ($('#familiar').length != 0){
                                                $('#familiar').prop('checked', '');
                                            }
                                            $.post(
                                                '../wp-content/plugins/chubb-for-woocomerce/src/check_options_chubb.php',
                                                { data: 'titular', idFamiliar: $this->idChubbTitularEsposa, idTitular: $this->idChubbTitular, act: $('#titular').prop('checked') },
                                                function(data) {
                                                    console.log(data);
                                                    $( 'body' ).trigger( 'update_checkout' );
                                                }
                                            ) ;
                                        }); 
                                        $('#familiar').on( 'change', function(){
                                            $('#titular').prop('checked', '');
                                            $.post(
                                                '../wp-content/plugins/chubb-for-woocomerce/src/check_options_chubb.php',
                                                { data: 'familiar', idFamiliar: $this->idChubbTitularEsposa, idTitular: $this->idChubbTitular, act: $('#familiar').prop('checked') },
                                                function(data) {
                                                    console.log(data);            
                                                    $( 'body' ).trigger( 'update_checkout' );
                                                }
                                            ) ;
                                        }); 
                                    }
                                }
                            );
                            oldData =  $('#billing_ruc').val();
                        }
                    } 
                    
                    
                }
                
                );   
            });
            window.onload=function() { 
                if ( document.getElementById('billing_cedula').value.length == 10)
                    document.getElementById('billing_cedula').dispatchEvent(event);
                if ( document.getElementById('billing_ruc').value.length == 13)
                    document.getElementById('billing_ruc').dispatchEvent(event);
            }
        </script>
        ";
    }


    public function addDiv(){
        ?>
        <div id="chubb-div">

        </div>
        <?php
    }

    public function chubb_order_history(){
        global $wpdb;
        $nombreTabla = $wpdb->prefix . "chubb_orders";

        $orders = $wpdb->get_results('Select * from '.$nombreTabla);
?>

        <div class="container-fluid">
            <div class="row p-3">
                <div class="col-12">
                    <h2>Historial de Chubb</h2>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-4 text-left">
                    <div class="input-group input-group-sm m-2">
                        <input id="buscar" type="text" class="form-control rounded" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" placeholder="buscar . . .">
                    </div>
                </div>
            </div>
        <div class="row p-3" style="font-size: smaller">
            <div class="table-responsive">
                <table class="wp-list-table widefat fixed striped posts" id="lista">
                    <thead class="thead-dark">
                    <tr>
                        <th> ID Orden - Cédula  </th>
                        <th>        Seguro      </th>
                        <th>        Cliente     </th>
                        <th>        Estado      </th>
                        <th>        Fecha       </th>
                        <th>   Vigencia hasta   </th>
                        <th>        Póliza      </th>
                        <th> s6 Transaction ID  </th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    for ($i=0; $i< count($orders);$i++){
                        ?>
                        <tr class="fila">
                            <td><a href="<?=admin_url('post.php?post='.$orders[$i]->order_id.'&amp;action=edit'); ?>" class="order-view"><strong><?= $orders[$i]->order_id." - ".$orders[$i]->document_id; ?></strong></a> </td>
                            <td><?= $orders[$i]->product_owned; ?>  </td>
                            <td><?= $orders[$i]->order_client; ?></td>
                            <td><?= $orders[$i]->order_status; ?></td>
                            <td><?= $orders[$i]->order_date_time; ?></td>
                            <td><?= $orders[$i]->order_vigency_date; ?></td>
                            <td><?= $orders[$i]->policy_number; ?></td>
                            <td><?= $orders[$i]->s6_tran_id; ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script>
            //var $=jQuery.noConflict();
            $(document).ready(function(){
                $("#buscar").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#lista .fila").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });

            });
        </script>
<?php
    }

}
