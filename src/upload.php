<?php

/* Getting file name */
$filename = "logo_chubb.png";

/* Location */
$location = $filename;
$uploadOk = 1;
try {
    $imageFileType = pathinfo($location,PATHINFO_EXTENSION);
    /* Valid Extensions */
    //$valid_extensions = array("jpg","jpeg","png");
    $valid_extensions = array("png");
    /* Check file extension */
    if( !in_array(strtolower($imageFileType),$valid_extensions) ) {
        $uploadOk = 0;
    }

    if($uploadOk == 0){
        echo 0;
    }else{
        /* Upload file */
        if(move_uploaded_file($_FILES['file']['tmp_name'],$location)){
            echo $location;
        }else{
            echo 0;
        }
    }

} catch(Exception $ex){
    print $ex->getMessage();
}