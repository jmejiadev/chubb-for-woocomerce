<?php

namespace Chubb\ChubbPlugin;

use Chubb\ChubbPlugin\Constants\Environment;
use WC_Settings_API;
use Exception;
use WC_HTTPS;


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

/**
 * Class Functions
 * @package Chubb
 */
class Functions extends Woocomerce_Chubb
{

    function __construct()
    {
        //$this->config();
        parent::__construct('',__FILE__);
        $this->initFormFields();
    }

    /**
     * Return list of environments for selection
     *
     * @return array
     */
    protected function getEnvironments()
    {
        return [
            Environment::TEST => __('Test', 'chubb-for-woocommerce'),
            Environment::PROD => __('Production', 'chubb-for-woocommerce'),
        ];
    }
    
    /**
     * Settings Options
     * @return void
     */
    public function initFormFields()
    {
        $this->form_fields = include(__DIR__ . '/config/form-fields.php');
       // $this->init_settings();
    }
}