<?php

require_once(dirname(__FILE__) . '/../../../../wp-load.php');

$data = $_POST;
$prod_unique_id = WC()->cart->generate_cart_id($data ['idFamiliar' ]);
WC()->cart->remove_cart_item( $prod_unique_id );
$prod_unique_id = WC()->cart->generate_cart_id($data ['idTitular' ]);
WC()->cart->remove_cart_item( $prod_unique_id );


if ( $data['data'] == "titular" &&  $data['act'] == "true" ){
    if ( !chubb_product_in_cart( $data['idTitular']  ) ){
        WC()->cart->add_to_cart( $data['idTitular']  );
    }
} else {
    if ( $data['act'] == "true" ){
        if ( !chubb_product_in_cart( $data['idFamiliar']  ) ){
            WC()->cart->add_to_cart( $data['idFamiliar']  );
        }
    }
}


print json_encode($data, true);

function chubb_product_in_cart($product_id) {
    global $woocommerce;

    foreach($woocommerce->cart->get_cart() as $key => $val ) {
        $_product = $val['data'];

        if($product_id == $_product->id ) {
            return true;
        }
    }

    return false;
}


function woocommerce_order_review( $deprecated = false ) {
    wc_get_template(
        'checkout/review-order.php',
        array(
            'checkout' => WC()->checkout(),
        )
    );
}

?>