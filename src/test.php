<?php


include "Services/ExternalWSCMS/autoload.php";
include "Services/S6TransactionExternal/autoload.php";
include "Services/BrokerService/autoload.php";
include "Services/RegistroCivil/autoload.php";

//ejemplo

//$splitInfo = json_decode( file_get_contents("./GetSplitInfo.json"),true);
print "<pre>";
error_reporting(E_ALL);
ini_set('display_errors', '1');

$accessId = "!az|^KfI5fH(tqfl@UFl";
$secret = "}!A+5Y%E9*%aK{8II(9P.nW@BzOS6K9knYlPgh2f";
date_default_timezone_set ( "America/Guayaquil" );
//$DATE_TIME_FORMAT = "yyyy-MM-ddTHH:mm:ss.fffZ";//Format to send timestamp
$fecha = date('c');

$NAMESPACE = "http://www.acelatinamerica.com/ACELA";//This namespace is sent in the request

$VERSION_VALUE = "1.0.0.0";//This version is sent in the request

$ACCESS_KEY_NAME = "AccessKey";//Name of message in the header

$TIME_STAMP_NAME = "Timestamp";//Name of message in

$SIGNATURE_NAME = "Signature";//Name of message in the header

$VERSION_NAME = "Version";//Name of message in the header

$urls = [
    "pruebas" => [
        "https://testwebservices.chubblatinamerica.com/ExternalCmsWsUAT/ExternalWSCMS.svc?singlewsdl", //CMS
        /*
         *Array
            (
                [0] => GetSplitInfoResponse GetSplitInfo(GetSplitInfo $parameters)
                [1] => GetMessagesByCategoryResponse GetMessagesByCategory(GetMessagesByCategory $parameters)
            )
         */
        "https://testwebservices.chubblatinamerica.com/ExternalS6TransactionWSUAT/S6TransactionExternal.svc?singlewsdl", //s6Transacion
        /*
         *Array
            (
                [0] => ProcessTransactionResponse ProcessTransaction(ProcessTransaction $parameters)
                [1] => GetTransactionStatusResponse GetTransactionStatus(GetTransactionStatus $parameters)
            )
         */
        "https://s6broker-ext-uat.acegroup.com/brokerservice.svc?singlewsdl",
        /*
         * Array
            (
                [0] => ValidateCreditTokenResponse ValidateCreditToken(ValidateCreditToken $parameters)
                [1] => ValidateDDATokenResponse ValidateDDAToken(ValidateDDAToken $parameters)
                [2] => ValidateGIROTokenResponse ValidateGIROToken(ValidateGIROToken $parameters)
                [3] => GetTokenForEncryptedValueResponse GetTokenForEncryptedValue(GetTokenForEncryptedValue $parameters)
                [4] => GetCardTokenForAmexGENTokenValueResponse GetCardTokenForAmexGENTokenValue(GetCardTokenForAmexGENTokenValue $parameters)
                [5] => GetCardTokenForAmexGENValueResponse GetCardTokenForAmexGENValue(GetCardTokenForAmexGENValue $parameters)
                [6] => GetIBANValueFromTokenValueResponse GetIBANValueFromTokenValue(GetIBANValueFromTokenValue $parameters)
                [7] => GetLogInfoSummaryResponse GetLogInfoSummary(GetLogInfoSummary $parameters)
                [8] => GetLogInfoDetailsResponse GetLogInfoDetails(GetLogInfoDetails $parameters)
                [9] => GetIBANValueFromTokenValue2Response GetIBANValueFromTokenValue2(GetIBANValueFromTokenValue2 $parameters)
            )
         */
        "https://testwebservices.chubblatinamerica.com/ExternalEcuadorListWSUAT/RegistroCivil.svc?singlewsdl",
        /*
         * GetPersonInformationResponse GetPersonInformation(GetPersonInformation $parameters)
         */
    ],
    "produccion" => [
        "https://webservices.chubblatinamerica.com/ExternalCmsWs/ExternalWSCMS.svc?singlewsdl", //CMS
        /*
         *Array
            (
                [0] => GetSplitInfoResponse GetSplitInfo(GetSplitInfo $parameters)
                [1] => GetMessagesByCategoryResponse GetMessagesByCategory(GetMessagesByCategory $parameters)
            )
         */
        "https://webservices.chubblatinamerica.com/ExternalS6TransactionWS/S6TransactionExternal.svc?singlewsdl",  //s6Transacion
        /*
         *Array
            (
                [0] => ProcessTransactionResponse ProcessTransaction(ProcessTransaction $parameters)
                [1] => GetTransactionStatusResponse GetTransactionStatus(GetTransactionStatus $parameters)
            )
         */
        "https://s6broker-ext.acegroup.com/brokerservice.svc?singlewsdl",
        /*
         * Array
            (
                [0] => ValidateCreditTokenResponse ValidateCreditToken(ValidateCreditToken $parameters)
                [1] => ValidateDDATokenResponse ValidateDDAToken(ValidateDDAToken $parameters)
                [2] => ValidateGIROTokenResponse ValidateGIROToken(ValidateGIROToken $parameters)
                [3] => GetTokenForEncryptedValueResponse GetTokenForEncryptedValue(GetTokenForEncryptedValue $parameters)
                [4] => GetCardTokenForAmexGENTokenValueResponse GetCardTokenForAmexGENTokenValue(GetCardTokenForAmexGENTokenValue $parameters)
                [5] => GetCardTokenForAmexGENValueResponse GetCardTokenForAmexGENValue(GetCardTokenForAmexGENValue $parameters)
                [6] => GetIBANValueFromTokenValueResponse GetIBANValueFromTokenValue(GetIBANValueFromTokenValue $parameters)
                [7] => GetLogInfoSummaryResponse GetLogInfoSummary(GetLogInfoSummary $parameters)
                [8] => GetLogInfoDetailsResponse GetLogInfoDetails(GetLogInfoDetails $parameters)
                [9] => GetIBANValueFromTokenValue2Response GetIBANValueFromTokenValue2(GetIBANValueFromTokenValue2 $parameters)
            )
         */
        "https://webservices.chubblatinamerica.com/ExternalEcuadorListWS/RegistroCivil.svc?singlewsdl"
        /*
         * GetPersonInformationResponse GetPersonInformation(GetPersonInformation $parameters)
         */
    ]
];
/*
$generator = new \Wsdl2PhpGenerator\Generator();
$generator->generate(
    new \Wsdl2PhpGenerator\Config(array(
        'inputFile' => $urls["pruebas"][3],
        'outputDir' => './RegistroCivil'
    ))
);
*/

try{

    //action para ExternalWSCMS SplitInfo y GetMessageByCategory

    $action = "http://tempuri.org/IExternalWSCMS/GetSplitInfo";
    $action = "http://tempuri.org/IExternalWSCMS/GetMessagesByCategory";
    //action para S6TransactionExternal
//    $action = "http://tempuri.org/IS6TransactionExternal/ProcessTransaction";
    //action para el registro civil
//    $action = "http://tempuri.org/IEcuadorListWebserviceExternal/GetPersonInformation";

//    $action = $urls["pruebas"][0];
    $hashed = hash_hmac ("sha1", $action .$fecha , $secret, true );
    $signature = base64_encode($hashed);
//    $signature = $hashed;

    $header[] = new SoapHeader( $NAMESPACE , $ACCESS_KEY_NAME, $accessId, false );
    $header[] = new SoapHeader( $NAMESPACE , $TIME_STAMP_NAME, $fecha, false  );
    $header[] = new SoapHeader( $NAMESPACE , $SIGNATURE_NAME, $signature, false  );
    $header[] = new SoapHeader( $NAMESPACE , $VERSION_NAME, $VERSION_VALUE, false  );

//    GetSplitInfo Method (string accessKeyId, GetSplitInfoRequest request)

    $arrContextOptions=[
        "ssl"=> [
            "verify_peer"=>false,
            "verify_peer_name"=>false,
            'crypto_method' => STREAM_CRYPTO_METHOD_TLS_CLIENT
        ]
    ];

    $options = [
        'soap_version'=>SOAP_1_1,
        'exceptions'=>true,
        'trace'=>true,
        'cache_wsdl'=>WSDL_CACHE_NONE,
        'stream_context' => stream_context_create($arrContextOptions),
        'encoding' => 'UTF-8',
        'keep_alive' => false,

        'use' => SOAP_LITERAL,
        'style' => SOAP_DOCUMENT,
    ];

    print $urls["pruebas"][3]. "<br>";

    $client = new SoapClient($urls["pruebas"][0], $options);
    $client->__setSoapHeaders($header);
// GetSplitInfo
    /*
    $GetSplitInfoRequest = new GetSplitInfoRequest( );
    $GetSplitInfoRequest->setLanguageCode   (230);
    $GetSplitInfoRequest->setLoadBankInfo   (true);
    $GetSplitInfoRequest->setSplitKey       ("EC19006901"); //EC19000101

    $GetSplitInfo = new GetSplitInfo($accessId, $GetSplitInfoRequest);

    $result = $client->GetSplitInfo( $GetSplitInfo );

     print_r(
        $client->GetSplitInfo( $GetSplitInfo )
    );
*/

    $GetMessagesByCategoryRequest = new GetMessagesByCategoryRequest();
    $GetMessagesByCategoryRequest->setCategoryCode("S6");
    $GetMessagesByCategoryRequest->setCountryCode("EC");
    $GetMessagesByCategoryRequest->setLanguageCode(230);
    $GetMessagesByCategory = new GetMessagesByCategory($accessId, $GetMessagesByCategoryRequest);


    print_r(
        $client->GetMessagesByCategory($GetMessagesByCategory)
    );

/*
//registro civil
    $client = new SoapClient($urls["pruebas"][3], $options);
    $client->__setSoapHeaders($header);

    //$GetPersonInformation = new GetPersonInformation( $accessId, "0401356035" ); Jaime
    $GetPersonInformation = new GetPersonInformation( $accessId, "1750402610" );

    print_r(
        $client->GetPersonInformation( $GetPersonInformation )
    );
*/

    echo "REQUEST:\n" . $client->__getLastRequest() . "\n";
} catch (Exception $e) {
    echo "<h2>Exception Error!</h2>";
    echo $e -> getMessage () . "\n";
    echo "REQUEST:\n" . $client->__getLastRequest() . "\n";
}




?>