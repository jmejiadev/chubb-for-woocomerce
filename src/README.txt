URLs con sus respectivos métodos

$urls = [
    "pruebas" => [
        "https://testwebservices.chubblatinamerica.com/ExternalCmsWsUAT/ExternalWSCMS.svc?singlewsdl", //CMS
        /*
         *Array
            (
                [0] => GetSplitInfoResponse GetSplitInfo(GetSplitInfo $parameters)
                [1] => GetMessagesByCategoryResponse GetMessagesByCategory(GetMessagesByCategory $parameters)
            )
         */
        "https://testwebservices.chubblatinamerica.com/ExternalS6TransactionWSUAT/S6TransactionExternal.svc?singlewsdl", //s6Transacion
        /*
         *Array
            (
                [0] => ProcessTransactionResponse ProcessTransaction(ProcessTransaction $parameters)
                [1] => GetTransactionStatusResponse GetTransactionStatus(GetTransactionStatus $parameters)
            )
         */
        "https://s6broker-ext-uat.acegroup.com/brokerservice.svc?singlewsdl",
        /*
         * Array
            (
                [0] => ValidateCreditTokenResponse ValidateCreditToken(ValidateCreditToken $parameters)
                [1] => ValidateDDATokenResponse ValidateDDAToken(ValidateDDAToken $parameters)
                [2] => ValidateGIROTokenResponse ValidateGIROToken(ValidateGIROToken $parameters)
                [3] => GetTokenForEncryptedValueResponse GetTokenForEncryptedValue(GetTokenForEncryptedValue $parameters)
                [4] => GetCardTokenForAmexGENTokenValueResponse GetCardTokenForAmexGENTokenValue(GetCardTokenForAmexGENTokenValue $parameters)
                [5] => GetCardTokenForAmexGENValueResponse GetCardTokenForAmexGENValue(GetCardTokenForAmexGENValue $parameters)
                [6] => GetIBANValueFromTokenValueResponse GetIBANValueFromTokenValue(GetIBANValueFromTokenValue $parameters)
                [7] => GetLogInfoSummaryResponse GetLogInfoSummary(GetLogInfoSummary $parameters)
                [8] => GetLogInfoDetailsResponse GetLogInfoDetails(GetLogInfoDetails $parameters)
                [9] => GetIBANValueFromTokenValue2Response GetIBANValueFromTokenValue2(GetIBANValueFromTokenValue2 $parameters)
            )
         */
        "https://testwebservices.chubblatinamerica.com/ExternalEcuadorListWSUAT/RegistroCivil.svc?singlewsdl",
        /*
         * GetPersonInformationResponse GetPersonInformation(GetPersonInformation $parameters)
         */
    ],
    "produccion" => [
        "https://webservices.chubblatinamerica.com/ExternalCmsWs/ExternalWSCMS.svc?singlewsdl", //CMS
        /*
         *Array
            (
                [0] => GetSplitInfoResponse GetSplitInfo(GetSplitInfo $parameters)
                [1] => GetMessagesByCategoryResponse GetMessagesByCategory(GetMessagesByCategory $parameters)
            )
         */
        "https://webservices.chubblatinamerica.com/ExternalS6TransactionWS/S6TransactionExternal.svc?singlewsdl",  //s6Transacion
        /*
         *Array
            (
                [0] => ProcessTransactionResponse ProcessTransaction(ProcessTransaction $parameters)
                [1] => GetTransactionStatusResponse GetTransactionStatus(GetTransactionStatus $parameters)
            )
         */
        "https://s6broker-ext.acegroup.com/brokerservice.svc?singlewsdl",
        /*
         * Array
            (
                [0] => ValidateCreditTokenResponse ValidateCreditToken(ValidateCreditToken $parameters)
                [1] => ValidateDDATokenResponse ValidateDDAToken(ValidateDDAToken $parameters)
                [2] => ValidateGIROTokenResponse ValidateGIROToken(ValidateGIROToken $parameters)
                [3] => GetTokenForEncryptedValueResponse GetTokenForEncryptedValue(GetTokenForEncryptedValue $parameters)
                [4] => GetCardTokenForAmexGENTokenValueResponse GetCardTokenForAmexGENTokenValue(GetCardTokenForAmexGENTokenValue $parameters)
                [5] => GetCardTokenForAmexGENValueResponse GetCardTokenForAmexGENValue(GetCardTokenForAmexGENValue $parameters)
                [6] => GetIBANValueFromTokenValueResponse GetIBANValueFromTokenValue(GetIBANValueFromTokenValue $parameters)
                [7] => GetLogInfoSummaryResponse GetLogInfoSummary(GetLogInfoSummary $parameters)
                [8] => GetLogInfoDetailsResponse GetLogInfoDetails(GetLogInfoDetails $parameters)
                [9] => GetIBANValueFromTokenValue2Response GetIBANValueFromTokenValue2(GetIBANValueFromTokenValue2 $parameters)
            )
         */
        "https://webservices.chubblatinamerica.com/ExternalEcuadorListWS/RegistroCivil.svc?singlewsdl"
        /*
         * GetPersonInformationResponse GetPersonInformation(GetPersonInformation $parameters)
         */
    ]
];

ACTIONS
    ExternalWSCMS
    http://tempuri.org/IExternalWSCMS/GetSplitInfo
    http://tempuri.org/IExternalWSCMS/GetMessagesByCategory

    S6TransactionExternal
    http://tempuri.org/IS6TransactionExternal/ProcessTransaction

    Registro civil
    http://tempuri.org/IEcuadorListWebserviceExternal/GetPersonInformation

//split
Array
(
    [GetSplitInfoResult] => Array
        (
            [ErrorCode] =>
            [ErrorDescription] =>
            [HasError] =>
            [SplitInfo] => Array
                (
                    [splitKey] => EC19006901
                    [splitDescription] => AGRICARE
                    [displayStatusCode] => 1556
                    [sponsorCode] => DS8
                    [splitType] => 01
                    [sponsorName] => EC - Agrizone
                    [prodPackage] => AGRICA
                    [prodPackageDescription] => AGRICARE
                    [prodPackageMarketingDesc] => AGRICARE
                    [policyPrefix] => AGR
                    [billNow] =>
                    [reconcileNow] =>
                    [currencyCode] => 55
                    [currencyDescription] => US Dollar
                    [lob] => 3712
                    [lobDescription] => A&H - Digital - Direct to Customer
                    [paymentFrequencies] => Array
                        (
                            [paymentFrequency] => Array
                                (
                                    [paymentFrequencyCode] => 1909
                                    [paymentFrequencyName] => Single Payment
                                )

                        )

                    [payments] => Array
                        (
                            [payment] => Array
                                (
                                    [paymentMethodCode] => 885
                                    [paymentMethodDescription] => Bordereaux
                                    [collectors] => Array
                                        (
                                            [collector] => Array
                                                (
                                                    [collectorCode] => AZE
                                                    [collectorName] => EC - Agrizone
                                                    [creditCards] => Array
                                                        (
                                                        )

                                                    [banks] => Array
                                                        (
                                                        )

                                                    [inactiveDate] =>
                                                )

                                        )

                                    [inactiveDate] =>
                                )

                        )

                    [products] => Array
                        (
                            [product] => Array
                                (
                                    [productId] => AGRICA
                                    [productDescription] => AGRICARE
                                    [required] => 1
                                    [individualRated] =>
                                    [allowBeneficiaries] => 1
                                    [allowedInsured] => 3
                                    [allowedCoverage] => 3
                                    [miMinAge] => 18
                                    [miMaxAge] => 65
                                    [spMinAge] => 18
                                    [spMaxAge] => 65
                                    [dpMinAge] => 0
                                    [dpMaxAge] => 0
                                    [unitRated] =>
                                    [minUnit] => 1
                                    [maxUnit] => 1
                                    [benefitLevels] => Array
                                        (
                                            [benefitLevel] => Array
                                                (
                                                    [benefitLevelCode] => 1
                                                    [benefitLevelDescription] => Benefit Level 1
                                                    [premiums] => Array
                                                        (
                                                            [premium] => Array
                                                                (
                                                                    [0] => Array
                                                                        (
                                                                            [individual] => MO
                                                                            [premiumValue] => 3
                                                                        )

                                                                    [1] => Array
                                                                        (
                                                                            [individual] => MS
                                                                            [premiumValue] => 5.5
                                                                        )

                                                                )

                                                        )

                                                    [MAX_BEN_AMT_MI] => 1500
                                                    [MAX_BEN_AMT_SP] => 1000
                                                    [MAX_BEN_AMT_DP] => 0
                                                )

                                        )

                                    [productSpecifics] => Array
                                        (
                                        )

                                    [ratingFactors] => Array
                                        (
                                        )

                                    [currencyCode] => 55
                                    [currencyDescription] => US Dollar
                                    [requiredProducts] =>
                                    [isSinglePremium] => 1
                                    [scheduledBenefits] => Array
                                        (
                                            [ProductScheduledBenefit] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => MI
                                                            [BenefitCode] => 0042
                                                            [BenefitDescription] => Muerte Accidental
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [Amount] => 300
                                                        )

                                                    [1] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => MI
                                                            [BenefitCode] => 3445
                                                            [BenefitDescription] => Asistencia NESEC
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [Amount] => 0
                                                        )

                                                    [2] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => MI
                                                            [BenefitCode] => 7057
                                                            [BenefitDescription] => Gastos Médicos por Accidente
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [Amount] => 1500
                                                        )

                                                    [3] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => SP
                                                            [BenefitCode] => 0042
                                                            [BenefitDescription] => Muerte Accidental
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [Amount] => 200
                                                        )

                                                    [4] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => SP
                                                            [BenefitCode] => 3445
                                                            [BenefitDescription] => Asistencia NESEC
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [Amount] => 0
                                                        )

                                                    [5] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => SP
                                                            [BenefitCode] => 7057
                                                            [BenefitDescription] => Gastos Médicos por Accidente
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [Amount] => 1000
                                                        )

                                                )

                                        )

                                    [coverages] => Array
                                        (
                                            [ProductCoverage] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [CoverageCode] => AA0001
                                                            [CoverageDescription] => (24 Hour) Accidental Death
                                                            [Individual] => MO
                                                            [MI_CVG_AMT] => 1500
                                                            [MI_TAX_BEN_AMT] => 0
                                                            [SPS_CVG_AMT] => 1000
                                                            [SP_TAX_BEN_AMT] => 0
                                                            [DEP_CVG_AMT] => 0
                                                            [DP_TAX_BEN_AMT] => 0
                                                            [MALE_PCT] => 100
                                                            [FEMALE_PCT] => 100
                                                            [UNISEX_PCT] => 100
                                                        )

                                                    [1] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [CoverageCode] => AA0001
                                                            [CoverageDescription] => (24 Hour) Accidental Death
                                                            [Individual] => MS
                                                            [MI_CVG_AMT] => 1500
                                                            [MI_TAX_BEN_AMT] => 0
                                                            [SPS_CVG_AMT] => 1000
                                                            [SP_TAX_BEN_AMT] => 0
                                                            [DEP_CVG_AMT] => 0
                                                            [DP_TAX_BEN_AMT] => 0
                                                            [MALE_PCT] => 100
                                                            [FEMALE_PCT] => 100
                                                            [UNISEX_PCT] => 100
                                                        )

                                                )

                                        )

                                    [premiums] => Array
                                        (
                                            [ProductPremium] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => MO
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Premium] => 3
                                                        )

                                                    [1] => Array
                                                        (
                                                            [BenefitLevelID] => 1
                                                            [Individual] => MS
                                                            [Rf_Combo_SeqNum] => 0
                                                            [ReferenceCode] => 0
                                                            [ReferenceDescription] =>
                                                            [CategoryCode] =>
                                                            [CategoryDescription] =>
                                                            [Premium] => 5.5
                                                        )

                                                )

                                        )

                                    [validateAge] => 1
                                )

                        )

                    [primaryMediaCode] => 55
                    [primaryMediaDescription] =>
                    [marketingApproachCode] => 868
                    [marketingApproachDescription] => Acquisition
                    [plannedLaunchDate] => 2019-09-01T00:00:00
                    [solicitationCostRequired] =>
                    [policyTransactionCode] => 01
                    [policyEffectiveDate] =>
                    [minimumProductNumberRequired] => 1
                    [maximumProductNumberRequired] => 1
                    [premiumInfoLink] =>
                    [CampaignStatus] => 916
                    [ApprovedCode] => 852
                    [actualLaunchDate] => 2019-09-01T00:00:00
                    [policyTransactionDescription] => New
                    [sourceListDescription] => Roll Out
                )

        )

)