<?php

class paymentInfo extends S6TransactionBase
{

    /**
     * @var int $payMethod
     */
    protected $payMethod = null;

    /**
     * @var int $payFreq
     */
    protected $payFreq = null;

    /**
     * @var int $taxAppCd
     */
    protected $taxAppCd = null;

    /**
     * @var string $nameOnBill
     */
    protected $nameOnBill = null;

    /**
     * @var int $taxJurisCd
     */
    protected $taxJurisCd = null;

    /**
     * @var string $employeeNum
     */
    protected $employeeNum = null;

    /**
     * @var int $debitDay
     */
    protected $debitDay = null;

    /**
     * @var string $billAccNum
     */
    protected $billAccNum = null;

    /**
     * @var string $bankCd
     */
    protected $bankCd = null;

    /**
     * @var string $branchCd
     */
    protected $branchCd = null;

    /**
     * @var string $ccCd
     */
    protected $ccCd = null;

    /**
     * @var string $expDate
     */
    protected $expDate = null;

    /**
     * @var string $collCd
     */
    protected $collCd = null;

    /**
     * @var string $Last5BillAccNum
     */
    protected $Last5BillAccNum = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getPayMethod()
    {
      return $this->payMethod;
    }

    /**
     * @param int $payMethod
     * @return paymentInfo
     */
    public function setPayMethod($payMethod)
    {
      $this->payMethod = $payMethod;
      return $this;
    }

    /**
     * @return int
     */
    public function getPayFreq()
    {
      return $this->payFreq;
    }

    /**
     * @param int $payFreq
     * @return paymentInfo
     */
    public function setPayFreq($payFreq)
    {
      $this->payFreq = $payFreq;
      return $this;
    }

    /**
     * @return int
     */
    public function getTaxAppCd()
    {
      return $this->taxAppCd;
    }

    /**
     * @param int $taxAppCd
     * @return paymentInfo
     */
    public function setTaxAppCd($taxAppCd)
    {
      $this->taxAppCd = $taxAppCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getNameOnBill()
    {
      return $this->nameOnBill;
    }

    /**
     * @param string $nameOnBill
     * @return paymentInfo
     */
    public function setNameOnBill($nameOnBill)
    {
      $this->nameOnBill = $nameOnBill;
      return $this;
    }

    /**
     * @return int
     */
    public function getTaxJurisCd()
    {
      return $this->taxJurisCd;
    }

    /**
     * @param int $taxJurisCd
     * @return paymentInfo
     */
    public function setTaxJurisCd($taxJurisCd)
    {
      $this->taxJurisCd = $taxJurisCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmployeeNum()
    {
      return $this->employeeNum;
    }

    /**
     * @param string $employeeNum
     * @return paymentInfo
     */
    public function setEmployeeNum($employeeNum)
    {
      $this->employeeNum = $employeeNum;
      return $this;
    }

    /**
     * @return int
     */
    public function getDebitDay()
    {
      return $this->debitDay;
    }

    /**
     * @param int $debitDay
     * @return paymentInfo
     */
    public function setDebitDay($debitDay)
    {
      $this->debitDay = $debitDay;
      return $this;
    }

    /**
     * @return string
     */
    public function getBillAccNum()
    {
      return $this->billAccNum;
    }

    /**
     * @param string $billAccNum
     * @return paymentInfo
     */
    public function setBillAccNum($billAccNum)
    {
      $this->billAccNum = $billAccNum;
      return $this;
    }

    /**
     * @return string
     */
    public function getBankCd()
    {
      return $this->bankCd;
    }

    /**
     * @param string $bankCd
     * @return paymentInfo
     */
    public function setBankCd($bankCd)
    {
      $this->bankCd = $bankCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getBranchCd()
    {
      return $this->branchCd;
    }

    /**
     * @param string $branchCd
     * @return paymentInfo
     */
    public function setBranchCd($branchCd)
    {
      $this->branchCd = $branchCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getCcCd()
    {
      return $this->ccCd;
    }

    /**
     * @param string $ccCd
     * @return paymentInfo
     */
    public function setCcCd($ccCd)
    {
      $this->ccCd = $ccCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getExpDate()
    {
      return $this->expDate;
    }

    /**
     * @param string $expDate
     * @return paymentInfo
     */
    public function setExpDate($expDate)
    {
      $this->expDate = $expDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollCd()
    {
      return $this->collCd;
    }

    /**
     * @param string $collCd
     * @return paymentInfo
     */
    public function setCollCd($collCd)
    {
      $this->collCd = $collCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getLast5BillAccNum()
    {
      return $this->Last5BillAccNum;
    }

    /**
     * @param string $Last5BillAccNum
     * @return paymentInfo
     */
    public function setLast5BillAccNum($Last5BillAccNum)
    {
      $this->Last5BillAccNum = $Last5BillAccNum;
      return $this;
    }

}
