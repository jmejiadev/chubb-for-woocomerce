<?php

class ArrayOfsuspReason implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var suspReason[] $suspReason
     */
    protected $suspReason = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return suspReason[]
     */
    public function getSuspReason()
    {
      return $this->suspReason;
    }

    /**
     * @param suspReason[] $suspReason
     * @return ArrayOfsuspReason
     */
    public function setSuspReason(array $suspReason = null)
    {
      $this->suspReason = $suspReason;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->suspReason[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return suspReason
     */
    public function offsetGet($offset)
    {
      return $this->suspReason[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param suspReason $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->suspReason[] = $value;
      } else {
        $this->suspReason[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->suspReason[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return suspReason Return the current element
     */
    public function current()
    {
      return current($this->suspReason);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->suspReason);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->suspReason);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->suspReason);
    }

    /**
     * Countable implementation
     *
     * @return suspReason Return count of elements
     */
    public function count()
    {
      return count($this->suspReason);
    }

}
