<?php

class suspReason extends S6TransactionBase
{

    /**
     * @var int $suspReasonKey
     */
    protected $suspReasonKey = null;

    /**
     * @var int $suspReasonCd
     */
    protected $suspReasonCd = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getSuspReasonKey()
    {
      return $this->suspReasonKey;
    }

    /**
     * @param int $suspReasonKey
     * @return suspReason
     */
    public function setSuspReasonKey($suspReasonKey)
    {
      $this->suspReasonKey = $suspReasonKey;
      return $this;
    }

    /**
     * @return int
     */
    public function getSuspReasonCd()
    {
      return $this->suspReasonCd;
    }

    /**
     * @param int $suspReasonCd
     * @return suspReason
     */
    public function setSuspReasonCd($suspReasonCd)
    {
      $this->suspReasonCd = $suspReasonCd;
      return $this;
    }

}
