<?php

class GetTransactionStatus
{

    /**
     * @var string $accessKeyId
     */
    protected $accessKeyId = null;

    /**
     * @var string $transactionId
     */
    protected $transactionId = null;

    /**
     * @param string $accessKeyId
     * @param string $transactionId
     */
    public function __construct($accessKeyId, $transactionId)
    {
      $this->accessKeyId = $accessKeyId;
      $this->transactionId = $transactionId;
    }

    /**
     * @return string
     */
    public function getAccessKeyId()
    {
      return $this->accessKeyId;
    }

    /**
     * @param string $accessKeyId
     * @return GetTransactionStatus
     */
    public function setAccessKeyId($accessKeyId)
    {
      $this->accessKeyId = $accessKeyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
      return $this->transactionId;
    }

    /**
     * @param string $transactionId
     * @return GetTransactionStatus
     */
    public function setTransactionId($transactionId)
    {
      $this->transactionId = $transactionId;
      return $this;
    }

}
