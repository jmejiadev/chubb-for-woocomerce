<?php

class reference extends S6TransactionBase
{

    /**
     * @var int $refKey
     */
    protected $refKey = null;

    /**
     * @var string $refText
     */
    protected $refText = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getRefKey()
    {
      return $this->refKey;
    }

    /**
     * @param int $refKey
     * @return reference
     */
    public function setRefKey($refKey)
    {
      $this->refKey = $refKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getRefText()
    {
      return $this->refText;
    }

    /**
     * @param string $refText
     * @return reference
     */
    public function setRefText($refText)
    {
      $this->refText = $refText;
      return $this;
    }

}
