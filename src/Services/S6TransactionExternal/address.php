<?php

class address extends S6TransactionBase
{

    /**
     * @var string $addrId
     */
    protected $addrId = null;

    /**
     * @var int $addrType
     */
    protected $addrType = null;

    /**
     * @var string $s6AddrId
     */
    protected $s6AddrId = null;

    /**
     * @var string $line1
     */
    protected $line1 = null;

    /**
     * @var string $line2
     */
    protected $line2 = null;

    /**
     * @var string $line3
     */
    protected $line3 = null;

    /**
     * @var string $line4
     */
    protected $line4 = null;

    /**
     * @var string $postalCd
     */
    protected $postalCd = null;

    /**
     * @var string $city
     */
    protected $city = null;

    /**
     * @var int $provinceCd
     */
    protected $provinceCd = null;

    /**
     * @var string $countryCd
     */
    protected $countryCd = null;

    /**
     * @var string $phone
     */
    protected $phone = null;

    /**
     * @var string $fax
     */
    protected $fax = null;

    /**
     * @var string $companyCd
     */
    protected $companyCd = null;

    /**
     * @var string $cell
     */
    protected $cell = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getAddrId()
    {
      return $this->addrId;
    }

    /**
     * @param string $addrId
     * @return address
     */
    public function setAddrId($addrId)
    {
      $this->addrId = $addrId;
      return $this;
    }

    /**
     * @return int
     */
    public function getAddrType()
    {
      return $this->addrType;
    }

    /**
     * @param int $addrType
     * @return address
     */
    public function setAddrType($addrType)
    {
      $this->addrType = $addrType;
      return $this;
    }

    /**
     * @return string
     */
    public function getS6AddrId()
    {
      return $this->s6AddrId;
    }

    /**
     * @param string $s6AddrId
     * @return address
     */
    public function setS6AddrId($s6AddrId)
    {
      $this->s6AddrId = $s6AddrId;
      return $this;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
      return $this->line1;
    }

    /**
     * @param string $line1
     * @return address
     */
    public function setLine1($line1)
    {
      $this->line1 = $line1;
      return $this;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
      return $this->line2;
    }

    /**
     * @param string $line2
     * @return address
     */
    public function setLine2($line2)
    {
      $this->line2 = $line2;
      return $this;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
      return $this->line3;
    }

    /**
     * @param string $line3
     * @return address
     */
    public function setLine3($line3)
    {
      $this->line3 = $line3;
      return $this;
    }

    /**
     * @return string
     */
    public function getLine4()
    {
      return $this->line4;
    }

    /**
     * @param string $line4
     * @return address
     */
    public function setLine4($line4)
    {
      $this->line4 = $line4;
      return $this;
    }

    /**
     * @return string
     */
    public function getPostalCd()
    {
      return $this->postalCd;
    }

    /**
     * @param string $postalCd
     * @return address
     */
    public function setPostalCd($postalCd)
    {
      $this->postalCd = $postalCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->city;
    }

    /**
     * @param string $city
     * @return address
     */
    public function setCity($city)
    {
      $this->city = $city;
      return $this;
    }

    /**
     * @return int
     */
    public function getProvinceCd()
    {
      return $this->provinceCd;
    }

    /**
     * @param int $provinceCd
     * @return address
     */
    public function setProvinceCd($provinceCd)
    {
      $this->provinceCd = $provinceCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCd()
    {
      return $this->countryCd;
    }

    /**
     * @param string $countryCd
     * @return address
     */
    public function setCountryCd($countryCd)
    {
      $this->countryCd = $countryCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
      return $this->phone;
    }

    /**
     * @param string $phone
     * @return address
     */
    public function setPhone($phone)
    {
      $this->phone = $phone;
      return $this;
    }

    /**
     * @return string
     */
    public function getFax()
    {
      return $this->fax;
    }

    /**
     * @param string $fax
     * @return address
     */
    public function setFax($fax)
    {
      $this->fax = $fax;
      return $this;
    }

    /**
     * @return string
     */
    public function getCompanyCd()
    {
      return $this->companyCd;
    }

    /**
     * @param string $companyCd
     * @return address
     */
    public function setCompanyCd($companyCd)
    {
      $this->companyCd = $companyCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getCell()
    {
      return $this->cell;
    }

    /**
     * @param string $cell
     * @return address
     */
    public function setCell($cell)
    {
      $this->cell = $cell;
      return $this;
    }

}
