<?php

class s6TransactionResult
{

    /**
     * @var string $s6TransactionID
     */
    protected $s6TransactionID = null;

    /**
     * @var string $policyNumber
     */
    protected $policyNumber = null;

    /**
     * @var s6TransactionStatus $statusCode
     */
    protected $statusCode = null;

    /**
     * @var string $rejectionCode
     */
    protected $rejectionCode = null;

    /**
     * @var string $rejectionReason
     */
    protected $rejectionReason = null;

    /**
     * @var string $rejectionDetails
     */
    protected $rejectionDetails = null;

    /**
     * @var string $botlTransaction
     */
    protected $botlTransaction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getS6TransactionID()
    {
      return $this->s6TransactionID;
    }

    /**
     * @param string $s6TransactionID
     * @return s6TransactionResult
     */
    public function setS6TransactionID($s6TransactionID)
    {
      $this->s6TransactionID = $s6TransactionID;
      return $this;
    }

    /**
     * @return string
     */
    public function getPolicyNumber()
    {
      return $this->policyNumber;
    }

    /**
     * @param string $policyNumber
     * @return s6TransactionResult
     */
    public function setPolicyNumber($policyNumber)
    {
      $this->policyNumber = $policyNumber;
      return $this;
    }

    /**
     * @return s6TransactionStatus
     */
    public function getStatusCode()
    {
      return $this->statusCode;
    }

    /**
     * @param s6TransactionStatus $statusCode
     * @return s6TransactionResult
     */
    public function setStatusCode($statusCode)
    {
      $this->statusCode = $statusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRejectionCode()
    {
      return $this->rejectionCode;
    }

    /**
     * @param string $rejectionCode
     * @return s6TransactionResult
     */
    public function setRejectionCode($rejectionCode)
    {
      $this->rejectionCode = $rejectionCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getRejectionReason()
    {
      return $this->rejectionReason;
    }

    /**
     * @param string $rejectionReason
     * @return s6TransactionResult
     */
    public function setRejectionReason($rejectionReason)
    {
      $this->rejectionReason = $rejectionReason;
      return $this;
    }

    /**
     * @return string
     */
    public function getRejectionDetails()
    {
      return $this->rejectionDetails;
    }

    /**
     * @param string $rejectionDetails
     * @return s6TransactionResult
     */
    public function setRejectionDetails($rejectionDetails)
    {
      $this->rejectionDetails = $rejectionDetails;
      return $this;
    }

    /**
     * @return string
     */
    public function getBotlTransaction()
    {
      return $this->botlTransaction;
    }

    /**
     * @param string $botlTransaction
     * @return s6TransactionResult
     */
    public function setBotlTransaction($botlTransaction)
    {
      $this->botlTransaction = $botlTransaction;
      return $this;
    }

}
