<?php

class product extends S6TransactionBase
{

    /**
     * @var string $prodCd
     */
    protected $prodCd = null;

    /**
     * @var int $coverageCd
     */
    protected $coverageCd = null;

    /**
     * @var int $depNum
     */
    protected $depNum = null;

    /**
     * @var float $premiumOverAmt
     */
    protected $premiumOverAmt = null;

    /**
     * @var string $premiumOverExp
     */
    protected $premiumOverExp = null;

    /**
     * @var int $premiumOverRs
     */
    protected $premiumOverRs = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getProdCd()
    {
      return $this->prodCd;
    }

    /**
     * @param string $prodCd
     * @return product
     */
    public function setProdCd($prodCd)
    {
      $this->prodCd = $prodCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getCoverageCd()
    {
      return $this->coverageCd;
    }

    /**
     * @param int $coverageCd
     * @return product
     */
    public function setCoverageCd($coverageCd)
    {
      $this->coverageCd = $coverageCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getDepNum()
    {
      return $this->depNum;
    }

    /**
     * @param int $depNum
     * @return product
     */
    public function setDepNum($depNum)
    {
      $this->depNum = $depNum;
      return $this;
    }

    /**
     * @return float
     */
    public function getPremiumOverAmt()
    {
      return $this->premiumOverAmt;
    }

    /**
     * @param float $premiumOverAmt
     * @return product
     */
    public function setPremiumOverAmt($premiumOverAmt)
    {
      $this->premiumOverAmt = $premiumOverAmt;
      return $this;
    }

    /**
     * @return string
     */
    public function getPremiumOverExp()
    {
      return $this->premiumOverExp;
    }

    /**
     * @param string $premiumOverExp
     * @return product
     */
    public function setPremiumOverExp($premiumOverExp)
    {
      $this->premiumOverExp = $premiumOverExp;
      return $this;
    }

    /**
     * @return int
     */
    public function getPremiumOverRs()
    {
      return $this->premiumOverRs;
    }

    /**
     * @param int $premiumOverRs
     * @return product
     */
    public function setPremiumOverRs($premiumOverRs)
    {
      $this->premiumOverRs = $premiumOverRs;
      return $this;
    }

}
