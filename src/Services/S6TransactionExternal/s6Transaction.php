<?php

class s6Transaction extends S6TransactionBase
{

    /**
     * @var string $tranType
     */
    protected $tranType = null;

    /**
     * @var string $chgType
     */
    protected $chgType = null;

    /**
     * @var int $lineNum
     */
    protected $lineNum = null;

    /**
     * @var string $polNum
     */
    protected $polNum = null;

    /**
     * @var string $countryCd
     */
    protected $countryCd = null;

    /**
     * @var string $campaign
     */
    protected $campaign = null;

    /**
     * @var float $refAmount
     */
    protected $refAmount = null;

    /**
     * @var paymentInfo $paymentInfo
     */
    protected $paymentInfo = null;

    /**
     * @var ArrayOfproduct $products
     */
    protected $products = null;

    /**
     * @var string $sellerId
     */
    protected $sellerId = null;

    /**
     * @var \DateTime $appDate
     */
    protected $appDate = null;

    /**
     * @var string $appNum
     */
    protected $appNum = null;

    /**
     * @var \DateTime $efftDate
     */
    protected $efftDate = null;

    /**
     * @var string $repPol
     */
    protected $repPol = null;

    /**
     * @var correspondenceType $correspondence
     */
    protected $correspondence = null;

    /**
     * @var string $tranNote
     */
    protected $tranNote = null;

    /**
     * @var \DateTime $polExpDate
     */
    protected $polExpDate = null;

    /**
     * @var int $bill
     */
    protected $bill = null;

    /**
     * @var int $cancelReason
     */
    protected $cancelReason = null;

    /**
     * @var int $endorsReason
     */
    protected $endorsReason = null;

    /**
     * @var string $providerId
     */
    protected $providerId = null;

    /**
     * @var ArrayOfcustomer $customers
     */
    protected $customers = null;

    /**
     * @var ArrayOfaddress $addresses
     */
    protected $addresses = null;

    /**
     * @var ArrayOfreference $references
     */
    protected $references = null;

    /**
     * @var ArrayOfsuspReason $suspensionsReason
     */
    protected $suspensionsReason = null;

    /**
     * @var float $premiumCheck
     */
    protected $premiumCheck = null;

    /**
     * @var string $diaryMessage
     */
    protected $diaryMessage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getTranType()
    {
      return $this->tranType;
    }

    /**
     * @param string $tranType
     * @return s6Transaction
     */
    public function setTranType($tranType)
    {
      $this->tranType = $tranType;
      return $this;
    }

    /**
     * @return string
     */
    public function getChgType()
    {
      return $this->chgType;
    }

    /**
     * @param string $chgType
     * @return s6Transaction
     */
    public function setChgType($chgType)
    {
      $this->chgType = $chgType;
      return $this;
    }

    /**
     * @return int
     */
    public function getLineNum()
    {
      return $this->lineNum;
    }

    /**
     * @param int $lineNum
     * @return s6Transaction
     */
    public function setLineNum($lineNum)
    {
      $this->lineNum = $lineNum;
      return $this;
    }

    /**
     * @return string
     */
    public function getPolNum()
    {
      return $this->polNum;
    }

    /**
     * @param string $polNum
     * @return s6Transaction
     */
    public function setPolNum($polNum)
    {
      $this->polNum = $polNum;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCd()
    {
      return $this->countryCd;
    }

    /**
     * @param string $countryCd
     * @return s6Transaction
     */
    public function setCountryCd($countryCd)
    {
      $this->countryCd = $countryCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getCampaign()
    {
      return $this->campaign;
    }

    /**
     * @param string $campaign
     * @return s6Transaction
     */
    public function setCampaign($campaign)
    {
      $this->campaign = $campaign;
      return $this;
    }

    /**
     * @return float
     */
    public function getRefAmount()
    {
      return $this->refAmount;
    }

    /**
     * @param float $refAmount
     * @return s6Transaction
     */
    public function setRefAmount($refAmount)
    {
      $this->refAmount = $refAmount;
      return $this;
    }

    /**
     * @return paymentInfo
     */
    public function getPaymentInfo()
    {
      return $this->paymentInfo;
    }

    /**
     * @param paymentInfo $paymentInfo
     * @return s6Transaction
     */
    public function setPaymentInfo($paymentInfo)
    {
      $this->paymentInfo = $paymentInfo;
      return $this;
    }

    /**
     * @return ArrayOfproduct
     */
    public function getProducts()
    {
      return $this->products;
    }

    /**
     * @param ArrayOfproduct $products
     * @return s6Transaction
     */
    public function setProducts($products)
    {
      $this->products = $products;
      return $this;
    }

    /**
     * @return string
     */
    public function getSellerId()
    {
      return $this->sellerId;
    }

    /**
     * @param string $sellerId
     * @return s6Transaction
     */
    public function setSellerId($sellerId)
    {
      $this->sellerId = $sellerId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAppDate()
    {
      if ($this->appDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->appDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $appDate
     * @return s6Transaction
     */
    public function setAppDate(\DateTime $appDate = null)
    {
      if ($appDate == null) {
       $this->appDate = null;
      } else {
        $this->appDate = $appDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getAppNum()
    {
      return $this->appNum;
    }

    /**
     * @param string $appNum
     * @return s6Transaction
     */
    public function setAppNum($appNum)
    {
      $this->appNum = $appNum;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEfftDate()
    {
      if ($this->efftDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->efftDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $efftDate
     * @return s6Transaction
     */
    public function setEfftDate(\DateTime $efftDate = null)
    {
      if ($efftDate == null) {
       $this->efftDate = null;
      } else {
        $this->efftDate = $efftDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getRepPol()
    {
      return $this->repPol;
    }

    /**
     * @param string $repPol
     * @return s6Transaction
     */
    public function setRepPol($repPol)
    {
      $this->repPol = $repPol;
      return $this;
    }

    /**
     * @return correspondenceType
     */
    public function getCorrespondence()
    {
      return $this->correspondence;
    }

    /**
     * @param correspondenceType $correspondence
     * @return s6Transaction
     */
    public function setCorrespondence($correspondence)
    {
      $this->correspondence = $correspondence;
      return $this;
    }

    /**
     * @return string
     */
    public function getTranNote()
    {
      return $this->tranNote;
    }

    /**
     * @param string $tranNote
     * @return s6Transaction
     */
    public function setTranNote($tranNote)
    {
      $this->tranNote = $tranNote;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPolExpDate()
    {
      if ($this->polExpDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->polExpDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $polExpDate
     * @return s6Transaction
     */
    public function setPolExpDate(\DateTime $polExpDate = null)
    {
      if ($polExpDate == null) {
       $this->polExpDate = null;
      } else {
        $this->polExpDate = $polExpDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getBill()
    {
      return $this->bill;
    }

    /**
     * @param int $bill
     * @return s6Transaction
     */
    public function setBill($bill)
    {
      $this->bill = $bill;
      return $this;
    }

    /**
     * @return int
     */
    public function getCancelReason()
    {
      return $this->cancelReason;
    }

    /**
     * @param int $cancelReason
     * @return s6Transaction
     */
    public function setCancelReason($cancelReason)
    {
      $this->cancelReason = $cancelReason;
      return $this;
    }

    /**
     * @return int
     */
    public function getEndorsReason()
    {
      return $this->endorsReason;
    }

    /**
     * @param int $endorsReason
     * @return s6Transaction
     */
    public function setEndorsReason($endorsReason)
    {
      $this->endorsReason = $endorsReason;
      return $this;
    }

    /**
     * @return string
     */
    public function getProviderId()
    {
      return $this->providerId;
    }

    /**
     * @param string $providerId
     * @return s6Transaction
     */
    public function setProviderId($providerId)
    {
      $this->providerId = $providerId;
      return $this;
    }

    /**
     * @return ArrayOfcustomer
     */
    public function getCustomers()
    {
      return $this->customers;
    }

    /**
     * @param ArrayOfcustomer $customers
     * @return s6Transaction
     */
    public function setCustomers($customers)
    {
      $this->customers = $customers;
      return $this;
    }

    /**
     * @return ArrayOfaddress
     */
    public function getAddresses()
    {
      return $this->addresses;
    }

    /**
     * @param ArrayOfaddress $addresses
     * @return s6Transaction
     */
    public function setAddresses($addresses)
    {
      $this->addresses = $addresses;
      return $this;
    }

    /**
     * @return ArrayOfreference
     */
    public function getReferences()
    {
      return $this->references;
    }

    /**
     * @param ArrayOfreference $references
     * @return s6Transaction
     */
    public function setReferences($references)
    {
      $this->references = $references;
      return $this;
    }

    /**
     * @return ArrayOfsuspReason
     */
    public function getSuspensionsReason()
    {
      return $this->suspensionsReason;
    }

    /**
     * @param ArrayOfsuspReason $suspensionsReason
     * @return s6Transaction
     */
    public function setSuspensionsReason($suspensionsReason)
    {
      $this->suspensionsReason = $suspensionsReason;
      return $this;
    }

    /**
     * @return float
     */
    public function getPremiumCheck()
    {
      return $this->premiumCheck;
    }

    /**
     * @param float $premiumCheck
     * @return s6Transaction
     */
    public function setPremiumCheck($premiumCheck)
    {
      $this->premiumCheck = $premiumCheck;
      return $this;
    }

    /**
     * @return string
     */
    public function getDiaryMessage()
    {
      return $this->diaryMessage;
    }

    /**
     * @param string $diaryMessage
     * @return s6Transaction
     */
    public function setDiaryMessage($diaryMessage)
    {
      $this->diaryMessage = $diaryMessage;
      return $this;
    }

}
