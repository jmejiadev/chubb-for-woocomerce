<?php

class ProcessTransactionRequest
{

    /**
     * @var boolean $AllowDuplicate
     */
    protected $AllowDuplicate = null;

    /**
     * @var boolean $ShouldCommit
     */
    protected $ShouldCommit = null;

    /**
     * @var boolean $ShouldReplace
     */
    protected $ShouldReplace = null;

    /**
     * @var boolean $StoreInQueue
     */
    protected $StoreInQueue = null;

    /**
     * @var s6Transaction $Transaction
     */
    protected $Transaction = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return boolean
     */
    public function getAllowDuplicate()
    {
      return $this->AllowDuplicate;
    }

    /**
     * @param boolean $AllowDuplicate
     * @return ProcessTransactionRequest
     */
    public function setAllowDuplicate($AllowDuplicate)
    {
      $this->AllowDuplicate = $AllowDuplicate;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShouldCommit()
    {
      return $this->ShouldCommit;
    }

    /**
     * @param boolean $ShouldCommit
     * @return ProcessTransactionRequest
     */
    public function setShouldCommit($ShouldCommit)
    {
      $this->ShouldCommit = $ShouldCommit;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getShouldReplace()
    {
      return $this->ShouldReplace;
    }

    /**
     * @param boolean $ShouldReplace
     * @return ProcessTransactionRequest
     */
    public function setShouldReplace($ShouldReplace)
    {
      $this->ShouldReplace = $ShouldReplace;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getStoreInQueue()
    {
      return $this->StoreInQueue;
    }

    /**
     * @param boolean $StoreInQueue
     * @return ProcessTransactionRequest
     */
    public function setStoreInQueue($StoreInQueue)
    {
      $this->StoreInQueue = $StoreInQueue;
      return $this;
    }

    /**
     * @return s6Transaction
     */
    public function getTransaction()
    {
      return $this->Transaction;
    }

    /**
     * @param s6Transaction $Transaction
     * @return ProcessTransactionRequest
     */
    public function setTransaction($Transaction)
    {
      $this->Transaction = $Transaction;
      return $this;
    }

}
