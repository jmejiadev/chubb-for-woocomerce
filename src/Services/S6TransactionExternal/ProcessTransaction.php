<?php

class ProcessTransaction
{

    /**
     * @var string $accessKeyId
     */
    protected $accessKeyId = null;

    /**
     * @var ProcessTransactionRequest $request
     */
    protected $request = null;

    /**
     * @param string $accessKeyId
     * @param ProcessTransactionRequest $request
     */
    public function __construct($accessKeyId, $request)
    {
      $this->accessKeyId = $accessKeyId;
      $this->request = $request;
    }

    /**
     * @return string
     */
    public function getAccessKeyId()
    {
      return $this->accessKeyId;
    }

    /**
     * @param string $accessKeyId
     * @return ProcessTransaction
     */
    public function setAccessKeyId($accessKeyId)
    {
      $this->accessKeyId = $accessKeyId;
      return $this;
    }

    /**
     * @return ProcessTransactionRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param ProcessTransactionRequest $request
     * @return ProcessTransaction
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
