<?php

class ProcessTransactionResponse
{

    /**
     * @var TransactionResponse $ProcessTransactionResult
     */
    protected $ProcessTransactionResult = null;

    /**
     * @param TransactionResponse $ProcessTransactionResult
     */
    public function __construct($ProcessTransactionResult)
    {
      $this->ProcessTransactionResult = $ProcessTransactionResult;
    }

    /**
     * @return TransactionResponse
     */
    public function getProcessTransactionResult()
    {
      return $this->ProcessTransactionResult;
    }

    /**
     * @param TransactionResponse $ProcessTransactionResult
     * @return ProcessTransactionResponse
     */
    public function setProcessTransactionResult($ProcessTransactionResult)
    {
      $this->ProcessTransactionResult = $ProcessTransactionResult;
      return $this;
    }

}
