<?php

class beneficiary extends S6TransactionBase
{

    /**
     * @var string $notes
     */
    protected $notes = null;

    /**
     * @var ArrayOfbeneficiaryDetail $beneficiaries
     */
    protected $beneficiaries = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getNotes()
    {
      return $this->notes;
    }

    /**
     * @param string $notes
     * @return beneficiary
     */
    public function setNotes($notes)
    {
      $this->notes = $notes;
      return $this;
    }

    /**
     * @return ArrayOfbeneficiaryDetail
     */
    public function getBeneficiaries()
    {
      return $this->beneficiaries;
    }

    /**
     * @param ArrayOfbeneficiaryDetail $beneficiaries
     * @return beneficiary
     */
    public function setBeneficiaries($beneficiaries)
    {
      $this->beneficiaries = $beneficiaries;
      return $this;
    }

}
