<?php

class ArrayOfbeneficiaryDetail implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var beneficiaryDetail[] $beneficiaryDetail
     */
    protected $beneficiaryDetail = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return beneficiaryDetail[]
     */
    public function getBeneficiaryDetail()
    {
      return $this->beneficiaryDetail;
    }

    /**
     * @param beneficiaryDetail[] $beneficiaryDetail
     * @return ArrayOfbeneficiaryDetail
     */
    public function setBeneficiaryDetail(array $beneficiaryDetail = null)
    {
      $this->beneficiaryDetail = $beneficiaryDetail;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->beneficiaryDetail[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return beneficiaryDetail
     */
    public function offsetGet($offset)
    {
      return $this->beneficiaryDetail[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param beneficiaryDetail $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->beneficiaryDetail[] = $value;
      } else {
        $this->beneficiaryDetail[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->beneficiaryDetail[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return beneficiaryDetail Return the current element
     */
    public function current()
    {
      return current($this->beneficiaryDetail);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->beneficiaryDetail);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->beneficiaryDetail);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->beneficiaryDetail);
    }

    /**
     * Countable implementation
     *
     * @return beneficiaryDetail Return count of elements
     */
    public function count()
    {
      return count($this->beneficiaryDetail);
    }

}
