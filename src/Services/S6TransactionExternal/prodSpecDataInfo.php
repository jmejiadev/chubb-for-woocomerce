<?php

class prodSpecDataInfo extends S6TransactionBase
{

    /**
     * @var int $categ
     */
    protected $categ = null;

    /**
     * @var string $value
     */
    protected $value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getCateg()
    {
      return $this->categ;
    }

    /**
     * @param int $categ
     * @return prodSpecDataInfo
     */
    public function setCateg($categ)
    {
      $this->categ = $categ;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return prodSpecDataInfo
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
