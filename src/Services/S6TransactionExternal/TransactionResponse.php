<?php

class TransactionResponse
{

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    /**
     * @var s6TransactionResult $S6TransactionResult
     */
    protected $S6TransactionResult = null;

    /**
     * @var ResultStatusEnum $Status
     */
    protected $Status = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return TransactionResponse
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

    /**
     * @return s6TransactionResult
     */
    public function getS6TransactionResult()
    {
      return $this->S6TransactionResult;
    }

    /**
     * @param s6TransactionResult $S6TransactionResult
     * @return TransactionResponse
     */
    public function setS6TransactionResult($S6TransactionResult)
    {
      $this->S6TransactionResult = $S6TransactionResult;
      return $this;
    }

    /**
     * @return ResultStatusEnum
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param ResultStatusEnum $Status
     * @return TransactionResponse
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

}
