<?php

class ArrayOfprodSpecData implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var prodSpecData[] $prodSpecData
     */
    protected $prodSpecData = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return prodSpecData[]
     */
    public function getProdSpecData()
    {
      return $this->prodSpecData;
    }

    /**
     * @param prodSpecData[] $prodSpecData
     * @return ArrayOfprodSpecData
     */
    public function setProdSpecData(array $prodSpecData = null)
    {
      $this->prodSpecData = $prodSpecData;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->prodSpecData[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return prodSpecData
     */
    public function offsetGet($offset)
    {
      return $this->prodSpecData[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param prodSpecData $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->prodSpecData[] = $value;
      } else {
        $this->prodSpecData[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->prodSpecData[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return prodSpecData Return the current element
     */
    public function current()
    {
      return current($this->prodSpecData);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->prodSpecData);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->prodSpecData);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->prodSpecData);
    }

    /**
     * Countable implementation
     *
     * @return prodSpecData Return count of elements
     */
    public function count()
    {
      return count($this->prodSpecData);
    }

}
