<?php

class exclsCd
{

    /**
     * @var int[] $exclCd
     */
    protected $exclCd = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int[]
     */
    public function getExclCd()
    {
      return $this->exclCd;
    }

    /**
     * @param int[] $exclCd
     * @return exclsCd
     */
    public function setExclCd(array $exclCd = null)
    {
      $this->exclCd = $exclCd;
      return $this;
    }

}
