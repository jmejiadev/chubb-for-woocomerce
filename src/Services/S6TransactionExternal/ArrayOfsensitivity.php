<?php

class ArrayOfsensitivity implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var sensitivity[] $sensitivity
     */
    protected $sensitivity = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return sensitivity[]
     */
    public function getSensitivity()
    {
      return $this->sensitivity;
    }

    /**
     * @param sensitivity[] $sensitivity
     * @return ArrayOfsensitivity
     */
    public function setSensitivity(array $sensitivity = null)
    {
      $this->sensitivity = $sensitivity;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->sensitivity[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return sensitivity
     */
    public function offsetGet($offset)
    {
      return $this->sensitivity[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param sensitivity $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->sensitivity[] = $value;
      } else {
        $this->sensitivity[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->sensitivity[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return sensitivity Return the current element
     */
    public function current()
    {
      return current($this->sensitivity);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->sensitivity);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->sensitivity);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->sensitivity);
    }

    /**
     * Countable implementation
     *
     * @return sensitivity Return count of elements
     */
    public function count()
    {
      return count($this->sensitivity);
    }

}
