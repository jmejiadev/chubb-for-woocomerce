<?php

class S6TransactionExternal extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'ProcessTransaction' => '\\ProcessTransaction',
  'ProcessTransactionResponse' => '\\ProcessTransactionResponse',
  'GetTransactionStatus' => '\\GetTransactionStatus',
  'GetTransactionStatusResponse' => '\\GetTransactionStatusResponse',
  'ProcessTransactionRequest' => '\\ProcessTransactionRequest',
  'TransactionResponse' => '\\TransactionResponse',
  's6Transaction' => '\\s6Transaction',
  'S6TransactionBase' => '\\S6TransactionBase',
  'prodSpecDataInfo' => '\\prodSpecDataInfo',
  'beneficiaryDetail' => '\\beneficiaryDetail',
  'product' => '\\product',
  'customer' => '\\customer',
  'ArrayOfcustProd' => '\\ArrayOfcustProd',
  'custProd' => '\\custProd',
  'beneficiary' => '\\beneficiary',
  'ArrayOfbeneficiaryDetail' => '\\ArrayOfbeneficiaryDetail',
  'reference' => '\\reference',
  'address' => '\\address',
  'sensitivity' => '\\sensitivity',
  'suspReason' => '\\suspReason',
  'paymentInfo' => '\\paymentInfo',
  'ArrayOfprodSpecData' => '\\ArrayOfprodSpecData',
  'prodSpecData' => '\\prodSpecData',
  'ArrayOfprodSpecDataInfo' => '\\ArrayOfprodSpecDataInfo',
  'ratingsFactor' => '\\ratingsFactor',
  'benefitsRatingFactor' => '\\benefitsRatingFactor',
  'exclsCd' => '\\exclsCd',
  'custAdds' => '\\custAdds',
  'ArrayOfsensitivity' => '\\ArrayOfsensitivity',
  'ArrayOfproduct' => '\\ArrayOfproduct',
  'ArrayOfcustomer' => '\\ArrayOfcustomer',
  'ArrayOfaddress' => '\\ArrayOfaddress',
  'ArrayOfreference' => '\\ArrayOfreference',
  'ArrayOfsuspReason' => '\\ArrayOfsuspReason',
  's6TransactionResult' => '\\s6TransactionResult',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
    
  foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
      $options = array_merge(array (
  'features' => 1,
), $options);
      if (!$wsdl) {
        $wsdl = 'https://testwebservices.chubblatinamerica.com/ExternalS6TransactionWSUAT/S6TransactionExternal.svc?singlewsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param ProcessTransaction $parameters
     * @return ProcessTransactionResponse
     */
    public function ProcessTransaction(ProcessTransaction $parameters)
    {
      return $this->__soapCall('ProcessTransaction', array($parameters));
    }

    /**
     * @param GetTransactionStatus $parameters
     * @return GetTransactionStatusResponse
     */
    public function GetTransactionStatus(GetTransactionStatus $parameters)
    {
      return $this->__soapCall('GetTransactionStatus', array($parameters));
    }

}
