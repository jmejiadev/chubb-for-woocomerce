<?php

class custAdds
{

    /**
     * @var string[] $custAdd
     */
    protected $custAdd = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string[]
     */
    public function getCustAdd()
    {
      return $this->custAdd;
    }

    /**
     * @param string[] $custAdd
     * @return custAdds
     */
    public function setCustAdd(array $custAdd = null)
    {
      $this->custAdd = $custAdd;
      return $this;
    }

}
