<?php

class GetTransactionStatusResponse
{

    /**
     * @var TransactionResponse $GetTransactionStatusResult
     */
    protected $GetTransactionStatusResult = null;

    /**
     * @param TransactionResponse $GetTransactionStatusResult
     */
    public function __construct($GetTransactionStatusResult)
    {
      $this->GetTransactionStatusResult = $GetTransactionStatusResult;
    }

    /**
     * @return TransactionResponse
     */
    public function getGetTransactionStatusResult()
    {
      return $this->GetTransactionStatusResult;
    }

    /**
     * @param TransactionResponse $GetTransactionStatusResult
     * @return GetTransactionStatusResponse
     */
    public function setGetTransactionStatusResult($GetTransactionStatusResult)
    {
      $this->GetTransactionStatusResult = $GetTransactionStatusResult;
      return $this;
    }

}
