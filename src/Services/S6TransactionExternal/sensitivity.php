<?php

class sensitivity extends S6TransactionBase
{

    /**
     * @var int $senCd
     */
    protected $senCd = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getSenCd()
    {
      return $this->senCd;
    }

    /**
     * @param int $senCd
     * @return sensitivity
     */
    public function setSenCd($senCd)
    {
      $this->senCd = $senCd;
      return $this;
    }

}
