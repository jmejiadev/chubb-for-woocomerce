<?php

class ArrayOfcustProd implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var custProd[] $custProd
     */
    protected $custProd = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return custProd[]
     */
    public function getCustProd()
    {
      return $this->custProd;
    }

    /**
     * @param custProd[] $custProd
     * @return ArrayOfcustProd
     */
    public function setCustProd(array $custProd = null)
    {
      $this->custProd = $custProd;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->custProd[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return custProd
     */
    public function offsetGet($offset)
    {
      return $this->custProd[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param custProd $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->custProd[] = $value;
      } else {
        $this->custProd[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->custProd[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return custProd Return the current element
     */
    public function current()
    {
      return current($this->custProd);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->custProd);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->custProd);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->custProd);
    }

    /**
     * Countable implementation
     *
     * @return custProd Return count of elements
     */
    public function count()
    {
      return count($this->custProd);
    }

}
