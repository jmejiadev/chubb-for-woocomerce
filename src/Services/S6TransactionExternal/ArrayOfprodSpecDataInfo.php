<?php

class ArrayOfprodSpecDataInfo implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var prodSpecDataInfo[] $prodSpecDataInfo
     */
    protected $prodSpecDataInfo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return prodSpecDataInfo[]
     */
    public function getProdSpecDataInfo()
    {
      return $this->prodSpecDataInfo;
    }

    /**
     * @param prodSpecDataInfo[] $prodSpecDataInfo
     * @return ArrayOfprodSpecDataInfo
     */
    public function setProdSpecDataInfo(array $prodSpecDataInfo = null)
    {
      $this->prodSpecDataInfo = $prodSpecDataInfo;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->prodSpecDataInfo[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return prodSpecDataInfo
     */
    public function offsetGet($offset)
    {
      return $this->prodSpecDataInfo[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param prodSpecDataInfo $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->prodSpecDataInfo[] = $value;
      } else {
        $this->prodSpecDataInfo[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->prodSpecDataInfo[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return prodSpecDataInfo Return the current element
     */
    public function current()
    {
      return current($this->prodSpecDataInfo);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->prodSpecDataInfo);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->prodSpecDataInfo);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->prodSpecDataInfo);
    }

    /**
     * Countable implementation
     *
     * @return prodSpecDataInfo Return count of elements
     */
    public function count()
    {
      return count($this->prodSpecDataInfo);
    }

}
