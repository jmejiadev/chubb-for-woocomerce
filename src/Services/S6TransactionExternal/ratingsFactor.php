<?php

class ratingsFactor
{

    /**
     * @var int[] $ratingFactor
     */
    protected $ratingFactor = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int[]
     */
    public function getRatingFactor()
    {
      return $this->ratingFactor;
    }

    /**
     * @param int[] $ratingFactor
     * @return ratingsFactor
     */
    public function setRatingFactor(array $ratingFactor = null)
    {
      $this->ratingFactor = $ratingFactor;
      return $this;
    }

}
