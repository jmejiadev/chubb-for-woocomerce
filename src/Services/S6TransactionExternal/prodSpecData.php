<?php

class prodSpecData
{

    /**
     * @var int $indexer
     */
    protected $indexer = null;

    /**
     * @var ArrayOfprodSpecDataInfo $prodsSpecDataInfo
     */
    protected $prodsSpecDataInfo = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getIndexer()
    {
      return $this->indexer;
    }

    /**
     * @param int $indexer
     * @return prodSpecData
     */
    public function setIndexer($indexer)
    {
      $this->indexer = $indexer;
      return $this;
    }

    /**
     * @return ArrayOfprodSpecDataInfo
     */
    public function getProdsSpecDataInfo()
    {
      return $this->prodsSpecDataInfo;
    }

    /**
     * @param ArrayOfprodSpecDataInfo $prodsSpecDataInfo
     * @return prodSpecData
     */
    public function setProdsSpecDataInfo($prodsSpecDataInfo)
    {
      $this->prodsSpecDataInfo = $prodsSpecDataInfo;
      return $this;
    }

}
