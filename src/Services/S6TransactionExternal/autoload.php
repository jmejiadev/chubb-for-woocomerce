<?php


 function autoload_52cb3e7885b155c54ba4748979ad6974($class)
{
    $classes = array(
        'S6TransactionExternal' => __DIR__ .'/S6TransactionExternal.php',
        'ProcessTransaction' => __DIR__ .'/ProcessTransaction.php',
        'ProcessTransactionResponse' => __DIR__ .'/ProcessTransactionResponse.php',
        'GetTransactionStatus' => __DIR__ .'/GetTransactionStatus.php',
        'GetTransactionStatusResponse' => __DIR__ .'/GetTransactionStatusResponse.php',
        'ProcessTransactionRequest' => __DIR__ .'/ProcessTransactionRequest.php',
        'TransactionResponse' => __DIR__ .'/TransactionResponse.php',
        'ResultStatusEnum' => __DIR__ .'/ResultStatusEnum.php',
        's6Transaction' => __DIR__ .'/s6Transaction.php',
        'S6TransactionBase' => __DIR__ .'/S6TransactionBase.php',
        'prodSpecDataInfo' => __DIR__ .'/prodSpecDataInfo.php',
        'beneficiaryDetail' => __DIR__ .'/beneficiaryDetail.php',
        'product' => __DIR__ .'/product.php',
        'customer' => __DIR__ .'/customer.php',
        'custType' => __DIR__ .'/custType.php',
        'ArrayOfcustProd' => __DIR__ .'/ArrayOfcustProd.php',
        'custProd' => __DIR__ .'/custProd.php',
        'beneficiary' => __DIR__ .'/beneficiary.php',
        'ArrayOfbeneficiaryDetail' => __DIR__ .'/ArrayOfbeneficiaryDetail.php',
        'reference' => __DIR__ .'/reference.php',
        'address' => __DIR__ .'/address.php',
        'sensitivity' => __DIR__ .'/sensitivity.php',
        'suspReason' => __DIR__ .'/suspReason.php',
        'paymentInfo' => __DIR__ .'/paymentInfo.php',
        'ArrayOfprodSpecData' => __DIR__ .'/ArrayOfprodSpecData.php',
        'prodSpecData' => __DIR__ .'/prodSpecData.php',
        'ArrayOfprodSpecDataInfo' => __DIR__ .'/ArrayOfprodSpecDataInfo.php',
        'ratingsFactor' => __DIR__ .'/ratingsFactor.php',
        'benefitsRatingFactor' => __DIR__ .'/benefitsRatingFactor.php',
        'exclsCd' => __DIR__ .'/exclsCd.php',
        'custAdds' => __DIR__ .'/custAdds.php',
        'ArrayOfsensitivity' => __DIR__ .'/ArrayOfsensitivity.php',
        'ArrayOfproduct' => __DIR__ .'/ArrayOfproduct.php',
        'correspondenceType' => __DIR__ .'/correspondenceType.php',
        'ArrayOfcustomer' => __DIR__ .'/ArrayOfcustomer.php',
        'ArrayOfaddress' => __DIR__ .'/ArrayOfaddress.php',
        'ArrayOfreference' => __DIR__ .'/ArrayOfreference.php',
        'ArrayOfsuspReason' => __DIR__ .'/ArrayOfsuspReason.php',
        's6TransactionResult' => __DIR__ .'/s6TransactionResult.php',
        's6TransactionStatus' => __DIR__ .'/s6TransactionStatus.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_52cb3e7885b155c54ba4748979ad6974');

// Do nothing. The rest is just leftovers from the code generation.
{
}
