<?php

class ArrayOfreference implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var reference[] $reference
     */
    protected $reference = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return reference[]
     */
    public function getReference()
    {
      return $this->reference;
    }

    /**
     * @param reference[] $reference
     * @return ArrayOfreference
     */
    public function setReference(array $reference = null)
    {
      $this->reference = $reference;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->reference[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return reference
     */
    public function offsetGet($offset)
    {
      return $this->reference[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param reference $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->reference[] = $value;
      } else {
        $this->reference[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->reference[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return reference Return the current element
     */
    public function current()
    {
      return current($this->reference);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->reference);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->reference);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->reference);
    }

    /**
     * Countable implementation
     *
     * @return reference Return count of elements
     */
    public function count()
    {
      return count($this->reference);
    }

}
