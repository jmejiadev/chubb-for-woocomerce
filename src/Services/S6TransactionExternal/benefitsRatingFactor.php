<?php

class benefitsRatingFactor
{

    /**
     * @var int[] $benefitRatingFactor
     */
    protected $benefitRatingFactor = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int[]
     */
    public function getBenefitRatingFactor()
    {
      return $this->benefitRatingFactor;
    }

    /**
     * @param int[] $benefitRatingFactor
     * @return benefitsRatingFactor
     */
    public function setBenefitRatingFactor(array $benefitRatingFactor = null)
    {
      $this->benefitRatingFactor = $benefitRatingFactor;
      return $this;
    }

}
