<?php

class customer extends S6TransactionBase
{

    /**
     * @var string $custId
     */
    protected $custId = null;

    /**
     * @var custType $custType
     */
    protected $custType = null;

    /**
     * @var string $s6AcctId
     */
    protected $s6AcctId = null;

    /**
     * @var boolean $polHolder
     */
    protected $polHolder = null;

    /**
     * @var boolean $polPayer
     */
    protected $polPayer = null;

    /**
     * @var string $lastName
     */
    protected $lastName = null;

    /**
     * @var string $firstName
     */
    protected $firstName = null;

    /**
     * @var string $middleName
     */
    protected $middleName = null;

    /**
     * @var string $nickName
     */
    protected $nickName = null;

    /**
     * @var string $formalName
     */
    protected $formalName = null;

    /**
     * @var int $title
     */
    protected $title = null;

    /**
     * @var int $langCd
     */
    protected $langCd = null;

    /**
     * @var string $personalId
     */
    protected $personalId = null;

    /**
     * @var \DateTime $birthDate
     */
    protected $birthDate = null;

    /**
     * @var int $sexCd
     */
    protected $sexCd = null;

    /**
     * @var string $emailAddr
     */
    protected $emailAddr = null;

    /**
     * @var string $emailFulfillment
     */
    protected $emailFulfillment = null;

    /**
     * @var int $callPrefCd
     */
    protected $callPrefCd = null;

    /**
     * @var int $nationCd
     */
    protected $nationCd = null;

    /**
     * @var int $raceCd
     */
    protected $raceCd = null;

    /**
     * @var int $religionCd
     */
    protected $religionCd = null;

    /**
     * @var int $eduCd
     */
    protected $eduCd = null;

    /**
     * @var int $ocupCd
     */
    protected $ocupCd = null;

    /**
     * @var int $indCd
     */
    protected $indCd = null;

    /**
     * @var int $maritalCd
     */
    protected $maritalCd = null;

    /**
     * @var int $relationshipCd
     */
    protected $relationshipCd = null;

    /**
     * @var ArrayOfcustProd $custProds
     */
    protected $custProds = null;

    /**
     * @var custAdds $custAdds
     */
    protected $custAdds = null;

    /**
     * @var ArrayOfsensitivity $sensitivities
     */
    protected $sensitivities = null;

    /**
     * @var int $dependentType
     */
    protected $dependentType = null;

    /**
     * @var string $AlternateId
     */
    protected $AlternateId = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCustId()
    {
      return $this->custId;
    }

    /**
     * @param string $custId
     * @return customer
     */
    public function setCustId($custId)
    {
      $this->custId = $custId;
      return $this;
    }

    /**
     * @return custType
     */
    public function getCustType()
    {
      return $this->custType;
    }

    /**
     * @param custType $custType
     * @return customer
     */
    public function setCustType($custType)
    {
      $this->custType = $custType;
      return $this;
    }

    /**
     * @return string
     */
    public function getS6AcctId()
    {
      return $this->s6AcctId;
    }

    /**
     * @param string $s6AcctId
     * @return customer
     */
    public function setS6AcctId($s6AcctId)
    {
      $this->s6AcctId = $s6AcctId;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPolHolder()
    {
      return $this->polHolder;
    }

    /**
     * @param boolean $polHolder
     * @return customer
     */
    public function setPolHolder($polHolder)
    {
      $this->polHolder = $polHolder;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPolPayer()
    {
      return $this->polPayer;
    }

    /**
     * @param boolean $polPayer
     * @return customer
     */
    public function setPolPayer($polPayer)
    {
      $this->polPayer = $polPayer;
      return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
      return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return customer
     */
    public function setLastName($lastName)
    {
      $this->lastName = $lastName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
      return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return customer
     */
    public function setFirstName($firstName)
    {
      $this->firstName = $firstName;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
      return $this->middleName;
    }

    /**
     * @param string $middleName
     * @return customer
     */
    public function setMiddleName($middleName)
    {
      $this->middleName = $middleName;
      return $this;
    }

    /**
     * @return string
     */
    public function getNickName()
    {
      return $this->nickName;
    }

    /**
     * @param string $nickName
     * @return customer
     */
    public function setNickName($nickName)
    {
      $this->nickName = $nickName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFormalName()
    {
      return $this->formalName;
    }

    /**
     * @param string $formalName
     * @return customer
     */
    public function setFormalName($formalName)
    {
      $this->formalName = $formalName;
      return $this;
    }

    /**
     * @return int
     */
    public function getTitle()
    {
      return $this->title;
    }

    /**
     * @param int $title
     * @return customer
     */
    public function setTitle($title)
    {
      $this->title = $title;
      return $this;
    }

    /**
     * @return int
     */
    public function getLangCd()
    {
      return $this->langCd;
    }

    /**
     * @param int $langCd
     * @return customer
     */
    public function setLangCd($langCd)
    {
      $this->langCd = $langCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getPersonalId()
    {
      return $this->personalId;
    }

    /**
     * @param string $personalId
     * @return customer
     */
    public function setPersonalId($personalId)
    {
      $this->personalId = $personalId;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
      if ($this->birthDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->birthDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $birthDate
     * @return customer
     */
    public function setBirthDate(\DateTime $birthDate = null)
    {
      if ($birthDate == null) {
       $this->birthDate = null;
      } else {
        $this->birthDate = $birthDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getSexCd()
    {
      return $this->sexCd;
    }

    /**
     * @param int $sexCd
     * @return customer
     */
    public function setSexCd($sexCd)
    {
      $this->sexCd = $sexCd;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmailAddr()
    {
      return $this->emailAddr;
    }

    /**
     * @param string $emailAddr
     * @return customer
     */
    public function setEmailAddr($emailAddr)
    {
      $this->emailAddr = $emailAddr;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmailFulfillment()
    {
      return $this->emailFulfillment;
    }

    /**
     * @param string $emailFulfillment
     * @return customer
     */
    public function setEmailFulfillment($emailFulfillment)
    {
      $this->emailFulfillment = $emailFulfillment;
      return $this;
    }

    /**
     * @return int
     */
    public function getCallPrefCd()
    {
      return $this->callPrefCd;
    }

    /**
     * @param int $callPrefCd
     * @return customer
     */
    public function setCallPrefCd($callPrefCd)
    {
      $this->callPrefCd = $callPrefCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getNationCd()
    {
      return $this->nationCd;
    }

    /**
     * @param int $nationCd
     * @return customer
     */
    public function setNationCd($nationCd)
    {
      $this->nationCd = $nationCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getRaceCd()
    {
      return $this->raceCd;
    }

    /**
     * @param int $raceCd
     * @return customer
     */
    public function setRaceCd($raceCd)
    {
      $this->raceCd = $raceCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getReligionCd()
    {
      return $this->religionCd;
    }

    /**
     * @param int $religionCd
     * @return customer
     */
    public function setReligionCd($religionCd)
    {
      $this->religionCd = $religionCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getEduCd()
    {
      return $this->eduCd;
    }

    /**
     * @param int $eduCd
     * @return customer
     */
    public function setEduCd($eduCd)
    {
      $this->eduCd = $eduCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getOcupCd()
    {
      return $this->ocupCd;
    }

    /**
     * @param int $ocupCd
     * @return customer
     */
    public function setOcupCd($ocupCd)
    {
      $this->ocupCd = $ocupCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getIndCd()
    {
      return $this->indCd;
    }

    /**
     * @param int $indCd
     * @return customer
     */
    public function setIndCd($indCd)
    {
      $this->indCd = $indCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaritalCd()
    {
      return $this->maritalCd;
    }

    /**
     * @param int $maritalCd
     * @return customer
     */
    public function setMaritalCd($maritalCd)
    {
      $this->maritalCd = $maritalCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getRelationshipCd()
    {
      return $this->relationshipCd;
    }

    /**
     * @param int $relationshipCd
     * @return customer
     */
    public function setRelationshipCd($relationshipCd)
    {
      $this->relationshipCd = $relationshipCd;
      return $this;
    }

    /**
     * @return ArrayOfcustProd
     */
    public function getCustProds()
    {
      return $this->custProds;
    }

    /**
     * @param ArrayOfcustProd $custProds
     * @return customer
     */
    public function setCustProds($custProds)
    {
      $this->custProds = $custProds;
      return $this;
    }

    /**
     * @return custAdds
     */
    public function getCustAdds()
    {
      return $this->custAdds;
    }

    /**
     * @param custAdds $custAdds
     * @return customer
     */
    public function setCustAdds($custAdds)
    {
      $this->custAdds = $custAdds;
      return $this;
    }

    /**
     * @return ArrayOfsensitivity
     */
    public function getSensitivities()
    {
      return $this->sensitivities;
    }

    /**
     * @param ArrayOfsensitivity $sensitivities
     * @return customer
     */
    public function setSensitivities($sensitivities)
    {
      $this->sensitivities = $sensitivities;
      return $this;
    }

    /**
     * @return int
     */
    public function getDependentType()
    {
      return $this->dependentType;
    }

    /**
     * @param int $dependentType
     * @return customer
     */
    public function setDependentType($dependentType)
    {
      $this->dependentType = $dependentType;
      return $this;
    }

    /**
     * @return string
     */
    public function getAlternateId()
    {
      return $this->AlternateId;
    }

    /**
     * @param string $AlternateId
     * @return customer
     */
    public function setAlternateId($AlternateId)
    {
      $this->AlternateId = $AlternateId;
      return $this;
    }

}
