<?php

class beneficiaryDetail extends S6TransactionBase
{

    /**
     * @var string $name
     */
    protected $name = null;

    /**
     * @var string $relationship
     */
    protected $relationship = null;

    /**
     * @var float $percentage
     */
    protected $percentage = null;

    /**
     * @var string $personalID
     */
    protected $personalID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getName()
    {
      return $this->name;
    }

    /**
     * @param string $name
     * @return beneficiaryDetail
     */
    public function setName($name)
    {
      $this->name = $name;
      return $this;
    }

    /**
     * @return string
     */
    public function getRelationship()
    {
      return $this->relationship;
    }

    /**
     * @param string $relationship
     * @return beneficiaryDetail
     */
    public function setRelationship($relationship)
    {
      $this->relationship = $relationship;
      return $this;
    }

    /**
     * @return float
     */
    public function getPercentage()
    {
      return $this->percentage;
    }

    /**
     * @param float $percentage
     * @return beneficiaryDetail
     */
    public function setPercentage($percentage)
    {
      $this->percentage = $percentage;
      return $this;
    }

    /**
     * @return string
     */
    public function getPersonalID()
    {
      return $this->personalID;
    }

    /**
     * @param string $personalID
     * @return beneficiaryDetail
     */
    public function setPersonalID($personalID)
    {
      $this->personalID = $personalID;
      return $this;
    }

}
