<?php

class custProd extends S6TransactionBase
{

    /**
     * @var string $prodCd
     */
    protected $prodCd = null;

    /**
     * @var int $benLv
     */
    protected $benLv = null;

    /**
     * @var float $units
     */
    protected $units = null;

    /**
     * @var beneficiary $beneficiary
     */
    protected $beneficiary = null;

    /**
     * @var ArrayOfprodSpecData $prodsSpecData
     */
    protected $prodsSpecData = null;

    /**
     * @var ratingsFactor $ratingsFactor
     */
    protected $ratingsFactor = null;

    /**
     * @var benefitsRatingFactor $benefitsRatingFactor
     */
    protected $benefitsRatingFactor = null;

    /**
     * @var exclsCd $exclsCd
     */
    protected $exclsCd = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getProdCd()
    {
      return $this->prodCd;
    }

    /**
     * @param string $prodCd
     * @return custProd
     */
    public function setProdCd($prodCd)
    {
      $this->prodCd = $prodCd;
      return $this;
    }

    /**
     * @return int
     */
    public function getBenLv()
    {
      return $this->benLv;
    }

    /**
     * @param int $benLv
     * @return custProd
     */
    public function setBenLv($benLv)
    {
      $this->benLv = $benLv;
      return $this;
    }

    /**
     * @return float
     */
    public function getUnits()
    {
      return $this->units;
    }

    /**
     * @param float $units
     * @return custProd
     */
    public function setUnits($units)
    {
      $this->units = $units;
      return $this;
    }

    /**
     * @return beneficiary
     */
    public function getBeneficiary()
    {
      return $this->beneficiary;
    }

    /**
     * @param beneficiary $beneficiary
     * @return custProd
     */
    public function setBeneficiary($beneficiary)
    {
      $this->beneficiary = $beneficiary;
      return $this;
    }

    /**
     * @return ArrayOfprodSpecData
     */
    public function getProdsSpecData()
    {
      return $this->prodsSpecData;
    }

    /**
     * @param ArrayOfprodSpecData $prodsSpecData
     * @return custProd
     */
    public function setProdsSpecData($prodsSpecData)
    {
      $this->prodsSpecData = $prodsSpecData;
      return $this;
    }

    /**
     * @return ratingsFactor
     */
    public function getRatingsFactor()
    {
      return $this->ratingsFactor;
    }

    /**
     * @param ratingsFactor $ratingsFactor
     * @return custProd
     */
    public function setRatingsFactor($ratingsFactor)
    {
      $this->ratingsFactor = $ratingsFactor;
      return $this;
    }

    /**
     * @return benefitsRatingFactor
     */
    public function getBenefitsRatingFactor()
    {
      return $this->benefitsRatingFactor;
    }

    /**
     * @param benefitsRatingFactor $benefitsRatingFactor
     * @return custProd
     */
    public function setBenefitsRatingFactor($benefitsRatingFactor)
    {
      $this->benefitsRatingFactor = $benefitsRatingFactor;
      return $this;
    }

    /**
     * @return exclsCd
     */
    public function getExclsCd()
    {
      return $this->exclsCd;
    }

    /**
     * @param exclsCd $exclsCd
     * @return custProd
     */
    public function setExclsCd($exclsCd)
    {
      $this->exclsCd = $exclsCd;
      return $this;
    }

}
