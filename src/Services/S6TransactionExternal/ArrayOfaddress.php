<?php

class ArrayOfaddress implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var address[] $address
     */
    protected $address = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return address[]
     */
    public function getAddress()
    {
      return $this->address;
    }

    /**
     * @param address[] $address
     * @return ArrayOfaddress
     */
    public function setAddress(array $address = null)
    {
      $this->address = $address;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->address[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return address
     */
    public function offsetGet($offset)
    {
      return $this->address[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param address $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->address[] = $value;
      } else {
        $this->address[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->address[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return address Return the current element
     */
    public function current()
    {
      return current($this->address);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->address);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->address);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->address);
    }

    /**
     * Countable implementation
     *
     * @return address Return count of elements
     */
    public function count()
    {
      return count($this->address);
    }

}
