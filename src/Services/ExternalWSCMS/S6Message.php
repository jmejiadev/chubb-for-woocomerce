<?php

class S6Message
{

    /**
     * @var string $CategoryCode
     */
    protected $CategoryCode = null;

    /**
     * @var ArrayOfMessage $Messages
     */
    protected $Messages = null;

    /**
     * @var int $languageCode
     */
    protected $languageCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCategoryCode()
    {
      return $this->CategoryCode;
    }

    /**
     * @param string $CategoryCode
     * @return S6Message
     */
    public function setCategoryCode($CategoryCode)
    {
      $this->CategoryCode = $CategoryCode;
      return $this;
    }

    /**
     * @return ArrayOfMessage
     */
    public function getMessages()
    {
      return $this->Messages;
    }

    /**
     * @param ArrayOfMessage $Messages
     * @return S6Message
     */
    public function setMessages($Messages)
    {
      $this->Messages = $Messages;
      return $this;
    }

    /**
     * @return int
     */
    public function getLanguageCode()
    {
      return $this->languageCode;
    }

    /**
     * @param int $languageCode
     * @return S6Message
     */
    public function setLanguageCode($languageCode)
    {
      $this->languageCode = $languageCode;
      return $this;
    }

}
