<?php

class ArrayOfbank implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var bank[] $bank
     */
    protected $bank = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return bank[]
     */
    public function getBank()
    {
      return $this->bank;
    }

    /**
     * @param bank[] $bank
     * @return ArrayOfbank
     */
    public function setBank(array $bank = null)
    {
      $this->bank = $bank;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->bank[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return bank
     */
    public function offsetGet($offset)
    {
      return $this->bank[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param bank $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->bank[] = $value;
      } else {
        $this->bank[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->bank[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return bank Return the current element
     */
    public function current()
    {
      return current($this->bank);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->bank);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->bank);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->bank);
    }

    /**
     * Countable implementation
     *
     * @return bank Return count of elements
     */
    public function count()
    {
      return count($this->bank);
    }

}
