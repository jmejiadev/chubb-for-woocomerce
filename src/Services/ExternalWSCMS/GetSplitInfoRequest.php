<?php

class GetSplitInfoRequest
{

    /**
     * @var int $LanguageCode
     */
    protected $LanguageCode = null;

    /**
     * @var boolean $LoadBankInfo
     */
    protected $LoadBankInfo = null;

    /**
     * @var string $SplitKey
     */
    protected $SplitKey = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getLanguageCode()
    {
      return $this->LanguageCode;
    }

    /**
     * @param int $LanguageCode
     * @return GetSplitInfoRequest
     */
    public function setLanguageCode($LanguageCode)
    {
      $this->LanguageCode = $LanguageCode;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getLoadBankInfo()
    {
      return $this->LoadBankInfo;
    }

    /**
     * @param boolean $LoadBankInfo
     * @return GetSplitInfoRequest
     */
    public function setLoadBankInfo($LoadBankInfo)
    {
      $this->LoadBankInfo = $LoadBankInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getSplitKey()
    {
      return $this->SplitKey;
    }

    /**
     * @param string $SplitKey
     * @return GetSplitInfoRequest
     */
    public function setSplitKey($SplitKey)
    {
      $this->SplitKey = $SplitKey;
      return $this;
    }

}
