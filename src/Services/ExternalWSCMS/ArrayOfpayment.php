<?php

class ArrayOfpayment implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var payment[] $payment
     */
    protected $payment = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return payment[]
     */
    public function getPayment()
    {
      return $this->payment;
    }

    /**
     * @param payment[] $payment
     * @return ArrayOfpayment
     */
    public function setPayment(array $payment = null)
    {
      $this->payment = $payment;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->payment[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return payment
     */
    public function offsetGet($offset)
    {
      return $this->payment[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param payment $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->payment[] = $value;
      } else {
        $this->payment[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->payment[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return payment Return the current element
     */
    public function current()
    {
      return current($this->payment);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->payment);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->payment);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->payment);
    }

    /**
     * Countable implementation
     *
     * @return payment Return count of elements
     */
    public function count()
    {
      return count($this->payment);
    }

}
