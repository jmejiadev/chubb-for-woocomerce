<?php

class GetMessagesByCategoryRequest
{

    /**
     * @var string $CategoryCode
     */
    protected $CategoryCode = null;

    /**
     * @var string $CountryCode
     */
    protected $CountryCode = null;

    /**
     * @var int $LanguageCode
     */
    protected $LanguageCode = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCategoryCode()
    {
      return $this->CategoryCode;
    }

    /**
     * @param string $CategoryCode
     * @return GetMessagesByCategoryRequest
     */
    public function setCategoryCode($CategoryCode)
    {
      $this->CategoryCode = $CategoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
      return $this->CountryCode;
    }

    /**
     * @param string $CountryCode
     * @return GetMessagesByCategoryRequest
     */
    public function setCountryCode($CountryCode)
    {
      $this->CountryCode = $CountryCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getLanguageCode()
    {
      return $this->LanguageCode;
    }

    /**
     * @param int $LanguageCode
     * @return GetMessagesByCategoryRequest
     */
    public function setLanguageCode($LanguageCode)
    {
      $this->LanguageCode = $LanguageCode;
      return $this;
    }

}
