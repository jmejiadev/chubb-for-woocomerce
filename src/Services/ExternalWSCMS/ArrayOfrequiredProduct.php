<?php

class ArrayOfrequiredProduct implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var requiredProduct[] $requiredProduct
     */
    protected $requiredProduct = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return requiredProduct[]
     */
    public function getRequiredProduct()
    {
      return $this->requiredProduct;
    }

    /**
     * @param requiredProduct[] $requiredProduct
     * @return ArrayOfrequiredProduct
     */
    public function setRequiredProduct(array $requiredProduct = null)
    {
      $this->requiredProduct = $requiredProduct;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->requiredProduct[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return requiredProduct
     */
    public function offsetGet($offset)
    {
      return $this->requiredProduct[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param requiredProduct $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->requiredProduct[] = $value;
      } else {
        $this->requiredProduct[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->requiredProduct[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return requiredProduct Return the current element
     */
    public function current()
    {
      return current($this->requiredProduct);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->requiredProduct);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->requiredProduct);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->requiredProduct);
    }

    /**
     * Countable implementation
     *
     * @return requiredProduct Return count of elements
     */
    public function count()
    {
      return count($this->requiredProduct);
    }

}
