<?php

class bank
{

    /**
     * @var string $bankCode
     */
    protected $bankCode = null;

    /**
     * @var string $bankName
     */
    protected $bankName = null;

    /**
     * @var ArrayOfbranch $branches
     */
    protected $branches = null;

    /**
     * @var \DateTime $inactiveDate
     */
    protected $inactiveDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBankCode()
    {
      return $this->bankCode;
    }

    /**
     * @param string $bankCode
     * @return bank
     */
    public function setBankCode($bankCode)
    {
      $this->bankCode = $bankCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
      return $this->bankName;
    }

    /**
     * @param string $bankName
     * @return bank
     */
    public function setBankName($bankName)
    {
      $this->bankName = $bankName;
      return $this;
    }

    /**
     * @return ArrayOfbranch
     */
    public function getBranches()
    {
      return $this->branches;
    }

    /**
     * @param ArrayOfbranch $branches
     * @return bank
     */
    public function setBranches($branches)
    {
      $this->branches = $branches;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInactiveDate()
    {
      if ($this->inactiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->inactiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $inactiveDate
     * @return bank
     */
    public function setInactiveDate(\DateTime $inactiveDate = null)
    {
      if ($inactiveDate == null) {
       $this->inactiveDate = null;
      } else {
        $this->inactiveDate = $inactiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

}
