<?php

class ArrayOfbenefitLevel implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var benefitLevel[] $benefitLevel
     */
    protected $benefitLevel = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return benefitLevel[]
     */
    public function getBenefitLevel()
    {
      return $this->benefitLevel;
    }

    /**
     * @param benefitLevel[] $benefitLevel
     * @return ArrayOfbenefitLevel
     */
    public function setBenefitLevel(array $benefitLevel = null)
    {
      $this->benefitLevel = $benefitLevel;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->benefitLevel[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return benefitLevel
     */
    public function offsetGet($offset)
    {
      return $this->benefitLevel[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param benefitLevel $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->benefitLevel[] = $value;
      } else {
        $this->benefitLevel[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->benefitLevel[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return benefitLevel Return the current element
     */
    public function current()
    {
      return current($this->benefitLevel);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->benefitLevel);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->benefitLevel);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->benefitLevel);
    }

    /**
     * Countable implementation
     *
     * @return benefitLevel Return count of elements
     */
    public function count()
    {
      return count($this->benefitLevel);
    }

}
