<?php

class branch
{

    /**
     * @var string $branchCode
     */
    protected $branchCode = null;

    /**
     * @var string $branchName
     */
    protected $branchName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBranchCode()
    {
      return $this->branchCode;
    }

    /**
     * @param string $branchCode
     * @return branch
     */
    public function setBranchCode($branchCode)
    {
      $this->branchCode = $branchCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBranchName()
    {
      return $this->branchName;
    }

    /**
     * @param string $branchName
     * @return branch
     */
    public function setBranchName($branchName)
    {
      $this->branchName = $branchName;
      return $this;
    }

}
