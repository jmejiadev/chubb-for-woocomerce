<?php

class premium
{

    /**
     * @var string $individual
     */
    protected $individual = null;

    /**
     * @var float $premiumValue
     */
    protected $premiumValue = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getIndividual()
    {
      return $this->individual;
    }

    /**
     * @param string $individual
     * @return premium
     */
    public function setIndividual($individual)
    {
      $this->individual = $individual;
      return $this;
    }

    /**
     * @return float
     */
    public function getPremiumValue()
    {
      return $this->premiumValue;
    }

    /**
     * @param float $premiumValue
     * @return premium
     */
    public function setPremiumValue($premiumValue)
    {
      $this->premiumValue = $premiumValue;
      return $this;
    }

}
