<?php


 function autoload_32c990dba1fff69e463aecfff34c4107($class)
{
    $classes = array(
        'ExternalWSCMS' => __DIR__ .'/ExternalWSCMS.php',
        'GetSplitInfo' => __DIR__ .'/GetSplitInfo.php',
        'GetSplitInfoResponse' => __DIR__ .'/GetSplitInfoResponse.php',
        'GetMessagesByCategory' => __DIR__ .'/GetMessagesByCategory.php',
        'GetMessagesByCategoryResponse' => __DIR__ .'/GetMessagesByCategoryResponse.php',
        'GetSplitInfoRequest' => __DIR__ .'/GetSplitInfoRequest.php',
        'ResponseSplitInfo' => __DIR__ .'/ResponseSplitInfo.php',
        'Response' => __DIR__ .'/Response.php',
        'GetMessagesByCategoryRequest' => __DIR__ .'/GetMessagesByCategoryRequest.php',
        'ResponseMessagesByCategory' => __DIR__ .'/ResponseMessagesByCategory.php',
        'splitInfo' => __DIR__ .'/splitInfo.php',
        'ArrayOfpaymentFrequency' => __DIR__ .'/ArrayOfpaymentFrequency.php',
        'paymentFrequency' => __DIR__ .'/paymentFrequency.php',
        'ArrayOfpayment' => __DIR__ .'/ArrayOfpayment.php',
        'payment' => __DIR__ .'/payment.php',
        'ArrayOfcollector' => __DIR__ .'/ArrayOfcollector.php',
        'collector' => __DIR__ .'/collector.php',
        'ArrayOfcreditCard' => __DIR__ .'/ArrayOfcreditCard.php',
        'creditCard' => __DIR__ .'/creditCard.php',
        'ArrayOfbank' => __DIR__ .'/ArrayOfbank.php',
        'bank' => __DIR__ .'/bank.php',
        'ArrayOfbranch' => __DIR__ .'/ArrayOfbranch.php',
        'branch' => __DIR__ .'/branch.php',
        'ArrayOfproduct' => __DIR__ .'/ArrayOfproduct.php',
        'product' => __DIR__ .'/product.php',
        'ArrayOfbenefitLevel' => __DIR__ .'/ArrayOfbenefitLevel.php',
        'benefitLevel' => __DIR__ .'/benefitLevel.php',
        'ArrayOfpremium' => __DIR__ .'/ArrayOfpremium.php',
        'premium' => __DIR__ .'/premium.php',
        'ArrayOfproductSpecific' => __DIR__ .'/ArrayOfproductSpecific.php',
        'productSpecific' => __DIR__ .'/productSpecific.php',
        'ArrayOfproductSpecificDetail' => __DIR__ .'/ArrayOfproductSpecificDetail.php',
        'productSpecificDetail' => __DIR__ .'/productSpecificDetail.php',
        'ArrayOfratingFactor' => __DIR__ .'/ArrayOfratingFactor.php',
        'ratingFactor' => __DIR__ .'/ratingFactor.php',
        'ArrayOfratingFactorDetail' => __DIR__ .'/ArrayOfratingFactorDetail.php',
        'ratingFactorDetail' => __DIR__ .'/ratingFactorDetail.php',
        'ArrayOfrequiredProduct' => __DIR__ .'/ArrayOfrequiredProduct.php',
        'requiredProduct' => __DIR__ .'/requiredProduct.php',
        'ArrayOfProductScheduledBenefit' => __DIR__ .'/ArrayOfProductScheduledBenefit.php',
        'ProductScheduledBenefit' => __DIR__ .'/ProductScheduledBenefit.php',
        'ArrayOfProductCoverage' => __DIR__ .'/ArrayOfProductCoverage.php',
        'ProductCoverage' => __DIR__ .'/ProductCoverage.php',
        'ArrayOfProductPremium' => __DIR__ .'/ArrayOfProductPremium.php',
        'ProductPremium' => __DIR__ .'/ProductPremium.php',
        'ArrayOfstring' => __DIR__ .'/ArrayOfstring.php',
        'S6Message' => __DIR__ .'/S6Message.php',
        'ArrayOfMessage' => __DIR__ .'/ArrayOfMessage.php',
        'Message' => __DIR__ .'/Message.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_32c990dba1fff69e463aecfff34c4107');

// Do nothing. The rest is just leftovers from the code generation.
{
}
