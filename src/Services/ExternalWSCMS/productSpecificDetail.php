<?php

class productSpecificDetail
{

    /**
     * @var int $ProductSpecificDataCode
     */
    protected $ProductSpecificDataCode = null;

    /**
     * @var string $ProductSpecificDataDescription
     */
    protected $ProductSpecificDataDescription = null;

    /**
     * @var boolean $IsRequired
     */
    protected $IsRequired = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getProductSpecificDataCode()
    {
      return $this->ProductSpecificDataCode;
    }

    /**
     * @param int $ProductSpecificDataCode
     * @return productSpecificDetail
     */
    public function setProductSpecificDataCode($ProductSpecificDataCode)
    {
      $this->ProductSpecificDataCode = $ProductSpecificDataCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductSpecificDataDescription()
    {
      return $this->ProductSpecificDataDescription;
    }

    /**
     * @param string $ProductSpecificDataDescription
     * @return productSpecificDetail
     */
    public function setProductSpecificDataDescription($ProductSpecificDataDescription)
    {
      $this->ProductSpecificDataDescription = $ProductSpecificDataDescription;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsRequired()
    {
      return $this->IsRequired;
    }

    /**
     * @param boolean $IsRequired
     * @return productSpecificDetail
     */
    public function setIsRequired($IsRequired)
    {
      $this->IsRequired = $IsRequired;
      return $this;
    }

}
