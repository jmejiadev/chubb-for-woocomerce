<?php

class Message
{

    /**
     * @var int $Key
     */
    protected $Key = null;

    /**
     * @var string $Description
     */
    protected $Description = null;

    /**
     * @var string $MsgID
     */
    protected $MsgID = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getKey()
    {
      return $this->Key;
    }

    /**
     * @param int $Key
     * @return Message
     */
    public function setKey($Key)
    {
      $this->Key = $Key;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->Description;
    }

    /**
     * @param string $Description
     * @return Message
     */
    public function setDescription($Description)
    {
      $this->Description = $Description;
      return $this;
    }

    /**
     * @return string
     */
    public function getMsgID()
    {
      return $this->MsgID;
    }

    /**
     * @param string $MsgID
     * @return Message
     */
    public function setMsgID($MsgID)
    {
      $this->MsgID = $MsgID;
      return $this;
    }

}
