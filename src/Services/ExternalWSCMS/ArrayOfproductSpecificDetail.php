<?php

class ArrayOfproductSpecificDetail implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var productSpecificDetail[] $productSpecificDetail
     */
    protected $productSpecificDetail = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return productSpecificDetail[]
     */
    public function getProductSpecificDetail()
    {
      return $this->productSpecificDetail;
    }

    /**
     * @param productSpecificDetail[] $productSpecificDetail
     * @return ArrayOfproductSpecificDetail
     */
    public function setProductSpecificDetail(array $productSpecificDetail = null)
    {
      $this->productSpecificDetail = $productSpecificDetail;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->productSpecificDetail[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return productSpecificDetail
     */
    public function offsetGet($offset)
    {
      return $this->productSpecificDetail[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param productSpecificDetail $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->productSpecificDetail[] = $value;
      } else {
        $this->productSpecificDetail[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->productSpecificDetail[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return productSpecificDetail Return the current element
     */
    public function current()
    {
      return current($this->productSpecificDetail);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->productSpecificDetail);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->productSpecificDetail);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->productSpecificDetail);
    }

    /**
     * Countable implementation
     *
     * @return productSpecificDetail Return count of elements
     */
    public function count()
    {
      return count($this->productSpecificDetail);
    }

}
