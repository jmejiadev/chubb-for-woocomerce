<?php

class ArrayOfProductPremium implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductPremium[] $ProductPremium
     */
    protected $ProductPremium = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductPremium[]
     */
    public function getProductPremium()
    {
      return $this->ProductPremium;
    }

    /**
     * @param ProductPremium[] $ProductPremium
     * @return ArrayOfProductPremium
     */
    public function setProductPremium(array $ProductPremium = null)
    {
      $this->ProductPremium = $ProductPremium;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductPremium[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductPremium
     */
    public function offsetGet($offset)
    {
      return $this->ProductPremium[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductPremium $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ProductPremium[] = $value;
      } else {
        $this->ProductPremium[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductPremium[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductPremium Return the current element
     */
    public function current()
    {
      return current($this->ProductPremium);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductPremium);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductPremium);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductPremium);
    }

    /**
     * Countable implementation
     *
     * @return ProductPremium Return count of elements
     */
    public function count()
    {
      return count($this->ProductPremium);
    }

}
