<?php

class GetSplitInfo
{

    /**
     * @var string $accessKeyId
     */
    protected $accessKeyId = null;

    /**
     * @var GetSplitInfoRequest $request
     */
    protected $request = null;

    /**
     * @param string $accessKeyId
     * @param GetSplitInfoRequest $request
     */
    public function __construct($accessKeyId, $request)
    {
      $this->accessKeyId = $accessKeyId;
      $this->request = $request;
    }

    /**
     * @return string
     */
    public function getAccessKeyId()
    {
      return $this->accessKeyId;
    }

    /**
     * @param string $accessKeyId
     * @return GetSplitInfo
     */
    public function setAccessKeyId($accessKeyId)
    {
      $this->accessKeyId = $accessKeyId;
      return $this;
    }

    /**
     * @return GetSplitInfoRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetSplitInfoRequest $request
     * @return GetSplitInfo
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
