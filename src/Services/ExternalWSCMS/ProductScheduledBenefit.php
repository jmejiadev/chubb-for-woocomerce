<?php

class ProductScheduledBenefit
{

    /**
     * @var int $BenefitLevelID
     */
    protected $BenefitLevelID = null;

    /**
     * @var string $Individual
     */
    protected $Individual = null;

    /**
     * @var string $BenefitCode
     */
    protected $BenefitCode = null;

    /**
     * @var string $BenefitDescription
     */
    protected $BenefitDescription = null;

    /**
     * @var string $CategoryCode
     */
    protected $CategoryCode = null;

    /**
     * @var string $CategoryDescription
     */
    protected $CategoryDescription = null;

    /**
     * @var int $Rf_Combo_SeqNum
     */
    protected $Rf_Combo_SeqNum = null;

    /**
     * @var int $ReferenceCode
     */
    protected $ReferenceCode = null;

    /**
     * @var string $ReferenceDescription
     */
    protected $ReferenceDescription = null;

    /**
     * @var float $Amount
     */
    protected $Amount = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getBenefitLevelID()
    {
      return $this->BenefitLevelID;
    }

    /**
     * @param int $BenefitLevelID
     * @return ProductScheduledBenefit
     */
    public function setBenefitLevelID($BenefitLevelID)
    {
      $this->BenefitLevelID = $BenefitLevelID;
      return $this;
    }

    /**
     * @return string
     */
    public function getIndividual()
    {
      return $this->Individual;
    }

    /**
     * @param string $Individual
     * @return ProductScheduledBenefit
     */
    public function setIndividual($Individual)
    {
      $this->Individual = $Individual;
      return $this;
    }

    /**
     * @return string
     */
    public function getBenefitCode()
    {
      return $this->BenefitCode;
    }

    /**
     * @param string $BenefitCode
     * @return ProductScheduledBenefit
     */
    public function setBenefitCode($BenefitCode)
    {
      $this->BenefitCode = $BenefitCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBenefitDescription()
    {
      return $this->BenefitDescription;
    }

    /**
     * @param string $BenefitDescription
     * @return ProductScheduledBenefit
     */
    public function setBenefitDescription($BenefitDescription)
    {
      $this->BenefitDescription = $BenefitDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryCode()
    {
      return $this->CategoryCode;
    }

    /**
     * @param string $CategoryCode
     * @return ProductScheduledBenefit
     */
    public function setCategoryCode($CategoryCode)
    {
      $this->CategoryCode = $CategoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryDescription()
    {
      return $this->CategoryDescription;
    }

    /**
     * @param string $CategoryDescription
     * @return ProductScheduledBenefit
     */
    public function setCategoryDescription($CategoryDescription)
    {
      $this->CategoryDescription = $CategoryDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getRf_Combo_SeqNum()
    {
      return $this->Rf_Combo_SeqNum;
    }

    /**
     * @param int $Rf_Combo_SeqNum
     * @return ProductScheduledBenefit
     */
    public function setRf_Combo_SeqNum($Rf_Combo_SeqNum)
    {
      $this->Rf_Combo_SeqNum = $Rf_Combo_SeqNum;
      return $this;
    }

    /**
     * @return int
     */
    public function getReferenceCode()
    {
      return $this->ReferenceCode;
    }

    /**
     * @param int $ReferenceCode
     * @return ProductScheduledBenefit
     */
    public function setReferenceCode($ReferenceCode)
    {
      $this->ReferenceCode = $ReferenceCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getReferenceDescription()
    {
      return $this->ReferenceDescription;
    }

    /**
     * @param string $ReferenceDescription
     * @return ProductScheduledBenefit
     */
    public function setReferenceDescription($ReferenceDescription)
    {
      $this->ReferenceDescription = $ReferenceDescription;
      return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
      return $this->Amount;
    }

    /**
     * @param float $Amount
     * @return ProductScheduledBenefit
     */
    public function setAmount($Amount)
    {
      $this->Amount = $Amount;
      return $this;
    }

}
