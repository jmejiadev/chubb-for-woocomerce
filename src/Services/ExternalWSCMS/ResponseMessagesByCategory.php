<?php

class ResponseMessagesByCategory extends Response
{

    /**
     * @var S6Message $MessagesByCategory
     */
    protected $MessagesByCategory = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return S6Message
     */
    public function getMessagesByCategory()
    {
      return $this->MessagesByCategory;
    }

    /**
     * @param S6Message $MessagesByCategory
     * @return ResponseMessagesByCategory
     */
    public function setMessagesByCategory($MessagesByCategory)
    {
      $this->MessagesByCategory = $MessagesByCategory;
      return $this;
    }

}
