<?php

class ArrayOfpaymentFrequency implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var paymentFrequency[] $paymentFrequency
     */
    protected $paymentFrequency = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return paymentFrequency[]
     */
    public function getPaymentFrequency()
    {
      return $this->paymentFrequency;
    }

    /**
     * @param paymentFrequency[] $paymentFrequency
     * @return ArrayOfpaymentFrequency
     */
    public function setPaymentFrequency(array $paymentFrequency = null)
    {
      $this->paymentFrequency = $paymentFrequency;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->paymentFrequency[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return paymentFrequency
     */
    public function offsetGet($offset)
    {
      return $this->paymentFrequency[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param paymentFrequency $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->paymentFrequency[] = $value;
      } else {
        $this->paymentFrequency[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->paymentFrequency[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return paymentFrequency Return the current element
     */
    public function current()
    {
      return current($this->paymentFrequency);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->paymentFrequency);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->paymentFrequency);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->paymentFrequency);
    }

    /**
     * Countable implementation
     *
     * @return paymentFrequency Return count of elements
     */
    public function count()
    {
      return count($this->paymentFrequency);
    }

}
