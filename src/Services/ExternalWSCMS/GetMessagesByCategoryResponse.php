<?php

class GetMessagesByCategoryResponse
{

    /**
     * @var ResponseMessagesByCategory $GetMessagesByCategoryResult
     */
    protected $GetMessagesByCategoryResult = null;

    /**
     * @param ResponseMessagesByCategory $GetMessagesByCategoryResult
     */
    public function __construct($GetMessagesByCategoryResult)
    {
      $this->GetMessagesByCategoryResult = $GetMessagesByCategoryResult;
    }

    /**
     * @return ResponseMessagesByCategory
     */
    public function getGetMessagesByCategoryResult()
    {
      return $this->GetMessagesByCategoryResult;
    }

    /**
     * @param ResponseMessagesByCategory $GetMessagesByCategoryResult
     * @return GetMessagesByCategoryResponse
     */
    public function setGetMessagesByCategoryResult($GetMessagesByCategoryResult)
    {
      $this->GetMessagesByCategoryResult = $GetMessagesByCategoryResult;
      return $this;
    }

}
