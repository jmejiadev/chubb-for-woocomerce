<?php

class ArrayOfratingFactor implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ratingFactor[] $ratingFactor
     */
    protected $ratingFactor = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ratingFactor[]
     */
    public function getRatingFactor()
    {
      return $this->ratingFactor;
    }

    /**
     * @param ratingFactor[] $ratingFactor
     * @return ArrayOfratingFactor
     */
    public function setRatingFactor(array $ratingFactor = null)
    {
      $this->ratingFactor = $ratingFactor;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ratingFactor[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ratingFactor
     */
    public function offsetGet($offset)
    {
      return $this->ratingFactor[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ratingFactor $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ratingFactor[] = $value;
      } else {
        $this->ratingFactor[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ratingFactor[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ratingFactor Return the current element
     */
    public function current()
    {
      return current($this->ratingFactor);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ratingFactor);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ratingFactor);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ratingFactor);
    }

    /**
     * Countable implementation
     *
     * @return ratingFactor Return count of elements
     */
    public function count()
    {
      return count($this->ratingFactor);
    }

}
