<?php

class ArrayOfproduct implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var product[] $product
     */
    protected $product = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return product[]
     */
    public function getProduct()
    {
      return $this->product;
    }

    /**
     * @param product[] $product
     * @return ArrayOfproduct
     */
    public function setProduct(array $product = null)
    {
      $this->product = $product;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->product[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return product
     */
    public function offsetGet($offset)
    {
      return $this->product[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param product $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->product[] = $value;
      } else {
        $this->product[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->product[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return product Return the current element
     */
    public function current()
    {
      return current($this->product);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->product);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->product);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->product);
    }

    /**
     * Countable implementation
     *
     * @return product Return count of elements
     */
    public function count()
    {
      return count($this->product);
    }

}
