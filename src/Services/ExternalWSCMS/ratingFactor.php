<?php

class ratingFactor
{

    /**
     * @var string $category
     */
    protected $category = null;

    /**
     * @var string $categoryDescription
     */
    protected $categoryDescription = null;

    /**
     * @var ArrayOfratingFactorDetail $ratingFactorDetails
     */
    protected $ratingFactorDetails = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCategory()
    {
      return $this->category;
    }

    /**
     * @param string $category
     * @return ratingFactor
     */
    public function setCategory($category)
    {
      $this->category = $category;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryDescription()
    {
      return $this->categoryDescription;
    }

    /**
     * @param string $categoryDescription
     * @return ratingFactor
     */
    public function setCategoryDescription($categoryDescription)
    {
      $this->categoryDescription = $categoryDescription;
      return $this;
    }

    /**
     * @return ArrayOfratingFactorDetail
     */
    public function getRatingFactorDetails()
    {
      return $this->ratingFactorDetails;
    }

    /**
     * @param ArrayOfratingFactorDetail $ratingFactorDetails
     * @return ratingFactor
     */
    public function setRatingFactorDetails($ratingFactorDetails)
    {
      $this->ratingFactorDetails = $ratingFactorDetails;
      return $this;
    }

}
