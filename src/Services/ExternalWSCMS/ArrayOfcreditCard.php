<?php

class ArrayOfcreditCard implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var creditCard[] $creditCard
     */
    protected $creditCard = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return creditCard[]
     */
    public function getCreditCard()
    {
      return $this->creditCard;
    }

    /**
     * @param creditCard[] $creditCard
     * @return ArrayOfcreditCard
     */
    public function setCreditCard(array $creditCard = null)
    {
      $this->creditCard = $creditCard;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->creditCard[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return creditCard
     */
    public function offsetGet($offset)
    {
      return $this->creditCard[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param creditCard $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->creditCard[] = $value;
      } else {
        $this->creditCard[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->creditCard[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return creditCard Return the current element
     */
    public function current()
    {
      return current($this->creditCard);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->creditCard);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->creditCard);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->creditCard);
    }

    /**
     * Countable implementation
     *
     * @return creditCard Return count of elements
     */
    public function count()
    {
      return count($this->creditCard);
    }

}
