<?php

class ResponseSplitInfo extends Response
{

    /**
     * @var splitInfo $SplitInfo
     */
    protected $SplitInfo = null;

    
    public function __construct()
    {
      parent::__construct();
    }

    /**
     * @return splitInfo
     */
    public function getSplitInfo()
    {
      return $this->SplitInfo;
    }

    /**
     * @param splitInfo $SplitInfo
     * @return ResponseSplitInfo
     */
    public function setSplitInfo($SplitInfo)
    {
      $this->SplitInfo = $SplitInfo;
      return $this;
    }

}
