<?php

class ExternalWSCMS extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'GetSplitInfo' => '\\GetSplitInfo',
  'GetSplitInfoResponse' => '\\GetSplitInfoResponse',
  'GetMessagesByCategory' => '\\GetMessagesByCategory',
  'GetMessagesByCategoryResponse' => '\\GetMessagesByCategoryResponse',
  'GetSplitInfoRequest' => '\\GetSplitInfoRequest',
  'ResponseSplitInfo' => '\\ResponseSplitInfo',
  'Response' => '\\Response',
  'GetMessagesByCategoryRequest' => '\\GetMessagesByCategoryRequest',
  'ResponseMessagesByCategory' => '\\ResponseMessagesByCategory',
  'splitInfo' => '\\splitInfo',
  'ArrayOfpaymentFrequency' => '\\ArrayOfpaymentFrequency',
  'paymentFrequency' => '\\paymentFrequency',
  'ArrayOfpayment' => '\\ArrayOfpayment',
  'payment' => '\\payment',
  'ArrayOfcollector' => '\\ArrayOfcollector',
  'collector' => '\\collector',
  'ArrayOfcreditCard' => '\\ArrayOfcreditCard',
  'creditCard' => '\\creditCard',
  'ArrayOfbank' => '\\ArrayOfbank',
  'bank' => '\\bank',
  'ArrayOfbranch' => '\\ArrayOfbranch',
  'branch' => '\\branch',
  'ArrayOfproduct' => '\\ArrayOfproduct',
  'product' => '\\product',
  'ArrayOfbenefitLevel' => '\\ArrayOfbenefitLevel',
  'benefitLevel' => '\\benefitLevel',
  'ArrayOfpremium' => '\\ArrayOfpremium',
  'premium' => '\\premium',
  'ArrayOfproductSpecific' => '\\ArrayOfproductSpecific',
  'productSpecific' => '\\productSpecific',
  'ArrayOfproductSpecificDetail' => '\\ArrayOfproductSpecificDetail',
  'productSpecificDetail' => '\\productSpecificDetail',
  'ArrayOfratingFactor' => '\\ArrayOfratingFactor',
  'ratingFactor' => '\\ratingFactor',
  'ArrayOfratingFactorDetail' => '\\ArrayOfratingFactorDetail',
  'ratingFactorDetail' => '\\ratingFactorDetail',
  'ArrayOfrequiredProduct' => '\\ArrayOfrequiredProduct',
  'requiredProduct' => '\\requiredProduct',
  'ArrayOfProductScheduledBenefit' => '\\ArrayOfProductScheduledBenefit',
  'ProductScheduledBenefit' => '\\ProductScheduledBenefit',
  'ArrayOfProductCoverage' => '\\ArrayOfProductCoverage',
  'ProductCoverage' => '\\ProductCoverage',
  'ArrayOfProductPremium' => '\\ArrayOfProductPremium',
  'ProductPremium' => '\\ProductPremium',
  'ArrayOfstring' => '\\ArrayOfstring',
  'S6Message' => '\\S6Message',
  'ArrayOfMessage' => '\\ArrayOfMessage',
  'Message' => '\\Message',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
    
  foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
      $options = array_merge(array (
  'features' => 1,
), $options);
      if (!$wsdl) {
        $wsdl = 'https://testwebservices.chubblatinamerica.com/ExternalCmsWsUAT/ExternalWSCMS.svc?singlewsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetSplitInfo $parameters
     * @return GetSplitInfoResponse
     */
    public function GetSplitInfo(GetSplitInfo $parameters)
    {
      return $this->__soapCall('GetSplitInfo', array($parameters));
    }

    /**
     * @param GetMessagesByCategory $parameters
     * @return GetMessagesByCategoryResponse
     */
    public function GetMessagesByCategory(GetMessagesByCategory $parameters)
    {
      return $this->__soapCall('GetMessagesByCategory', array($parameters));
    }

}
