<?php

class creditCard
{

    /**
     * @var string $creditCardCode
     */
    protected $creditCardCode = null;

    /**
     * @var string $creditCardDescription
     */
    protected $creditCardDescription = null;

    /**
     * @var int $creditCardClass
     */
    protected $creditCardClass = null;

    /**
     * @var \DateTime $inactiveDate
     */
    protected $inactiveDate = null;

    /**
     * @var boolean $RequiredExpirationDate
     */
    protected $RequiredExpirationDate = null;

    /**
     * @var ArrayOfstring $Prefixes
     */
    protected $Prefixes = null;

    /**
     * @var ArrayOfstring $Templates
     */
    protected $Templates = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCreditCardCode()
    {
      return $this->creditCardCode;
    }

    /**
     * @param string $creditCardCode
     * @return creditCard
     */
    public function setCreditCardCode($creditCardCode)
    {
      $this->creditCardCode = $creditCardCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreditCardDescription()
    {
      return $this->creditCardDescription;
    }

    /**
     * @param string $creditCardDescription
     * @return creditCard
     */
    public function setCreditCardDescription($creditCardDescription)
    {
      $this->creditCardDescription = $creditCardDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getCreditCardClass()
    {
      return $this->creditCardClass;
    }

    /**
     * @param int $creditCardClass
     * @return creditCard
     */
    public function setCreditCardClass($creditCardClass)
    {
      $this->creditCardClass = $creditCardClass;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInactiveDate()
    {
      if ($this->inactiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->inactiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $inactiveDate
     * @return creditCard
     */
    public function setInactiveDate(\DateTime $inactiveDate = null)
    {
      if ($inactiveDate == null) {
       $this->inactiveDate = null;
      } else {
        $this->inactiveDate = $inactiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRequiredExpirationDate()
    {
      return $this->RequiredExpirationDate;
    }

    /**
     * @param boolean $RequiredExpirationDate
     * @return creditCard
     */
    public function setRequiredExpirationDate($RequiredExpirationDate)
    {
      $this->RequiredExpirationDate = $RequiredExpirationDate;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getPrefixes()
    {
      return $this->Prefixes;
    }

    /**
     * @param ArrayOfstring $Prefixes
     * @return creditCard
     */
    public function setPrefixes($Prefixes)
    {
      $this->Prefixes = $Prefixes;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getTemplates()
    {
      return $this->Templates;
    }

    /**
     * @param ArrayOfstring $Templates
     * @return creditCard
     */
    public function setTemplates($Templates)
    {
      $this->Templates = $Templates;
      return $this;
    }

}
