<?php

class benefitLevel
{

    /**
     * @var int $benefitLevelCode
     */
    protected $benefitLevelCode = null;

    /**
     * @var string $benefitLevelDescription
     */
    protected $benefitLevelDescription = null;

    /**
     * @var ArrayOfpremium $premiums
     */
    protected $premiums = null;

    /**
     * @var float $MAX_BEN_AMT_MI
     */
    protected $MAX_BEN_AMT_MI = null;

    /**
     * @var float $MAX_BEN_AMT_SP
     */
    protected $MAX_BEN_AMT_SP = null;

    /**
     * @var float $MAX_BEN_AMT_DP
     */
    protected $MAX_BEN_AMT_DP = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getBenefitLevelCode()
    {
      return $this->benefitLevelCode;
    }

    /**
     * @param int $benefitLevelCode
     * @return benefitLevel
     */
    public function setBenefitLevelCode($benefitLevelCode)
    {
      $this->benefitLevelCode = $benefitLevelCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBenefitLevelDescription()
    {
      return $this->benefitLevelDescription;
    }

    /**
     * @param string $benefitLevelDescription
     * @return benefitLevel
     */
    public function setBenefitLevelDescription($benefitLevelDescription)
    {
      $this->benefitLevelDescription = $benefitLevelDescription;
      return $this;
    }

    /**
     * @return ArrayOfpremium
     */
    public function getPremiums()
    {
      return $this->premiums;
    }

    /**
     * @param ArrayOfpremium $premiums
     * @return benefitLevel
     */
    public function setPremiums($premiums)
    {
      $this->premiums = $premiums;
      return $this;
    }

    /**
     * @return float
     */
    public function getMAX_BEN_AMT_MI()
    {
      return $this->MAX_BEN_AMT_MI;
    }

    /**
     * @param float $MAX_BEN_AMT_MI
     * @return benefitLevel
     */
    public function setMAX_BEN_AMT_MI($MAX_BEN_AMT_MI)
    {
      $this->MAX_BEN_AMT_MI = $MAX_BEN_AMT_MI;
      return $this;
    }

    /**
     * @return float
     */
    public function getMAX_BEN_AMT_SP()
    {
      return $this->MAX_BEN_AMT_SP;
    }

    /**
     * @param float $MAX_BEN_AMT_SP
     * @return benefitLevel
     */
    public function setMAX_BEN_AMT_SP($MAX_BEN_AMT_SP)
    {
      $this->MAX_BEN_AMT_SP = $MAX_BEN_AMT_SP;
      return $this;
    }

    /**
     * @return float
     */
    public function getMAX_BEN_AMT_DP()
    {
      return $this->MAX_BEN_AMT_DP;
    }

    /**
     * @param float $MAX_BEN_AMT_DP
     * @return benefitLevel
     */
    public function setMAX_BEN_AMT_DP($MAX_BEN_AMT_DP)
    {
      $this->MAX_BEN_AMT_DP = $MAX_BEN_AMT_DP;
      return $this;
    }

}
