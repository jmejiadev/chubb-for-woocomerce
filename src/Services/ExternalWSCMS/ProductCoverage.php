<?php

class ProductCoverage
{

    /**
     * @var int $BenefitLevelID
     */
    protected $BenefitLevelID = null;

    /**
     * @var string $CoverageCode
     */
    protected $CoverageCode = null;

    /**
     * @var string $CoverageDescription
     */
    protected $CoverageDescription = null;

    /**
     * @var string $Individual
     */
    protected $Individual = null;

    /**
     * @var float $MI_CVG_AMT
     */
    protected $MI_CVG_AMT = null;

    /**
     * @var float $MI_TAX_BEN_AMT
     */
    protected $MI_TAX_BEN_AMT = null;

    /**
     * @var float $SPS_CVG_AMT
     */
    protected $SPS_CVG_AMT = null;

    /**
     * @var float $SP_TAX_BEN_AMT
     */
    protected $SP_TAX_BEN_AMT = null;

    /**
     * @var float $DEP_CVG_AMT
     */
    protected $DEP_CVG_AMT = null;

    /**
     * @var float $DP_TAX_BEN_AMT
     */
    protected $DP_TAX_BEN_AMT = null;

    /**
     * @var float $MALE_PCT
     */
    protected $MALE_PCT = null;

    /**
     * @var float $FEMALE_PCT
     */
    protected $FEMALE_PCT = null;

    /**
     * @var float $UNISEX_PCT
     */
    protected $UNISEX_PCT = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getBenefitLevelID()
    {
      return $this->BenefitLevelID;
    }

    /**
     * @param int $BenefitLevelID
     * @return ProductCoverage
     */
    public function setBenefitLevelID($BenefitLevelID)
    {
      $this->BenefitLevelID = $BenefitLevelID;
      return $this;
    }

    /**
     * @return string
     */
    public function getCoverageCode()
    {
      return $this->CoverageCode;
    }

    /**
     * @param string $CoverageCode
     * @return ProductCoverage
     */
    public function setCoverageCode($CoverageCode)
    {
      $this->CoverageCode = $CoverageCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCoverageDescription()
    {
      return $this->CoverageDescription;
    }

    /**
     * @param string $CoverageDescription
     * @return ProductCoverage
     */
    public function setCoverageDescription($CoverageDescription)
    {
      $this->CoverageDescription = $CoverageDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getIndividual()
    {
      return $this->Individual;
    }

    /**
     * @param string $Individual
     * @return ProductCoverage
     */
    public function setIndividual($Individual)
    {
      $this->Individual = $Individual;
      return $this;
    }

    /**
     * @return float
     */
    public function getMI_CVG_AMT()
    {
      return $this->MI_CVG_AMT;
    }

    /**
     * @param float $MI_CVG_AMT
     * @return ProductCoverage
     */
    public function setMI_CVG_AMT($MI_CVG_AMT)
    {
      $this->MI_CVG_AMT = $MI_CVG_AMT;
      return $this;
    }

    /**
     * @return float
     */
    public function getMI_TAX_BEN_AMT()
    {
      return $this->MI_TAX_BEN_AMT;
    }

    /**
     * @param float $MI_TAX_BEN_AMT
     * @return ProductCoverage
     */
    public function setMI_TAX_BEN_AMT($MI_TAX_BEN_AMT)
    {
      $this->MI_TAX_BEN_AMT = $MI_TAX_BEN_AMT;
      return $this;
    }

    /**
     * @return float
     */
    public function getSPS_CVG_AMT()
    {
      return $this->SPS_CVG_AMT;
    }

    /**
     * @param float $SPS_CVG_AMT
     * @return ProductCoverage
     */
    public function setSPS_CVG_AMT($SPS_CVG_AMT)
    {
      $this->SPS_CVG_AMT = $SPS_CVG_AMT;
      return $this;
    }

    /**
     * @return float
     */
    public function getSP_TAX_BEN_AMT()
    {
      return $this->SP_TAX_BEN_AMT;
    }

    /**
     * @param float $SP_TAX_BEN_AMT
     * @return ProductCoverage
     */
    public function setSP_TAX_BEN_AMT($SP_TAX_BEN_AMT)
    {
      $this->SP_TAX_BEN_AMT = $SP_TAX_BEN_AMT;
      return $this;
    }

    /**
     * @return float
     */
    public function getDEP_CVG_AMT()
    {
      return $this->DEP_CVG_AMT;
    }

    /**
     * @param float $DEP_CVG_AMT
     * @return ProductCoverage
     */
    public function setDEP_CVG_AMT($DEP_CVG_AMT)
    {
      $this->DEP_CVG_AMT = $DEP_CVG_AMT;
      return $this;
    }

    /**
     * @return float
     */
    public function getDP_TAX_BEN_AMT()
    {
      return $this->DP_TAX_BEN_AMT;
    }

    /**
     * @param float $DP_TAX_BEN_AMT
     * @return ProductCoverage
     */
    public function setDP_TAX_BEN_AMT($DP_TAX_BEN_AMT)
    {
      $this->DP_TAX_BEN_AMT = $DP_TAX_BEN_AMT;
      return $this;
    }

    /**
     * @return float
     */
    public function getMALE_PCT()
    {
      return $this->MALE_PCT;
    }

    /**
     * @param float $MALE_PCT
     * @return ProductCoverage
     */
    public function setMALE_PCT($MALE_PCT)
    {
      $this->MALE_PCT = $MALE_PCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getFEMALE_PCT()
    {
      return $this->FEMALE_PCT;
    }

    /**
     * @param float $FEMALE_PCT
     * @return ProductCoverage
     */
    public function setFEMALE_PCT($FEMALE_PCT)
    {
      $this->FEMALE_PCT = $FEMALE_PCT;
      return $this;
    }

    /**
     * @return float
     */
    public function getUNISEX_PCT()
    {
      return $this->UNISEX_PCT;
    }

    /**
     * @param float $UNISEX_PCT
     * @return ProductCoverage
     */
    public function setUNISEX_PCT($UNISEX_PCT)
    {
      $this->UNISEX_PCT = $UNISEX_PCT;
      return $this;
    }

}
