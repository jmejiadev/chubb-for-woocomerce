<?php

class paymentFrequency
{

    /**
     * @var int $paymentFrequencyCode
     */
    protected $paymentFrequencyCode = null;

    /**
     * @var string $paymentFrequencyName
     */
    protected $paymentFrequencyName = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getPaymentFrequencyCode()
    {
      return $this->paymentFrequencyCode;
    }

    /**
     * @param int $paymentFrequencyCode
     * @return paymentFrequency
     */
    public function setPaymentFrequencyCode($paymentFrequencyCode)
    {
      $this->paymentFrequencyCode = $paymentFrequencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentFrequencyName()
    {
      return $this->paymentFrequencyName;
    }

    /**
     * @param string $paymentFrequencyName
     * @return paymentFrequency
     */
    public function setPaymentFrequencyName($paymentFrequencyName)
    {
      $this->paymentFrequencyName = $paymentFrequencyName;
      return $this;
    }

}
