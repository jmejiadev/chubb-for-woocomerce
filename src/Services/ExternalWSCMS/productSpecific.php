<?php

class productSpecific
{

    /**
     * @var string $categoryCode
     */
    protected $categoryCode = null;

    /**
     * @var string $categoryDescription
     */
    protected $categoryDescription = null;

    /**
     * @var string $collectingLevel
     */
    protected $collectingLevel = null;

    /**
     * @var boolean $allowMultipleValues
     */
    protected $allowMultipleValues = null;

    /**
     * @var ArrayOfproductSpecificDetail $productSpecificDetails
     */
    protected $productSpecificDetails = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCategoryCode()
    {
      return $this->categoryCode;
    }

    /**
     * @param string $categoryCode
     * @return productSpecific
     */
    public function setCategoryCode($categoryCode)
    {
      $this->categoryCode = $categoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryDescription()
    {
      return $this->categoryDescription;
    }

    /**
     * @param string $categoryDescription
     * @return productSpecific
     */
    public function setCategoryDescription($categoryDescription)
    {
      $this->categoryDescription = $categoryDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollectingLevel()
    {
      return $this->collectingLevel;
    }

    /**
     * @param string $collectingLevel
     * @return productSpecific
     */
    public function setCollectingLevel($collectingLevel)
    {
      $this->collectingLevel = $collectingLevel;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowMultipleValues()
    {
      return $this->allowMultipleValues;
    }

    /**
     * @param boolean $allowMultipleValues
     * @return productSpecific
     */
    public function setAllowMultipleValues($allowMultipleValues)
    {
      $this->allowMultipleValues = $allowMultipleValues;
      return $this;
    }

    /**
     * @return ArrayOfproductSpecificDetail
     */
    public function getProductSpecificDetails()
    {
      return $this->productSpecificDetails;
    }

    /**
     * @param ArrayOfproductSpecificDetail $productSpecificDetails
     * @return productSpecific
     */
    public function setProductSpecificDetails($productSpecificDetails)
    {
      $this->productSpecificDetails = $productSpecificDetails;
      return $this;
    }

}
