<?php

class requiredProduct
{

    /**
     * @var string $id
     */
    protected $id = null;

    /**
     * @var string $description
     */
    protected $description = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getId()
    {
      return $this->id;
    }

    /**
     * @param string $id
     * @return requiredProduct
     */
    public function setId($id)
    {
      $this->id = $id;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return requiredProduct
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

}
