<?php

class ArrayOfProductCoverage implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductCoverage[] $ProductCoverage
     */
    protected $ProductCoverage = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductCoverage[]
     */
    public function getProductCoverage()
    {
      return $this->ProductCoverage;
    }

    /**
     * @param ProductCoverage[] $ProductCoverage
     * @return ArrayOfProductCoverage
     */
    public function setProductCoverage(array $ProductCoverage = null)
    {
      $this->ProductCoverage = $ProductCoverage;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductCoverage[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductCoverage
     */
    public function offsetGet($offset)
    {
      return $this->ProductCoverage[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductCoverage $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ProductCoverage[] = $value;
      } else {
        $this->ProductCoverage[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductCoverage[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductCoverage Return the current element
     */
    public function current()
    {
      return current($this->ProductCoverage);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductCoverage);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductCoverage);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductCoverage);
    }

    /**
     * Countable implementation
     *
     * @return ProductCoverage Return count of elements
     */
    public function count()
    {
      return count($this->ProductCoverage);
    }

}
