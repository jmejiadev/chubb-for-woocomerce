<?php

class Response
{

    /**
     * @var string $ErrorCode
     */
    protected $ErrorCode = null;

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    /**
     * @var boolean $HasError
     */
    protected $HasError = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
      return $this->ErrorCode;
    }

    /**
     * @param string $ErrorCode
     * @return Response
     */
    public function setErrorCode($ErrorCode)
    {
      $this->ErrorCode = $ErrorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return Response
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getHasError()
    {
      return $this->HasError;
    }

    /**
     * @param boolean $HasError
     * @return Response
     */
    public function setHasError($HasError)
    {
      $this->HasError = $HasError;
      return $this;
    }

}
