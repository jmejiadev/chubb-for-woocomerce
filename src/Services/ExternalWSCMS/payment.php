<?php

class payment
{

    /**
     * @var int $paymentMethodCode
     */
    protected $paymentMethodCode = null;

    /**
     * @var string $paymentMethodDescription
     */
    protected $paymentMethodDescription = null;

    /**
     * @var ArrayOfcollector $collectors
     */
    protected $collectors = null;

    /**
     * @var \DateTime $inactiveDate
     */
    protected $inactiveDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getPaymentMethodCode()
    {
      return $this->paymentMethodCode;
    }

    /**
     * @param int $paymentMethodCode
     * @return payment
     */
    public function setPaymentMethodCode($paymentMethodCode)
    {
      $this->paymentMethodCode = $paymentMethodCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPaymentMethodDescription()
    {
      return $this->paymentMethodDescription;
    }

    /**
     * @param string $paymentMethodDescription
     * @return payment
     */
    public function setPaymentMethodDescription($paymentMethodDescription)
    {
      $this->paymentMethodDescription = $paymentMethodDescription;
      return $this;
    }

    /**
     * @return ArrayOfcollector
     */
    public function getCollectors()
    {
      return $this->collectors;
    }

    /**
     * @param ArrayOfcollector $collectors
     * @return payment
     */
    public function setCollectors($collectors)
    {
      $this->collectors = $collectors;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInactiveDate()
    {
      if ($this->inactiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->inactiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $inactiveDate
     * @return payment
     */
    public function setInactiveDate(\DateTime $inactiveDate = null)
    {
      if ($inactiveDate == null) {
       $this->inactiveDate = null;
      } else {
        $this->inactiveDate = $inactiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

}
