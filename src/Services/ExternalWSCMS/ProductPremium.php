<?php

class ProductPremium
{

    /**
     * @var int $BenefitLevelID
     */
    protected $BenefitLevelID = null;

    /**
     * @var string $Individual
     */
    protected $Individual = null;

    /**
     * @var int $Rf_Combo_SeqNum
     */
    protected $Rf_Combo_SeqNum = null;

    /**
     * @var int $ReferenceCode
     */
    protected $ReferenceCode = null;

    /**
     * @var string $ReferenceDescription
     */
    protected $ReferenceDescription = null;

    /**
     * @var string $CategoryCode
     */
    protected $CategoryCode = null;

    /**
     * @var string $CategoryDescription
     */
    protected $CategoryDescription = null;

    /**
     * @var float $Premium
     */
    protected $Premium = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return int
     */
    public function getBenefitLevelID()
    {
      return $this->BenefitLevelID;
    }

    /**
     * @param int $BenefitLevelID
     * @return ProductPremium
     */
    public function setBenefitLevelID($BenefitLevelID)
    {
      $this->BenefitLevelID = $BenefitLevelID;
      return $this;
    }

    /**
     * @return string
     */
    public function getIndividual()
    {
      return $this->Individual;
    }

    /**
     * @param string $Individual
     * @return ProductPremium
     */
    public function setIndividual($Individual)
    {
      $this->Individual = $Individual;
      return $this;
    }

    /**
     * @return int
     */
    public function getRf_Combo_SeqNum()
    {
      return $this->Rf_Combo_SeqNum;
    }

    /**
     * @param int $Rf_Combo_SeqNum
     * @return ProductPremium
     */
    public function setRf_Combo_SeqNum($Rf_Combo_SeqNum)
    {
      $this->Rf_Combo_SeqNum = $Rf_Combo_SeqNum;
      return $this;
    }

    /**
     * @return int
     */
    public function getReferenceCode()
    {
      return $this->ReferenceCode;
    }

    /**
     * @param int $ReferenceCode
     * @return ProductPremium
     */
    public function setReferenceCode($ReferenceCode)
    {
      $this->ReferenceCode = $ReferenceCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getReferenceDescription()
    {
      return $this->ReferenceDescription;
    }

    /**
     * @param string $ReferenceDescription
     * @return ProductPremium
     */
    public function setReferenceDescription($ReferenceDescription)
    {
      $this->ReferenceDescription = $ReferenceDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryCode()
    {
      return $this->CategoryCode;
    }

    /**
     * @param string $CategoryCode
     * @return ProductPremium
     */
    public function setCategoryCode($CategoryCode)
    {
      $this->CategoryCode = $CategoryCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCategoryDescription()
    {
      return $this->CategoryDescription;
    }

    /**
     * @param string $CategoryDescription
     * @return ProductPremium
     */
    public function setCategoryDescription($CategoryDescription)
    {
      $this->CategoryDescription = $CategoryDescription;
      return $this;
    }

    /**
     * @return float
     */
    public function getPremium()
    {
      return $this->Premium;
    }

    /**
     * @param float $Premium
     * @return ProductPremium
     */
    public function setPremium($Premium)
    {
      $this->Premium = $Premium;
      return $this;
    }

}
