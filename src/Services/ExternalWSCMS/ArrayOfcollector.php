<?php

class ArrayOfcollector implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var collector[] $collector
     */
    protected $collector = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return collector[]
     */
    public function getCollector()
    {
      return $this->collector;
    }

    /**
     * @param collector[] $collector
     * @return ArrayOfcollector
     */
    public function setCollector(array $collector = null)
    {
      $this->collector = $collector;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->collector[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return collector
     */
    public function offsetGet($offset)
    {
      return $this->collector[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param collector $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->collector[] = $value;
      } else {
        $this->collector[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->collector[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return collector Return the current element
     */
    public function current()
    {
      return current($this->collector);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->collector);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->collector);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->collector);
    }

    /**
     * Countable implementation
     *
     * @return collector Return count of elements
     */
    public function count()
    {
      return count($this->collector);
    }

}
