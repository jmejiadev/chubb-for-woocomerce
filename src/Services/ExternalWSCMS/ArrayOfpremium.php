<?php

class ArrayOfpremium implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var premium[] $premium
     */
    protected $premium = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return premium[]
     */
    public function getPremium()
    {
      return $this->premium;
    }

    /**
     * @param premium[] $premium
     * @return ArrayOfpremium
     */
    public function setPremium(array $premium = null)
    {
      $this->premium = $premium;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->premium[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return premium
     */
    public function offsetGet($offset)
    {
      return $this->premium[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param premium $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->premium[] = $value;
      } else {
        $this->premium[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->premium[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return premium Return the current element
     */
    public function current()
    {
      return current($this->premium);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->premium);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->premium);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->premium);
    }

    /**
     * Countable implementation
     *
     * @return premium Return count of elements
     */
    public function count()
    {
      return count($this->premium);
    }

}
