<?php

class GetSplitInfoResponse
{

    /**
     * @var ResponseSplitInfo $GetSplitInfoResult
     */
    protected $GetSplitInfoResult = null;

    /**
     * @param ResponseSplitInfo $GetSplitInfoResult
     */
    public function __construct($GetSplitInfoResult)
    {
      $this->GetSplitInfoResult = $GetSplitInfoResult;
    }

    /**
     * @return ResponseSplitInfo
     */
    public function getGetSplitInfoResult()
    {
      return $this->GetSplitInfoResult;
    }

    /**
     * @param ResponseSplitInfo $GetSplitInfoResult
     * @return GetSplitInfoResponse
     */
    public function setGetSplitInfoResult($GetSplitInfoResult)
    {
      $this->GetSplitInfoResult = $GetSplitInfoResult;
      return $this;
    }

}
