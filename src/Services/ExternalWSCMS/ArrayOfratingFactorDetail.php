<?php

class ArrayOfratingFactorDetail implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ratingFactorDetail[] $ratingFactorDetail
     */
    protected $ratingFactorDetail = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ratingFactorDetail[]
     */
    public function getRatingFactorDetail()
    {
      return $this->ratingFactorDetail;
    }

    /**
     * @param ratingFactorDetail[] $ratingFactorDetail
     * @return ArrayOfratingFactorDetail
     */
    public function setRatingFactorDetail(array $ratingFactorDetail = null)
    {
      $this->ratingFactorDetail = $ratingFactorDetail;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ratingFactorDetail[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ratingFactorDetail
     */
    public function offsetGet($offset)
    {
      return $this->ratingFactorDetail[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ratingFactorDetail $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ratingFactorDetail[] = $value;
      } else {
        $this->ratingFactorDetail[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ratingFactorDetail[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ratingFactorDetail Return the current element
     */
    public function current()
    {
      return current($this->ratingFactorDetail);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ratingFactorDetail);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ratingFactorDetail);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ratingFactorDetail);
    }

    /**
     * Countable implementation
     *
     * @return ratingFactorDetail Return count of elements
     */
    public function count()
    {
      return count($this->ratingFactorDetail);
    }

}
