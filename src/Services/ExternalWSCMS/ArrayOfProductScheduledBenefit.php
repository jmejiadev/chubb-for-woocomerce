<?php

class ArrayOfProductScheduledBenefit implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var ProductScheduledBenefit[] $ProductScheduledBenefit
     */
    protected $ProductScheduledBenefit = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return ProductScheduledBenefit[]
     */
    public function getProductScheduledBenefit()
    {
      return $this->ProductScheduledBenefit;
    }

    /**
     * @param ProductScheduledBenefit[] $ProductScheduledBenefit
     * @return ArrayOfProductScheduledBenefit
     */
    public function setProductScheduledBenefit(array $ProductScheduledBenefit = null)
    {
      $this->ProductScheduledBenefit = $ProductScheduledBenefit;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->ProductScheduledBenefit[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return ProductScheduledBenefit
     */
    public function offsetGet($offset)
    {
      return $this->ProductScheduledBenefit[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param ProductScheduledBenefit $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->ProductScheduledBenefit[] = $value;
      } else {
        $this->ProductScheduledBenefit[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->ProductScheduledBenefit[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return ProductScheduledBenefit Return the current element
     */
    public function current()
    {
      return current($this->ProductScheduledBenefit);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->ProductScheduledBenefit);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->ProductScheduledBenefit);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->ProductScheduledBenefit);
    }

    /**
     * Countable implementation
     *
     * @return ProductScheduledBenefit Return count of elements
     */
    public function count()
    {
      return count($this->ProductScheduledBenefit);
    }

}
