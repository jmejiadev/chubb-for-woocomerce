<?php

class ratingFactorDetail
{

    /**
     * @var string $reference
     */
    protected $reference = null;

    /**
     * @var string $description
     */
    protected $description = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getReference()
    {
      return $this->reference;
    }

    /**
     * @param string $reference
     * @return ratingFactorDetail
     */
    public function setReference($reference)
    {
      $this->reference = $reference;
      return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
      return $this->description;
    }

    /**
     * @param string $description
     * @return ratingFactorDetail
     */
    public function setDescription($description)
    {
      $this->description = $description;
      return $this;
    }

}
