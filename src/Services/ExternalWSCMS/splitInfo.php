<?php

class splitInfo
{

    /**
     * @var string $splitKey
     */
    protected $splitKey = null;

    /**
     * @var string $splitDescription
     */
    protected $splitDescription = null;

    /**
     * @var int $displayStatusCode
     */
    protected $displayStatusCode = null;

    /**
     * @var string $sponsorCode
     */
    protected $sponsorCode = null;

    /**
     * @var string $splitType
     */
    protected $splitType = null;

    /**
     * @var string $sponsorName
     */
    protected $sponsorName = null;

    /**
     * @var string $prodPackage
     */
    protected $prodPackage = null;

    /**
     * @var string $prodPackageDescription
     */
    protected $prodPackageDescription = null;

    /**
     * @var string $prodPackageMarketingDesc
     */
    protected $prodPackageMarketingDesc = null;

    /**
     * @var string $policyPrefix
     */
    protected $policyPrefix = null;

    /**
     * @var boolean $billNow
     */
    protected $billNow = null;

    /**
     * @var boolean $reconcileNow
     */
    protected $reconcileNow = null;

    /**
     * @var int $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var string $currencyDescription
     */
    protected $currencyDescription = null;

    /**
     * @var int $lob
     */
    protected $lob = null;

    /**
     * @var string $lobDescription
     */
    protected $lobDescription = null;

    /**
     * @var ArrayOfpaymentFrequency $paymentFrequencies
     */
    protected $paymentFrequencies = null;

    /**
     * @var ArrayOfpayment $payments
     */
    protected $payments = null;

    /**
     * @var ArrayOfproduct $products
     */
    protected $products = null;

    /**
     * @var string $primaryMediaCode
     */
    protected $primaryMediaCode = null;

    /**
     * @var string $primaryMediaDescription
     */
    protected $primaryMediaDescription = null;

    /**
     * @var int $marketingApproachCode
     */
    protected $marketingApproachCode = null;

    /**
     * @var string $marketingApproachDescription
     */
    protected $marketingApproachDescription = null;

    /**
     * @var \DateTime $plannedLaunchDate
     */
    protected $plannedLaunchDate = null;

    /**
     * @var boolean $solicitationCostRequired
     */
    protected $solicitationCostRequired = null;

    /**
     * @var string $policyTransactionCode
     */
    protected $policyTransactionCode = null;

    /**
     * @var \DateTime $policyEffectiveDate
     */
    protected $policyEffectiveDate = null;

    /**
     * @var int $minimumProductNumberRequired
     */
    protected $minimumProductNumberRequired = null;

    /**
     * @var int $maximumProductNumberRequired
     */
    protected $maximumProductNumberRequired = null;

    /**
     * @var string $premiumInfoLink
     */
    protected $premiumInfoLink = null;

    /**
     * @var int $CampaignStatus
     */
    protected $CampaignStatus = null;

    /**
     * @var int $ApprovedCode
     */
    protected $ApprovedCode = null;

    /**
     * @var \DateTime $actualLaunchDate
     */
    protected $actualLaunchDate = null;

    /**
     * @var string $policyTransactionDescription
     */
    protected $policyTransactionDescription = null;

    /**
     * @var string $sourceListDescription
     */
    protected $sourceListDescription = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getSplitKey()
    {
      return $this->splitKey;
    }

    /**
     * @param string $splitKey
     * @return splitInfo
     */
    public function setSplitKey($splitKey)
    {
      $this->splitKey = $splitKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSplitDescription()
    {
      return $this->splitDescription;
    }

    /**
     * @param string $splitDescription
     * @return splitInfo
     */
    public function setSplitDescription($splitDescription)
    {
      $this->splitDescription = $splitDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getDisplayStatusCode()
    {
      return $this->displayStatusCode;
    }

    /**
     * @param int $displayStatusCode
     * @return splitInfo
     */
    public function setDisplayStatusCode($displayStatusCode)
    {
      $this->displayStatusCode = $displayStatusCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSponsorCode()
    {
      return $this->sponsorCode;
    }

    /**
     * @param string $sponsorCode
     * @return splitInfo
     */
    public function setSponsorCode($sponsorCode)
    {
      $this->sponsorCode = $sponsorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSplitType()
    {
      return $this->splitType;
    }

    /**
     * @param string $splitType
     * @return splitInfo
     */
    public function setSplitType($splitType)
    {
      $this->splitType = $splitType;
      return $this;
    }

    /**
     * @return string
     */
    public function getSponsorName()
    {
      return $this->sponsorName;
    }

    /**
     * @param string $sponsorName
     * @return splitInfo
     */
    public function setSponsorName($sponsorName)
    {
      $this->sponsorName = $sponsorName;
      return $this;
    }

    /**
     * @return string
     */
    public function getProdPackage()
    {
      return $this->prodPackage;
    }

    /**
     * @param string $prodPackage
     * @return splitInfo
     */
    public function setProdPackage($prodPackage)
    {
      $this->prodPackage = $prodPackage;
      return $this;
    }

    /**
     * @return string
     */
    public function getProdPackageDescription()
    {
      return $this->prodPackageDescription;
    }

    /**
     * @param string $prodPackageDescription
     * @return splitInfo
     */
    public function setProdPackageDescription($prodPackageDescription)
    {
      $this->prodPackageDescription = $prodPackageDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getProdPackageMarketingDesc()
    {
      return $this->prodPackageMarketingDesc;
    }

    /**
     * @param string $prodPackageMarketingDesc
     * @return splitInfo
     */
    public function setProdPackageMarketingDesc($prodPackageMarketingDesc)
    {
      $this->prodPackageMarketingDesc = $prodPackageMarketingDesc;
      return $this;
    }

    /**
     * @return string
     */
    public function getPolicyPrefix()
    {
      return $this->policyPrefix;
    }

    /**
     * @param string $policyPrefix
     * @return splitInfo
     */
    public function setPolicyPrefix($policyPrefix)
    {
      $this->policyPrefix = $policyPrefix;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getBillNow()
    {
      return $this->billNow;
    }

    /**
     * @param boolean $billNow
     * @return splitInfo
     */
    public function setBillNow($billNow)
    {
      $this->billNow = $billNow;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getReconcileNow()
    {
      return $this->reconcileNow;
    }

    /**
     * @param boolean $reconcileNow
     * @return splitInfo
     */
    public function setReconcileNow($reconcileNow)
    {
      $this->reconcileNow = $reconcileNow;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param int $currencyCode
     * @return splitInfo
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyDescription()
    {
      return $this->currencyDescription;
    }

    /**
     * @param string $currencyDescription
     * @return splitInfo
     */
    public function setCurrencyDescription($currencyDescription)
    {
      $this->currencyDescription = $currencyDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getLob()
    {
      return $this->lob;
    }

    /**
     * @param int $lob
     * @return splitInfo
     */
    public function setLob($lob)
    {
      $this->lob = $lob;
      return $this;
    }

    /**
     * @return string
     */
    public function getLobDescription()
    {
      return $this->lobDescription;
    }

    /**
     * @param string $lobDescription
     * @return splitInfo
     */
    public function setLobDescription($lobDescription)
    {
      $this->lobDescription = $lobDescription;
      return $this;
    }

    /**
     * @return ArrayOfpaymentFrequency
     */
    public function getPaymentFrequencies()
    {
      return $this->paymentFrequencies;
    }

    /**
     * @param ArrayOfpaymentFrequency $paymentFrequencies
     * @return splitInfo
     */
    public function setPaymentFrequencies($paymentFrequencies)
    {
      $this->paymentFrequencies = $paymentFrequencies;
      return $this;
    }

    /**
     * @return ArrayOfpayment
     */
    public function getPayments()
    {
      return $this->payments;
    }

    /**
     * @param ArrayOfpayment $payments
     * @return splitInfo
     */
    public function setPayments($payments)
    {
      $this->payments = $payments;
      return $this;
    }

    /**
     * @return ArrayOfproduct
     */
    public function getProducts()
    {
      return $this->products;
    }

    /**
     * @param ArrayOfproduct $products
     * @return splitInfo
     */
    public function setProducts($products)
    {
      $this->products = $products;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryMediaCode()
    {
      return $this->primaryMediaCode;
    }

    /**
     * @param string $primaryMediaCode
     * @return splitInfo
     */
    public function setPrimaryMediaCode($primaryMediaCode)
    {
      $this->primaryMediaCode = $primaryMediaCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryMediaDescription()
    {
      return $this->primaryMediaDescription;
    }

    /**
     * @param string $primaryMediaDescription
     * @return splitInfo
     */
    public function setPrimaryMediaDescription($primaryMediaDescription)
    {
      $this->primaryMediaDescription = $primaryMediaDescription;
      return $this;
    }

    /**
     * @return int
     */
    public function getMarketingApproachCode()
    {
      return $this->marketingApproachCode;
    }

    /**
     * @param int $marketingApproachCode
     * @return splitInfo
     */
    public function setMarketingApproachCode($marketingApproachCode)
    {
      $this->marketingApproachCode = $marketingApproachCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getMarketingApproachDescription()
    {
      return $this->marketingApproachDescription;
    }

    /**
     * @param string $marketingApproachDescription
     * @return splitInfo
     */
    public function setMarketingApproachDescription($marketingApproachDescription)
    {
      $this->marketingApproachDescription = $marketingApproachDescription;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPlannedLaunchDate()
    {
      if ($this->plannedLaunchDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->plannedLaunchDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $plannedLaunchDate
     * @return splitInfo
     */
    public function setPlannedLaunchDate(\DateTime $plannedLaunchDate = null)
    {
      if ($plannedLaunchDate == null) {
       $this->plannedLaunchDate = null;
      } else {
        $this->plannedLaunchDate = $plannedLaunchDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return boolean
     */
    public function getSolicitationCostRequired()
    {
      return $this->solicitationCostRequired;
    }

    /**
     * @param boolean $solicitationCostRequired
     * @return splitInfo
     */
    public function setSolicitationCostRequired($solicitationCostRequired)
    {
      $this->solicitationCostRequired = $solicitationCostRequired;
      return $this;
    }

    /**
     * @return string
     */
    public function getPolicyTransactionCode()
    {
      return $this->policyTransactionCode;
    }

    /**
     * @param string $policyTransactionCode
     * @return splitInfo
     */
    public function setPolicyTransactionCode($policyTransactionCode)
    {
      $this->policyTransactionCode = $policyTransactionCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPolicyEffectiveDate()
    {
      if ($this->policyEffectiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->policyEffectiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $policyEffectiveDate
     * @return splitInfo
     */
    public function setPolicyEffectiveDate(\DateTime $policyEffectiveDate = null)
    {
      if ($policyEffectiveDate == null) {
       $this->policyEffectiveDate = null;
      } else {
        $this->policyEffectiveDate = $policyEffectiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return int
     */
    public function getMinimumProductNumberRequired()
    {
      return $this->minimumProductNumberRequired;
    }

    /**
     * @param int $minimumProductNumberRequired
     * @return splitInfo
     */
    public function setMinimumProductNumberRequired($minimumProductNumberRequired)
    {
      $this->minimumProductNumberRequired = $minimumProductNumberRequired;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaximumProductNumberRequired()
    {
      return $this->maximumProductNumberRequired;
    }

    /**
     * @param int $maximumProductNumberRequired
     * @return splitInfo
     */
    public function setMaximumProductNumberRequired($maximumProductNumberRequired)
    {
      $this->maximumProductNumberRequired = $maximumProductNumberRequired;
      return $this;
    }

    /**
     * @return string
     */
    public function getPremiumInfoLink()
    {
      return $this->premiumInfoLink;
    }

    /**
     * @param string $premiumInfoLink
     * @return splitInfo
     */
    public function setPremiumInfoLink($premiumInfoLink)
    {
      $this->premiumInfoLink = $premiumInfoLink;
      return $this;
    }

    /**
     * @return int
     */
    public function getCampaignStatus()
    {
      return $this->CampaignStatus;
    }

    /**
     * @param int $CampaignStatus
     * @return splitInfo
     */
    public function setCampaignStatus($CampaignStatus)
    {
      $this->CampaignStatus = $CampaignStatus;
      return $this;
    }

    /**
     * @return int
     */
    public function getApprovedCode()
    {
      return $this->ApprovedCode;
    }

    /**
     * @param int $ApprovedCode
     * @return splitInfo
     */
    public function setApprovedCode($ApprovedCode)
    {
      $this->ApprovedCode = $ApprovedCode;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActualLaunchDate()
    {
      if ($this->actualLaunchDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->actualLaunchDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $actualLaunchDate
     * @return splitInfo
     */
    public function setActualLaunchDate(\DateTime $actualLaunchDate = null)
    {
      if ($actualLaunchDate == null) {
       $this->actualLaunchDate = null;
      } else {
        $this->actualLaunchDate = $actualLaunchDate->format(\DateTime::ATOM);
      }
      return $this;
    }

    /**
     * @return string
     */
    public function getPolicyTransactionDescription()
    {
      return $this->policyTransactionDescription;
    }

    /**
     * @param string $policyTransactionDescription
     * @return splitInfo
     */
    public function setPolicyTransactionDescription($policyTransactionDescription)
    {
      $this->policyTransactionDescription = $policyTransactionDescription;
      return $this;
    }

    /**
     * @return string
     */
    public function getSourceListDescription()
    {
      return $this->sourceListDescription;
    }

    /**
     * @param string $sourceListDescription
     * @return splitInfo
     */
    public function setSourceListDescription($sourceListDescription)
    {
      $this->sourceListDescription = $sourceListDescription;
      return $this;
    }

}
