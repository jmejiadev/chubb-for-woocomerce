<?php

class ArrayOfbranch implements \ArrayAccess, \Iterator, \Countable
{

    /**
     * @var branch[] $branch
     */
    protected $branch = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return branch[]
     */
    public function getBranch()
    {
      return $this->branch;
    }

    /**
     * @param branch[] $branch
     * @return ArrayOfbranch
     */
    public function setBranch(array $branch = null)
    {
      $this->branch = $branch;
      return $this;
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset An offset to check for
     * @return boolean true on success or false on failure
     */
    public function offsetExists($offset)
    {
      return isset($this->branch[$offset]);
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to retrieve
     * @return branch
     */
    public function offsetGet($offset)
    {
      return $this->branch[$offset];
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to assign the value to
     * @param branch $value The value to set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
      if (!isset($offset)) {
        $this->branch[] = $value;
      } else {
        $this->branch[$offset] = $value;
      }
    }

    /**
     * ArrayAccess implementation
     *
     * @param mixed $offset The offset to unset
     * @return void
     */
    public function offsetUnset($offset)
    {
      unset($this->branch[$offset]);
    }

    /**
     * Iterator implementation
     *
     * @return branch Return the current element
     */
    public function current()
    {
      return current($this->branch);
    }

    /**
     * Iterator implementation
     * Move forward to next element
     *
     * @return void
     */
    public function next()
    {
      next($this->branch);
    }

    /**
     * Iterator implementation
     *
     * @return string|null Return the key of the current element or null
     */
    public function key()
    {
      return key($this->branch);
    }

    /**
     * Iterator implementation
     *
     * @return boolean Return the validity of the current position
     */
    public function valid()
    {
      return $this->key() !== null;
    }

    /**
     * Iterator implementation
     * Rewind the Iterator to the first element
     *
     * @return void
     */
    public function rewind()
    {
      reset($this->branch);
    }

    /**
     * Countable implementation
     *
     * @return branch Return count of elements
     */
    public function count()
    {
      return count($this->branch);
    }

}
