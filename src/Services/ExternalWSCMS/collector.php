<?php

class collector
{

    /**
     * @var string $collectorCode
     */
    protected $collectorCode = null;

    /**
     * @var string $collectorName
     */
    protected $collectorName = null;

    /**
     * @var ArrayOfcreditCard $creditCards
     */
    protected $creditCards = null;

    /**
     * @var ArrayOfbank $banks
     */
    protected $banks = null;

    /**
     * @var \DateTime $inactiveDate
     */
    protected $inactiveDate = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCollectorCode()
    {
      return $this->collectorCode;
    }

    /**
     * @param string $collectorCode
     * @return collector
     */
    public function setCollectorCode($collectorCode)
    {
      $this->collectorCode = $collectorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCollectorName()
    {
      return $this->collectorName;
    }

    /**
     * @param string $collectorName
     * @return collector
     */
    public function setCollectorName($collectorName)
    {
      $this->collectorName = $collectorName;
      return $this;
    }

    /**
     * @return ArrayOfcreditCard
     */
    public function getCreditCards()
    {
      return $this->creditCards;
    }

    /**
     * @param ArrayOfcreditCard $creditCards
     * @return collector
     */
    public function setCreditCards($creditCards)
    {
      $this->creditCards = $creditCards;
      return $this;
    }

    /**
     * @return ArrayOfbank
     */
    public function getBanks()
    {
      return $this->banks;
    }

    /**
     * @param ArrayOfbank $banks
     * @return collector
     */
    public function setBanks($banks)
    {
      $this->banks = $banks;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInactiveDate()
    {
      if ($this->inactiveDate == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->inactiveDate);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $inactiveDate
     * @return collector
     */
    public function setInactiveDate(\DateTime $inactiveDate = null)
    {
      if ($inactiveDate == null) {
       $this->inactiveDate = null;
      } else {
        $this->inactiveDate = $inactiveDate->format(\DateTime::ATOM);
      }
      return $this;
    }

}
