<?php

class product
{

    /**
     * @var string $productId
     */
    protected $productId = null;

    /**
     * @var string $productDescription
     */
    protected $productDescription = null;

    /**
     * @var boolean $required
     */
    protected $required = null;

    /**
     * @var boolean $individualRated
     */
    protected $individualRated = null;

    /**
     * @var boolean $allowBeneficiaries
     */
    protected $allowBeneficiaries = null;

    /**
     * @var int $allowedInsured
     */
    protected $allowedInsured = null;

    /**
     * @var int $allowedCoverage
     */
    protected $allowedCoverage = null;

    /**
     * @var int $miMinAge
     */
    protected $miMinAge = null;

    /**
     * @var int $miMaxAge
     */
    protected $miMaxAge = null;

    /**
     * @var int $spMinAge
     */
    protected $spMinAge = null;

    /**
     * @var int $spMaxAge
     */
    protected $spMaxAge = null;

    /**
     * @var int $dpMinAge
     */
    protected $dpMinAge = null;

    /**
     * @var int $dpMaxAge
     */
    protected $dpMaxAge = null;

    /**
     * @var boolean $unitRated
     */
    protected $unitRated = null;

    /**
     * @var int $minUnit
     */
    protected $minUnit = null;

    /**
     * @var int $maxUnit
     */
    protected $maxUnit = null;

    /**
     * @var ArrayOfbenefitLevel $benefitLevels
     */
    protected $benefitLevels = null;

    /**
     * @var ArrayOfproductSpecific $productSpecifics
     */
    protected $productSpecifics = null;

    /**
     * @var ArrayOfratingFactor $ratingFactors
     */
    protected $ratingFactors = null;

    /**
     * @var int $currencyCode
     */
    protected $currencyCode = null;

    /**
     * @var string $currencyDescription
     */
    protected $currencyDescription = null;

    /**
     * @var ArrayOfrequiredProduct $requiredProducts
     */
    protected $requiredProducts = null;

    /**
     * @var boolean $isSinglePremium
     */
    protected $isSinglePremium = null;

    /**
     * @var ArrayOfProductScheduledBenefit $scheduledBenefits
     */
    protected $scheduledBenefits = null;

    /**
     * @var ArrayOfProductCoverage $coverages
     */
    protected $coverages = null;

    /**
     * @var ArrayOfProductPremium $premiums
     */
    protected $premiums = null;

    /**
     * @var boolean $validateAge
     */
    protected $validateAge = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getProductId()
    {
      return $this->productId;
    }

    /**
     * @param string $productId
     * @return product
     */
    public function setProductId($productId)
    {
      $this->productId = $productId;
      return $this;
    }

    /**
     * @return string
     */
    public function getProductDescription()
    {
      return $this->productDescription;
    }

    /**
     * @param string $productDescription
     * @return product
     */
    public function setProductDescription($productDescription)
    {
      $this->productDescription = $productDescription;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getRequired()
    {
      return $this->required;
    }

    /**
     * @param boolean $required
     * @return product
     */
    public function setRequired($required)
    {
      $this->required = $required;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIndividualRated()
    {
      return $this->individualRated;
    }

    /**
     * @param boolean $individualRated
     * @return product
     */
    public function setIndividualRated($individualRated)
    {
      $this->individualRated = $individualRated;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getAllowBeneficiaries()
    {
      return $this->allowBeneficiaries;
    }

    /**
     * @param boolean $allowBeneficiaries
     * @return product
     */
    public function setAllowBeneficiaries($allowBeneficiaries)
    {
      $this->allowBeneficiaries = $allowBeneficiaries;
      return $this;
    }

    /**
     * @return int
     */
    public function getAllowedInsured()
    {
      return $this->allowedInsured;
    }

    /**
     * @param int $allowedInsured
     * @return product
     */
    public function setAllowedInsured($allowedInsured)
    {
      $this->allowedInsured = $allowedInsured;
      return $this;
    }

    /**
     * @return int
     */
    public function getAllowedCoverage()
    {
      return $this->allowedCoverage;
    }

    /**
     * @param int $allowedCoverage
     * @return product
     */
    public function setAllowedCoverage($allowedCoverage)
    {
      $this->allowedCoverage = $allowedCoverage;
      return $this;
    }

    /**
     * @return int
     */
    public function getMiMinAge()
    {
      return $this->miMinAge;
    }

    /**
     * @param int $miMinAge
     * @return product
     */
    public function setMiMinAge($miMinAge)
    {
      $this->miMinAge = $miMinAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getMiMaxAge()
    {
      return $this->miMaxAge;
    }

    /**
     * @param int $miMaxAge
     * @return product
     */
    public function setMiMaxAge($miMaxAge)
    {
      $this->miMaxAge = $miMaxAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpMinAge()
    {
      return $this->spMinAge;
    }

    /**
     * @param int $spMinAge
     * @return product
     */
    public function setSpMinAge($spMinAge)
    {
      $this->spMinAge = $spMinAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getSpMaxAge()
    {
      return $this->spMaxAge;
    }

    /**
     * @param int $spMaxAge
     * @return product
     */
    public function setSpMaxAge($spMaxAge)
    {
      $this->spMaxAge = $spMaxAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getDpMinAge()
    {
      return $this->dpMinAge;
    }

    /**
     * @param int $dpMinAge
     * @return product
     */
    public function setDpMinAge($dpMinAge)
    {
      $this->dpMinAge = $dpMinAge;
      return $this;
    }

    /**
     * @return int
     */
    public function getDpMaxAge()
    {
      return $this->dpMaxAge;
    }

    /**
     * @param int $dpMaxAge
     * @return product
     */
    public function setDpMaxAge($dpMaxAge)
    {
      $this->dpMaxAge = $dpMaxAge;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getUnitRated()
    {
      return $this->unitRated;
    }

    /**
     * @param boolean $unitRated
     * @return product
     */
    public function setUnitRated($unitRated)
    {
      $this->unitRated = $unitRated;
      return $this;
    }

    /**
     * @return int
     */
    public function getMinUnit()
    {
      return $this->minUnit;
    }

    /**
     * @param int $minUnit
     * @return product
     */
    public function setMinUnit($minUnit)
    {
      $this->minUnit = $minUnit;
      return $this;
    }

    /**
     * @return int
     */
    public function getMaxUnit()
    {
      return $this->maxUnit;
    }

    /**
     * @param int $maxUnit
     * @return product
     */
    public function setMaxUnit($maxUnit)
    {
      $this->maxUnit = $maxUnit;
      return $this;
    }

    /**
     * @return ArrayOfbenefitLevel
     */
    public function getBenefitLevels()
    {
      return $this->benefitLevels;
    }

    /**
     * @param ArrayOfbenefitLevel $benefitLevels
     * @return product
     */
    public function setBenefitLevels($benefitLevels)
    {
      $this->benefitLevels = $benefitLevels;
      return $this;
    }

    /**
     * @return ArrayOfproductSpecific
     */
    public function getProductSpecifics()
    {
      return $this->productSpecifics;
    }

    /**
     * @param ArrayOfproductSpecific $productSpecifics
     * @return product
     */
    public function setProductSpecifics($productSpecifics)
    {
      $this->productSpecifics = $productSpecifics;
      return $this;
    }

    /**
     * @return ArrayOfratingFactor
     */
    public function getRatingFactors()
    {
      return $this->ratingFactors;
    }

    /**
     * @param ArrayOfratingFactor $ratingFactors
     * @return product
     */
    public function setRatingFactors($ratingFactors)
    {
      $this->ratingFactors = $ratingFactors;
      return $this;
    }

    /**
     * @return int
     */
    public function getCurrencyCode()
    {
      return $this->currencyCode;
    }

    /**
     * @param int $currencyCode
     * @return product
     */
    public function setCurrencyCode($currencyCode)
    {
      $this->currencyCode = $currencyCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyDescription()
    {
      return $this->currencyDescription;
    }

    /**
     * @param string $currencyDescription
     * @return product
     */
    public function setCurrencyDescription($currencyDescription)
    {
      $this->currencyDescription = $currencyDescription;
      return $this;
    }

    /**
     * @return ArrayOfrequiredProduct
     */
    public function getRequiredProducts()
    {
      return $this->requiredProducts;
    }

    /**
     * @param ArrayOfrequiredProduct $requiredProducts
     * @return product
     */
    public function setRequiredProducts($requiredProducts)
    {
      $this->requiredProducts = $requiredProducts;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsSinglePremium()
    {
      return $this->isSinglePremium;
    }

    /**
     * @param boolean $isSinglePremium
     * @return product
     */
    public function setIsSinglePremium($isSinglePremium)
    {
      $this->isSinglePremium = $isSinglePremium;
      return $this;
    }

    /**
     * @return ArrayOfProductScheduledBenefit
     */
    public function getScheduledBenefits()
    {
      return $this->scheduledBenefits;
    }

    /**
     * @param ArrayOfProductScheduledBenefit $scheduledBenefits
     * @return product
     */
    public function setScheduledBenefits($scheduledBenefits)
    {
      $this->scheduledBenefits = $scheduledBenefits;
      return $this;
    }

    /**
     * @return ArrayOfProductCoverage
     */
    public function getCoverages()
    {
      return $this->coverages;
    }

    /**
     * @param ArrayOfProductCoverage $coverages
     * @return product
     */
    public function setCoverages($coverages)
    {
      $this->coverages = $coverages;
      return $this;
    }

    /**
     * @return ArrayOfProductPremium
     */
    public function getPremiums()
    {
      return $this->premiums;
    }

    /**
     * @param ArrayOfProductPremium $premiums
     * @return product
     */
    public function setPremiums($premiums)
    {
      $this->premiums = $premiums;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getValidateAge()
    {
      return $this->validateAge;
    }

    /**
     * @param boolean $validateAge
     * @return product
     */
    public function setValidateAge($validateAge)
    {
      $this->validateAge = $validateAge;
      return $this;
    }

}
