<?php

class GetMessagesByCategory
{

    /**
     * @var string $accessKeyId
     */
    protected $accessKeyId = null;

    /**
     * @var GetMessagesByCategoryRequest $request
     */
    protected $request = null;

    /**
     * @param string $accessKeyId
     * @param GetMessagesByCategoryRequest $request
     */
    public function __construct($accessKeyId, $request)
    {
      $this->accessKeyId = $accessKeyId;
      $this->request = $request;
    }

    /**
     * @return string
     */
    public function getAccessKeyId()
    {
      return $this->accessKeyId;
    }

    /**
     * @param string $accessKeyId
     * @return GetMessagesByCategory
     */
    public function setAccessKeyId($accessKeyId)
    {
      $this->accessKeyId = $accessKeyId;
      return $this;
    }

    /**
     * @return GetMessagesByCategoryRequest
     */
    public function getRequest()
    {
      return $this->request;
    }

    /**
     * @param GetMessagesByCategoryRequest $request
     * @return GetMessagesByCategory
     */
    public function setRequest($request)
    {
      $this->request = $request;
      return $this;
    }

}
