<?php

class GetPersonInformation
{

    /**
     * @var string $accessKeyId
     */
    protected $accessKeyId = null;

    /**
     * @var string $personalId
     */
    protected $personalId = null;

    /**
     * @param string $accessKeyId
     * @param string $personalId
     */
    public function __construct($accessKeyId, $personalId)
    {
      $this->accessKeyId = $accessKeyId;
      $this->personalId = $personalId;
    }

    /**
     * @return string
     */
    public function getAccessKeyId()
    {
      return $this->accessKeyId;
    }

    /**
     * @param string $accessKeyId
     * @return GetPersonInformation
     */
    public function setAccessKeyId($accessKeyId)
    {
      $this->accessKeyId = $accessKeyId;
      return $this;
    }

    /**
     * @return string
     */
    public function getPersonalId()
    {
      return $this->personalId;
    }

    /**
     * @param string $personalId
     * @return GetPersonInformation
     */
    public function setPersonalId($personalId)
    {
      $this->personalId = $personalId;
      return $this;
    }

}
