<?php

class GetPersonInformationResponse
{

    /**
     * @var ResultPersonInformation $GetPersonInformationResult
     */
    protected $GetPersonInformationResult = null;

    /**
     * @param ResultPersonInformation $GetPersonInformationResult
     */
    public function __construct($GetPersonInformationResult)
    {
      $this->GetPersonInformationResult = $GetPersonInformationResult;
    }

    /**
     * @return ResultPersonInformation
     */
    public function getGetPersonInformationResult()
    {
      return $this->GetPersonInformationResult;
    }

    /**
     * @param ResultPersonInformation $GetPersonInformationResult
     * @return GetPersonInformationResponse
     */
    public function setGetPersonInformationResult($GetPersonInformationResult)
    {
      $this->GetPersonInformationResult = $GetPersonInformationResult;
      return $this;
    }

}
