<?php


 function autoload_e1a7cdf9d0c56e68a696cc6e115de1c5($class)
{
    $classes = array(
        'RegionalListWebserviceExternal' => __DIR__ .'/RegionalListWebserviceExternal.php',
        'GetPersonInformation' => __DIR__ .'/GetPersonInformation.php',
        'GetPersonInformationResponse' => __DIR__ .'/GetPersonInformationResponse.php',
        'ResultPersonInformation' => __DIR__ .'/ResultPersonInformation.php',
        'PersonInformation' => __DIR__ .'/PersonInformation.php',
        'AddressInformation' => __DIR__ .'/AddressInformation.php',
        'Composite' => __DIR__ .'/Composite.php',
        'ResultStatusEnum' => __DIR__ .'/ResultStatusEnum.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_e1a7cdf9d0c56e68a696cc6e115de1c5');

// Do nothing. The rest is just leftovers from the code generation.
{
}
