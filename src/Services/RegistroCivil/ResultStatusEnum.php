<?php

class ResultStatusEnum
{
    const __default = 'PersonNotFoundInCivilRegistration';
    const PersonNotFoundInCivilRegistration = 'PersonNotFoundInCivilRegistration';
    const PersonSuccessfullyValidated = 'PersonSuccessfullyValidated';
    const PersonHasLegalRestriction = 'PersonHasLegalRestriction';
    const GeneralError = 'GeneralError';
    const InvalidPersonalId = 'InvalidPersonalId';


}
