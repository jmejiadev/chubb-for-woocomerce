<?php

class RegionalListWebserviceExternal extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'GetPersonInformation' => '\\GetPersonInformation',
  'GetPersonInformationResponse' => '\\GetPersonInformationResponse',
  'ResultPersonInformation' => '\\ResultPersonInformation',
  'PersonInformation' => '\\PersonInformation',
  'AddressInformation' => '\\AddressInformation',
  'Composite' => '\\Composite',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
    
  foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
      $options = array_merge(array (
  'features' => 1,
), $options);
      if (!$wsdl) {
        $wsdl = 'https://testwebservices.chubblatinamerica.com/ExternalEcuadorListWSUAT/RegistroCivil.svc?singlewsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param GetPersonInformation $parameters
     * @return GetPersonInformationResponse
     */
    public function GetPersonInformation(GetPersonInformation $parameters)
    {
      return $this->__soapCall('GetPersonInformation', array($parameters));
    }

}
