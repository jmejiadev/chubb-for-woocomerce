<?php

class AddressInformation
{

    /**
     * @var string $City
     */
    protected $City = null;

    /**
     * @var string $HouseNumber
     */
    protected $HouseNumber = null;

    /**
     * @var string $Province
     */
    protected $Province = null;

    /**
     * @var string $Street
     */
    protected $Street = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getCity()
    {
      return $this->City;
    }

    /**
     * @param string $City
     * @return AddressInformation
     */
    public function setCity($City)
    {
      $this->City = $City;
      return $this;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
      return $this->HouseNumber;
    }

    /**
     * @param string $HouseNumber
     * @return AddressInformation
     */
    public function setHouseNumber($HouseNumber)
    {
      $this->HouseNumber = $HouseNumber;
      return $this;
    }

    /**
     * @return string
     */
    public function getProvince()
    {
      return $this->Province;
    }

    /**
     * @param string $Province
     * @return AddressInformation
     */
    public function setProvince($Province)
    {
      $this->Province = $Province;
      return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
      return $this->Street;
    }

    /**
     * @param string $Street
     * @return AddressInformation
     */
    public function setStreet($Street)
    {
      $this->Street = $Street;
      return $this;
    }

}
