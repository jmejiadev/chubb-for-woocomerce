<?php

class PersonInformation
{

    /**
     * @var AddressInformation $Address
     */
    protected $Address = null;

    /**
     * @var string $Birthdate
     */
    protected $Birthdate = null;

    /**
     * @var Composite $MaritalStatus
     */
    protected $MaritalStatus = null;

    /**
     * @var string $Names
     */
    protected $Names = null;

    /**
     * @var Composite $Sex
     */
    protected $Sex = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return AddressInformation
     */
    public function getAddress()
    {
      return $this->Address;
    }

    /**
     * @param AddressInformation $Address
     * @return PersonInformation
     */
    public function setAddress($Address)
    {
      $this->Address = $Address;
      return $this;
    }

    /**
     * @return string
     */
    public function getBirthdate()
    {
      return $this->Birthdate;
    }

    /**
     * @param string $Birthdate
     * @return PersonInformation
     */
    public function setBirthdate($Birthdate)
    {
      $this->Birthdate = $Birthdate;
      return $this;
    }

    /**
     * @return Composite
     */
    public function getMaritalStatus()
    {
      return $this->MaritalStatus;
    }

    /**
     * @param Composite $MaritalStatus
     * @return PersonInformation
     */
    public function setMaritalStatus($MaritalStatus)
    {
      $this->MaritalStatus = $MaritalStatus;
      return $this;
    }

    /**
     * @return string
     */
    public function getNames()
    {
      return $this->Names;
    }

    /**
     * @param string $Names
     * @return PersonInformation
     */
    public function setNames($Names)
    {
      $this->Names = $Names;
      return $this;
    }

    /**
     * @return Composite
     */
    public function getSex()
    {
      return $this->Sex;
    }

    /**
     * @param Composite $Sex
     * @return PersonInformation
     */
    public function setSex($Sex)
    {
      $this->Sex = $Sex;
      return $this;
    }

}
