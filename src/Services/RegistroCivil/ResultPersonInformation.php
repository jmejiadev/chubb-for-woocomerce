<?php

class ResultPersonInformation
{

    /**
     * @var string $ErrorDescription
     */
    protected $ErrorDescription = null;

    /**
     * @var PersonInformation $Person
     */
    protected $Person = null;

    /**
     * @var ResultStatusEnum $Status
     */
    protected $Status = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorDescription()
    {
      return $this->ErrorDescription;
    }

    /**
     * @param string $ErrorDescription
     * @return ResultPersonInformation
     */
    public function setErrorDescription($ErrorDescription)
    {
      $this->ErrorDescription = $ErrorDescription;
      return $this;
    }

    /**
     * @return PersonInformation
     */
    public function getPerson()
    {
      return $this->Person;
    }

    /**
     * @param PersonInformation $Person
     * @return ResultPersonInformation
     */
    public function setPerson($Person)
    {
      $this->Person = $Person;
      return $this;
    }

    /**
     * @return ResultStatusEnum
     */
    public function getStatus()
    {
      return $this->Status;
    }

    /**
     * @param ResultStatusEnum $Status
     * @return ResultPersonInformation
     */
    public function setStatus($Status)
    {
      $this->Status = $Status;
      return $this;
    }

}
