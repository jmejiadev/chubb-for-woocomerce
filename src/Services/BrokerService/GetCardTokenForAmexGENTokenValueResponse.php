<?php

class GetCardTokenForAmexGENTokenValueResponse
{

    /**
     * @var string $GetCardTokenForAmexGENTokenValueResult
     */
    protected $GetCardTokenForAmexGENTokenValueResult = null;

    /**
     * @param string $GetCardTokenForAmexGENTokenValueResult
     */
    public function __construct($GetCardTokenForAmexGENTokenValueResult)
    {
      $this->GetCardTokenForAmexGENTokenValueResult = $GetCardTokenForAmexGENTokenValueResult;
    }

    /**
     * @return string
     */
    public function getGetCardTokenForAmexGENTokenValueResult()
    {
      return $this->GetCardTokenForAmexGENTokenValueResult;
    }

    /**
     * @param string $GetCardTokenForAmexGENTokenValueResult
     * @return GetCardTokenForAmexGENTokenValueResponse
     */
    public function setGetCardTokenForAmexGENTokenValueResult($GetCardTokenForAmexGENTokenValueResult)
    {
      $this->GetCardTokenForAmexGENTokenValueResult = $GetCardTokenForAmexGENTokenValueResult;
      return $this;
    }

}
