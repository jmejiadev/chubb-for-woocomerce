<?php

class GetCardTokenForAmexGENValue
{

    /**
     * @var string $configID
     */
    protected $configID = null;

    /**
     * @var string $amexGENValue
     */
    protected $amexGENValue = null;

    /**
     * @param string $configID
     * @param string $amexGENValue
     */
    public function __construct($configID, $amexGENValue)
    {
      $this->configID = $configID;
      $this->amexGENValue = $amexGENValue;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
      return $this->configID;
    }

    /**
     * @param string $configID
     * @return GetCardTokenForAmexGENValue
     */
    public function setConfigID($configID)
    {
      $this->configID = $configID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAmexGENValue()
    {
      return $this->amexGENValue;
    }

    /**
     * @param string $amexGENValue
     * @return GetCardTokenForAmexGENValue
     */
    public function setAmexGENValue($amexGENValue)
    {
      $this->amexGENValue = $amexGENValue;
      return $this;
    }

}
