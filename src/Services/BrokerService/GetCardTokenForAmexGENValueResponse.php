<?php

class GetCardTokenForAmexGENValueResponse
{

    /**
     * @var string $GetCardTokenForAmexGENValueResult
     */
    protected $GetCardTokenForAmexGENValueResult = null;

    /**
     * @param string $GetCardTokenForAmexGENValueResult
     */
    public function __construct($GetCardTokenForAmexGENValueResult)
    {
      $this->GetCardTokenForAmexGENValueResult = $GetCardTokenForAmexGENValueResult;
    }

    /**
     * @return string
     */
    public function getGetCardTokenForAmexGENValueResult()
    {
      return $this->GetCardTokenForAmexGENValueResult;
    }

    /**
     * @param string $GetCardTokenForAmexGENValueResult
     * @return GetCardTokenForAmexGENValueResponse
     */
    public function setGetCardTokenForAmexGENValueResult($GetCardTokenForAmexGENValueResult)
    {
      $this->GetCardTokenForAmexGENValueResult = $GetCardTokenForAmexGENValueResult;
      return $this;
    }

}
