<?php

class ValidateCreditTokenResponse
{

    /**
     * @var string $ValidateCreditTokenResult
     */
    protected $ValidateCreditTokenResult = null;

    /**
     * @param string $ValidateCreditTokenResult
     */
    public function __construct($ValidateCreditTokenResult)
    {
      $this->ValidateCreditTokenResult = $ValidateCreditTokenResult;
    }

    /**
     * @return string
     */
    public function getValidateCreditTokenResult()
    {
      return $this->ValidateCreditTokenResult;
    }

    /**
     * @param string $ValidateCreditTokenResult
     * @return ValidateCreditTokenResponse
     */
    public function setValidateCreditTokenResult($ValidateCreditTokenResult)
    {
      $this->ValidateCreditTokenResult = $ValidateCreditTokenResult;
      return $this;
    }

}
