<?php

class GetTokenForEncryptedValue
{

    /**
     * @var string $configID
     */
    protected $configID = null;

    /**
     * @var string $encryptedValue
     */
    protected $encryptedValue = null;

    /**
     * @param string $configID
     * @param string $encryptedValue
     */
    public function __construct($configID, $encryptedValue)
    {
      $this->configID = $configID;
      $this->encryptedValue = $encryptedValue;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
      return $this->configID;
    }

    /**
     * @param string $configID
     * @return GetTokenForEncryptedValue
     */
    public function setConfigID($configID)
    {
      $this->configID = $configID;
      return $this;
    }

    /**
     * @return string
     */
    public function getEncryptedValue()
    {
      return $this->encryptedValue;
    }

    /**
     * @param string $encryptedValue
     * @return GetTokenForEncryptedValue
     */
    public function setEncryptedValue($encryptedValue)
    {
      $this->encryptedValue = $encryptedValue;
      return $this;
    }

}
