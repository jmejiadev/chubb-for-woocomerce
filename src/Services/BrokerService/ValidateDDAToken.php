<?php

class ValidateDDAToken
{

    /**
     * @var boolean $testFlag
     */
    protected $testFlag = null;

    /**
     * @var string $country
     */
    protected $country = null;

    /**
     * @var string $configID
     */
    protected $configID = null;

    /**
     * @var int $billMethodKey
     */
    protected $billMethodKey = null;

    /**
     * @var string $sponsorCode
     */
    protected $sponsorCode = null;

    /**
     * @var string $bankCode
     */
    protected $bankCode = null;

    /**
     * @var string $bankNumber
     */
    protected $bankNumber = null;

    /**
     * @param boolean $testFlag
     * @param string $country
     * @param string $configID
     * @param int $billMethodKey
     * @param string $sponsorCode
     * @param string $bankCode
     * @param string $bankNumber
     */
    public function __construct($testFlag, $country, $configID, $billMethodKey, $sponsorCode, $bankCode, $bankNumber)
    {
      $this->testFlag = $testFlag;
      $this->country = $country;
      $this->configID = $configID;
      $this->billMethodKey = $billMethodKey;
      $this->sponsorCode = $sponsorCode;
      $this->bankCode = $bankCode;
      $this->bankNumber = $bankNumber;
    }

    /**
     * @return boolean
     */
    public function getTestFlag()
    {
      return $this->testFlag;
    }

    /**
     * @param boolean $testFlag
     * @return ValidateDDAToken
     */
    public function setTestFlag($testFlag)
    {
      $this->testFlag = $testFlag;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param string $country
     * @return ValidateDDAToken
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
      return $this->configID;
    }

    /**
     * @param string $configID
     * @return ValidateDDAToken
     */
    public function setConfigID($configID)
    {
      $this->configID = $configID;
      return $this;
    }

    /**
     * @return int
     */
    public function getBillMethodKey()
    {
      return $this->billMethodKey;
    }

    /**
     * @param int $billMethodKey
     * @return ValidateDDAToken
     */
    public function setBillMethodKey($billMethodKey)
    {
      $this->billMethodKey = $billMethodKey;
      return $this;
    }

    /**
     * @return string
     */
    public function getSponsorCode()
    {
      return $this->sponsorCode;
    }

    /**
     * @param string $sponsorCode
     * @return ValidateDDAToken
     */
    public function setSponsorCode($sponsorCode)
    {
      $this->sponsorCode = $sponsorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBankCode()
    {
      return $this->bankCode;
    }

    /**
     * @param string $bankCode
     * @return ValidateDDAToken
     */
    public function setBankCode($bankCode)
    {
      $this->bankCode = $bankCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getBankNumber()
    {
      return $this->bankNumber;
    }

    /**
     * @param string $bankNumber
     * @return ValidateDDAToken
     */
    public function setBankNumber($bankNumber)
    {
      $this->bankNumber = $bankNumber;
      return $this;
    }

}
