<?php

class GetLogInfoSummaryResponse
{

    /**
     * @var ArrayOfstring $GetLogInfoSummaryResult
     */
    protected $GetLogInfoSummaryResult = null;

    /**
     * @param ArrayOfstring $GetLogInfoSummaryResult
     */
    public function __construct($GetLogInfoSummaryResult)
    {
      $this->GetLogInfoSummaryResult = $GetLogInfoSummaryResult;
    }

    /**
     * @return ArrayOfstring
     */
    public function getGetLogInfoSummaryResult()
    {
      return $this->GetLogInfoSummaryResult;
    }

    /**
     * @param ArrayOfstring $GetLogInfoSummaryResult
     * @return GetLogInfoSummaryResponse
     */
    public function setGetLogInfoSummaryResult($GetLogInfoSummaryResult)
    {
      $this->GetLogInfoSummaryResult = $GetLogInfoSummaryResult;
      return $this;
    }

}
