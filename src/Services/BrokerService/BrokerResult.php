<?php

class BrokerResult
{

    /**
     * @var string $errorID
     */
    protected $errorID = null;

    /**
     * @var string $errorLocation
     */
    protected $errorLocation = null;

    /**
     * @var string $errorMessage
     */
    protected $errorMessage = null;

    /**
     * @var string $status
     */
    protected $status = null;

    /**
     * @var string $value
     */
    protected $value = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getErrorID()
    {
      return $this->errorID;
    }

    /**
     * @param string $errorID
     * @return BrokerResult
     */
    public function setErrorID($errorID)
    {
      $this->errorID = $errorID;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorLocation()
    {
      return $this->errorLocation;
    }

    /**
     * @param string $errorLocation
     * @return BrokerResult
     */
    public function setErrorLocation($errorLocation)
    {
      $this->errorLocation = $errorLocation;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return BrokerResult
     */
    public function setErrorMessage($errorMessage)
    {
      $this->errorMessage = $errorMessage;
      return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
      return $this->status;
    }

    /**
     * @param string $status
     * @return BrokerResult
     */
    public function setStatus($status)
    {
      $this->status = $status;
      return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return BrokerResult
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
