<?php

class GetIBANValueFromTokenValueResponse
{

    /**
     * @var string $GetIBANValueFromTokenValueResult
     */
    protected $GetIBANValueFromTokenValueResult = null;

    /**
     * @param string $GetIBANValueFromTokenValueResult
     */
    public function __construct($GetIBANValueFromTokenValueResult)
    {
      $this->GetIBANValueFromTokenValueResult = $GetIBANValueFromTokenValueResult;
    }

    /**
     * @return string
     */
    public function getGetIBANValueFromTokenValueResult()
    {
      return $this->GetIBANValueFromTokenValueResult;
    }

    /**
     * @param string $GetIBANValueFromTokenValueResult
     * @return GetIBANValueFromTokenValueResponse
     */
    public function setGetIBANValueFromTokenValueResult($GetIBANValueFromTokenValueResult)
    {
      $this->GetIBANValueFromTokenValueResult = $GetIBANValueFromTokenValueResult;
      return $this;
    }

}
