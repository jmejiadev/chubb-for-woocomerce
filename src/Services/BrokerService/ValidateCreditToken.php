<?php

class ValidateCreditToken
{

    /**
     * @var boolean $testFlag
     */
    protected $testFlag = null;

    /**
     * @var string $country
     */
    protected $country = null;

    /**
     * @var string $configID
     */
    protected $configID = null;

    /**
     * @var string $creditCardCode
     */
    protected $creditCardCode = null;

    /**
     * @var int $creditCardClass
     */
    protected $creditCardClass = null;

    /**
     * @var ArrayOfstring $cardPrefixes
     */
    protected $cardPrefixes = null;

    /**
     * @var int $excludePrefix
     */
    protected $excludePrefix = null;

    /**
     * @var ArrayOfstring $cardTemplates
     */
    protected $cardTemplates = null;

    /**
     * @var string $billAccountNumberToken
     */
    protected $billAccountNumberToken = null;

    /**
     * @param boolean $testFlag
     * @param string $country
     * @param string $configID
     * @param string $creditCardCode
     * @param int $creditCardClass
     * @param ArrayOfstring $cardPrefixes
     * @param int $excludePrefix
     * @param ArrayOfstring $cardTemplates
     * @param string $billAccountNumberToken
     */
    public function __construct($testFlag, $country, $configID, $creditCardCode, $creditCardClass, $cardPrefixes, $excludePrefix, $cardTemplates, $billAccountNumberToken)
    {
      $this->testFlag = $testFlag;
      $this->country = $country;
      $this->configID = $configID;
      $this->creditCardCode = $creditCardCode;
      $this->creditCardClass = $creditCardClass;
      $this->cardPrefixes = $cardPrefixes;
      $this->excludePrefix = $excludePrefix;
      $this->cardTemplates = $cardTemplates;
      $this->billAccountNumberToken = $billAccountNumberToken;
    }

    /**
     * @return boolean
     */
    public function getTestFlag()
    {
      return $this->testFlag;
    }

    /**
     * @param boolean $testFlag
     * @return ValidateCreditToken
     */
    public function setTestFlag($testFlag)
    {
      $this->testFlag = $testFlag;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
      return $this->country;
    }

    /**
     * @param string $country
     * @return ValidateCreditToken
     */
    public function setCountry($country)
    {
      $this->country = $country;
      return $this;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
      return $this->configID;
    }

    /**
     * @param string $configID
     * @return ValidateCreditToken
     */
    public function setConfigID($configID)
    {
      $this->configID = $configID;
      return $this;
    }

    /**
     * @return string
     */
    public function getCreditCardCode()
    {
      return $this->creditCardCode;
    }

    /**
     * @param string $creditCardCode
     * @return ValidateCreditToken
     */
    public function setCreditCardCode($creditCardCode)
    {
      $this->creditCardCode = $creditCardCode;
      return $this;
    }

    /**
     * @return int
     */
    public function getCreditCardClass()
    {
      return $this->creditCardClass;
    }

    /**
     * @param int $creditCardClass
     * @return ValidateCreditToken
     */
    public function setCreditCardClass($creditCardClass)
    {
      $this->creditCardClass = $creditCardClass;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getCardPrefixes()
    {
      return $this->cardPrefixes;
    }

    /**
     * @param ArrayOfstring $cardPrefixes
     * @return ValidateCreditToken
     */
    public function setCardPrefixes($cardPrefixes)
    {
      $this->cardPrefixes = $cardPrefixes;
      return $this;
    }

    /**
     * @return int
     */
    public function getExcludePrefix()
    {
      return $this->excludePrefix;
    }

    /**
     * @param int $excludePrefix
     * @return ValidateCreditToken
     */
    public function setExcludePrefix($excludePrefix)
    {
      $this->excludePrefix = $excludePrefix;
      return $this;
    }

    /**
     * @return ArrayOfstring
     */
    public function getCardTemplates()
    {
      return $this->cardTemplates;
    }

    /**
     * @param ArrayOfstring $cardTemplates
     * @return ValidateCreditToken
     */
    public function setCardTemplates($cardTemplates)
    {
      $this->cardTemplates = $cardTemplates;
      return $this;
    }

    /**
     * @return string
     */
    public function getBillAccountNumberToken()
    {
      return $this->billAccountNumberToken;
    }

    /**
     * @param string $billAccountNumberToken
     * @return ValidateCreditToken
     */
    public function setBillAccountNumberToken($billAccountNumberToken)
    {
      $this->billAccountNumberToken = $billAccountNumberToken;
      return $this;
    }

}
