<?php

class BrokerService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'ValidateCreditToken' => '\\ValidateCreditToken',
  'ValidateCreditTokenResponse' => '\\ValidateCreditTokenResponse',
  'ValidateDDAToken' => '\\ValidateDDAToken',
  'ValidateDDATokenResponse' => '\\ValidateDDATokenResponse',
  'ValidateGIROToken' => '\\ValidateGIROToken',
  'ValidateGIROTokenResponse' => '\\ValidateGIROTokenResponse',
  'GetTokenForEncryptedValue' => '\\GetTokenForEncryptedValue',
  'GetTokenForEncryptedValueResponse' => '\\GetTokenForEncryptedValueResponse',
  'GetCardTokenForAmexGENTokenValue' => '\\GetCardTokenForAmexGENTokenValue',
  'GetCardTokenForAmexGENTokenValueResponse' => '\\GetCardTokenForAmexGENTokenValueResponse',
  'GetCardTokenForAmexGENValue' => '\\GetCardTokenForAmexGENValue',
  'GetCardTokenForAmexGENValueResponse' => '\\GetCardTokenForAmexGENValueResponse',
  'GetIBANValueFromTokenValue' => '\\GetIBANValueFromTokenValue',
  'GetIBANValueFromTokenValueResponse' => '\\GetIBANValueFromTokenValueResponse',
  'GetLogInfoSummary' => '\\GetLogInfoSummary',
  'GetLogInfoSummaryResponse' => '\\GetLogInfoSummaryResponse',
  'GetLogInfoDetails' => '\\GetLogInfoDetails',
  'GetLogInfoDetailsResponse' => '\\GetLogInfoDetailsResponse',
  'GetIBANValueFromTokenValue2' => '\\GetIBANValueFromTokenValue2',
  'GetIBANValueFromTokenValue2Response' => '\\GetIBANValueFromTokenValue2Response',
  'ArrayOfstring' => '\\ArrayOfstring',
  'BrokerResult' => '\\BrokerResult',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
    
  foreach (self::$classmap as $key => $value) {
    if (!isset($options['classmap'][$key])) {
      $options['classmap'][$key] = $value;
    }
  }
      $options = array_merge(array (
  'features' => 1,
), $options);
      if (!$wsdl) {
        $wsdl = 'https://s6broker-ext-uat.acegroup.com/brokerservice.svc?singlewsdl';
      }
      parent::__construct($wsdl, $options);
    }

    /**
     * @param ValidateCreditToken $parameters
     * @return ValidateCreditTokenResponse
     */
    public function ValidateCreditToken(ValidateCreditToken $parameters)
    {
      return $this->__soapCall('ValidateCreditToken', array($parameters));
    }

    /**
     * @param ValidateDDAToken $parameters
     * @return ValidateDDATokenResponse
     */
    public function ValidateDDAToken(ValidateDDAToken $parameters)
    {
      return $this->__soapCall('ValidateDDAToken', array($parameters));
    }

    /**
     * @param ValidateGIROToken $parameters
     * @return ValidateGIROTokenResponse
     */
    public function ValidateGIROToken(ValidateGIROToken $parameters)
    {
      return $this->__soapCall('ValidateGIROToken', array($parameters));
    }

    /**
     * @param GetTokenForEncryptedValue $parameters
     * @return GetTokenForEncryptedValueResponse
     */
    public function GetTokenForEncryptedValue(GetTokenForEncryptedValue $parameters)
    {
      return $this->__soapCall('GetTokenForEncryptedValue', array($parameters));
    }

    /**
     * @param GetCardTokenForAmexGENTokenValue $parameters
     * @return GetCardTokenForAmexGENTokenValueResponse
     */
    public function GetCardTokenForAmexGENTokenValue(GetCardTokenForAmexGENTokenValue $parameters)
    {
      return $this->__soapCall('GetCardTokenForAmexGENTokenValue', array($parameters));
    }

    /**
     * @param GetCardTokenForAmexGENValue $parameters
     * @return GetCardTokenForAmexGENValueResponse
     */
    public function GetCardTokenForAmexGENValue(GetCardTokenForAmexGENValue $parameters)
    {
      return $this->__soapCall('GetCardTokenForAmexGENValue', array($parameters));
    }

    /**
     * @param GetIBANValueFromTokenValue $parameters
     * @return GetIBANValueFromTokenValueResponse
     */
    public function GetIBANValueFromTokenValue(GetIBANValueFromTokenValue $parameters)
    {
      return $this->__soapCall('GetIBANValueFromTokenValue', array($parameters));
    }

    /**
     * @param GetLogInfoSummary $parameters
     * @return GetLogInfoSummaryResponse
     */
    public function GetLogInfoSummary(GetLogInfoSummary $parameters)
    {
      return $this->__soapCall('GetLogInfoSummary', array($parameters));
    }

    /**
     * @param GetLogInfoDetails $parameters
     * @return GetLogInfoDetailsResponse
     */
    public function GetLogInfoDetails(GetLogInfoDetails $parameters)
    {
      return $this->__soapCall('GetLogInfoDetails', array($parameters));
    }

    /**
     * @param GetIBANValueFromTokenValue2 $parameters
     * @return GetIBANValueFromTokenValue2Response
     */
    public function GetIBANValueFromTokenValue2(GetIBANValueFromTokenValue2 $parameters)
    {
      return $this->__soapCall('GetIBANValueFromTokenValue2', array($parameters));
    }

}
