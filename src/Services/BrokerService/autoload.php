<?php


 function autoload_fc6ef5ff3b305f46c07b67fa94fa50e2($class)
{
    $classes = array(
        'BrokerService' => __DIR__ .'/BrokerService.php',
        'ValidateCreditToken' => __DIR__ .'/ValidateCreditToken.php',
        'ValidateCreditTokenResponse' => __DIR__ .'/ValidateCreditTokenResponse.php',
        'ValidateDDAToken' => __DIR__ .'/ValidateDDAToken.php',
        'ValidateDDATokenResponse' => __DIR__ .'/ValidateDDATokenResponse.php',
        'ValidateGIROToken' => __DIR__ .'/ValidateGIROToken.php',
        'ValidateGIROTokenResponse' => __DIR__ .'/ValidateGIROTokenResponse.php',
        'GetTokenForEncryptedValue' => __DIR__ .'/GetTokenForEncryptedValue.php',
        'GetTokenForEncryptedValueResponse' => __DIR__ .'/GetTokenForEncryptedValueResponse.php',
        'GetCardTokenForAmexGENTokenValue' => __DIR__ .'/GetCardTokenForAmexGENTokenValue.php',
        'GetCardTokenForAmexGENTokenValueResponse' => __DIR__ .'/GetCardTokenForAmexGENTokenValueResponse.php',
        'GetCardTokenForAmexGENValue' => __DIR__ .'/GetCardTokenForAmexGENValue.php',
        'GetCardTokenForAmexGENValueResponse' => __DIR__ .'/GetCardTokenForAmexGENValueResponse.php',
        'GetIBANValueFromTokenValue' => __DIR__ .'/GetIBANValueFromTokenValue.php',
        'GetIBANValueFromTokenValueResponse' => __DIR__ .'/GetIBANValueFromTokenValueResponse.php',
        'GetLogInfoSummary' => __DIR__ .'/GetLogInfoSummary.php',
        'GetLogInfoSummaryResponse' => __DIR__ .'/GetLogInfoSummaryResponse.php',
        'GetLogInfoDetails' => __DIR__ .'/GetLogInfoDetails.php',
        'GetLogInfoDetailsResponse' => __DIR__ .'/GetLogInfoDetailsResponse.php',
        'GetIBANValueFromTokenValue2' => __DIR__ .'/GetIBANValueFromTokenValue2.php',
        'GetIBANValueFromTokenValue2Response' => __DIR__ .'/GetIBANValueFromTokenValue2Response.php',
        'ArrayOfstring' => __DIR__ .'/ArrayOfstring.php',
        'BrokerResult' => __DIR__ .'/BrokerResult.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_fc6ef5ff3b305f46c07b67fa94fa50e2');

// Do nothing. The rest is just leftovers from the code generation.
{
}
