<?php

class ValidateDDATokenResponse
{

    /**
     * @var string $ValidateDDATokenResult
     */
    protected $ValidateDDATokenResult = null;

    /**
     * @param string $ValidateDDATokenResult
     */
    public function __construct($ValidateDDATokenResult)
    {
      $this->ValidateDDATokenResult = $ValidateDDATokenResult;
    }

    /**
     * @return string
     */
    public function getValidateDDATokenResult()
    {
      return $this->ValidateDDATokenResult;
    }

    /**
     * @param string $ValidateDDATokenResult
     * @return ValidateDDATokenResponse
     */
    public function setValidateDDATokenResult($ValidateDDATokenResult)
    {
      $this->ValidateDDATokenResult = $ValidateDDATokenResult;
      return $this;
    }

}
