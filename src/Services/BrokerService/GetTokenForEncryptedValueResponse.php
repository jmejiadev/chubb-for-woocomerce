<?php

class GetTokenForEncryptedValueResponse
{

    /**
     * @var string $GetTokenForEncryptedValueResult
     */
    protected $GetTokenForEncryptedValueResult = null;

    /**
     * @param string $GetTokenForEncryptedValueResult
     */
    public function __construct($GetTokenForEncryptedValueResult)
    {
      $this->GetTokenForEncryptedValueResult = $GetTokenForEncryptedValueResult;
    }

    /**
     * @return string
     */
    public function getGetTokenForEncryptedValueResult()
    {
      return $this->GetTokenForEncryptedValueResult;
    }

    /**
     * @param string $GetTokenForEncryptedValueResult
     * @return GetTokenForEncryptedValueResponse
     */
    public function setGetTokenForEncryptedValueResult($GetTokenForEncryptedValueResult)
    {
      $this->GetTokenForEncryptedValueResult = $GetTokenForEncryptedValueResult;
      return $this;
    }

}
