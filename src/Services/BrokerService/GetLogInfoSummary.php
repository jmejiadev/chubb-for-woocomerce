<?php

class GetLogInfoSummary
{

    /**
     * @var string $startDate
     */
    protected $startDate = null;

    /**
     * @var string $endDate
     */
    protected $endDate = null;

    /**
     * @var string $countrycode
     */
    protected $countrycode = null;

    /**
     * @var string $SourceID
     */
    protected $SourceID = null;

    /**
     * @param string $startDate
     * @param string $endDate
     * @param string $countrycode
     * @param string $SourceID
     */
    public function __construct($startDate, $endDate, $countrycode, $SourceID)
    {
      $this->startDate = $startDate;
      $this->endDate = $endDate;
      $this->countrycode = $countrycode;
      $this->SourceID = $SourceID;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
      return $this->startDate;
    }

    /**
     * @param string $startDate
     * @return GetLogInfoSummary
     */
    public function setStartDate($startDate)
    {
      $this->startDate = $startDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
      return $this->endDate;
    }

    /**
     * @param string $endDate
     * @return GetLogInfoSummary
     */
    public function setEndDate($endDate)
    {
      $this->endDate = $endDate;
      return $this;
    }

    /**
     * @return string
     */
    public function getCountrycode()
    {
      return $this->countrycode;
    }

    /**
     * @param string $countrycode
     * @return GetLogInfoSummary
     */
    public function setCountrycode($countrycode)
    {
      $this->countrycode = $countrycode;
      return $this;
    }

    /**
     * @return string
     */
    public function getSourceID()
    {
      return $this->SourceID;
    }

    /**
     * @param string $SourceID
     * @return GetLogInfoSummary
     */
    public function setSourceID($SourceID)
    {
      $this->SourceID = $SourceID;
      return $this;
    }

}
