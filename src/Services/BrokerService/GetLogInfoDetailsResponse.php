<?php

class GetLogInfoDetailsResponse
{

    /**
     * @var ArrayOfstring $GetLogInfoDetailsResult
     */
    protected $GetLogInfoDetailsResult = null;

    /**
     * @param ArrayOfstring $GetLogInfoDetailsResult
     */
    public function __construct($GetLogInfoDetailsResult)
    {
      $this->GetLogInfoDetailsResult = $GetLogInfoDetailsResult;
    }

    /**
     * @return ArrayOfstring
     */
    public function getGetLogInfoDetailsResult()
    {
      return $this->GetLogInfoDetailsResult;
    }

    /**
     * @param ArrayOfstring $GetLogInfoDetailsResult
     * @return GetLogInfoDetailsResponse
     */
    public function setGetLogInfoDetailsResult($GetLogInfoDetailsResult)
    {
      $this->GetLogInfoDetailsResult = $GetLogInfoDetailsResult;
      return $this;
    }

}
