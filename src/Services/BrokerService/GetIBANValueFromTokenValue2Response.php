<?php

class GetIBANValueFromTokenValue2Response
{

    /**
     * @var BrokerResult $GetIBANValueFromTokenValue2Result
     */
    protected $GetIBANValueFromTokenValue2Result = null;

    /**
     * @param BrokerResult $GetIBANValueFromTokenValue2Result
     */
    public function __construct($GetIBANValueFromTokenValue2Result)
    {
      $this->GetIBANValueFromTokenValue2Result = $GetIBANValueFromTokenValue2Result;
    }

    /**
     * @return BrokerResult
     */
    public function getGetIBANValueFromTokenValue2Result()
    {
      return $this->GetIBANValueFromTokenValue2Result;
    }

    /**
     * @param BrokerResult $GetIBANValueFromTokenValue2Result
     * @return GetIBANValueFromTokenValue2Response
     */
    public function setGetIBANValueFromTokenValue2Result($GetIBANValueFromTokenValue2Result)
    {
      $this->GetIBANValueFromTokenValue2Result = $GetIBANValueFromTokenValue2Result;
      return $this;
    }

}
