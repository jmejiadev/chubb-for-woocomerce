<?php

class GetCardTokenForAmexGENTokenValue
{

    /**
     * @var string $configID
     */
    protected $configID = null;

    /**
     * @var string $amexGENTokenValue
     */
    protected $amexGENTokenValue = null;

    /**
     * @param string $configID
     * @param string $amexGENTokenValue
     */
    public function __construct($configID, $amexGENTokenValue)
    {
      $this->configID = $configID;
      $this->amexGENTokenValue = $amexGENTokenValue;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
      return $this->configID;
    }

    /**
     * @param string $configID
     * @return GetCardTokenForAmexGENTokenValue
     */
    public function setConfigID($configID)
    {
      $this->configID = $configID;
      return $this;
    }

    /**
     * @return string
     */
    public function getAmexGENTokenValue()
    {
      return $this->amexGENTokenValue;
    }

    /**
     * @param string $amexGENTokenValue
     * @return GetCardTokenForAmexGENTokenValue
     */
    public function setAmexGENTokenValue($amexGENTokenValue)
    {
      $this->amexGENTokenValue = $amexGENTokenValue;
      return $this;
    }

}
