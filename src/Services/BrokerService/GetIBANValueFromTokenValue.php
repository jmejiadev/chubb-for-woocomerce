<?php

class GetIBANValueFromTokenValue
{

    /**
     * @var string $configID
     */
    protected $configID = null;

    /**
     * @var string $ibanTokenValue
     */
    protected $ibanTokenValue = null;

    /**
     * @param string $configID
     * @param string $ibanTokenValue
     */
    public function __construct($configID, $ibanTokenValue)
    {
      $this->configID = $configID;
      $this->ibanTokenValue = $ibanTokenValue;
    }

    /**
     * @return string
     */
    public function getConfigID()
    {
      return $this->configID;
    }

    /**
     * @param string $configID
     * @return GetIBANValueFromTokenValue
     */
    public function setConfigID($configID)
    {
      $this->configID = $configID;
      return $this;
    }

    /**
     * @return string
     */
    public function getIbanTokenValue()
    {
      return $this->ibanTokenValue;
    }

    /**
     * @param string $ibanTokenValue
     * @return GetIBANValueFromTokenValue
     */
    public function setIbanTokenValue($ibanTokenValue)
    {
      $this->ibanTokenValue = $ibanTokenValue;
      return $this;
    }

}
