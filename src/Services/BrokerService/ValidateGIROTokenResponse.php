<?php

class ValidateGIROTokenResponse
{

    /**
     * @var string $ValidateGIROTokenResult
     */
    protected $ValidateGIROTokenResult = null;

    /**
     * @param string $ValidateGIROTokenResult
     */
    public function __construct($ValidateGIROTokenResult)
    {
      $this->ValidateGIROTokenResult = $ValidateGIROTokenResult;
    }

    /**
     * @return string
     */
    public function getValidateGIROTokenResult()
    {
      return $this->ValidateGIROTokenResult;
    }

    /**
     * @param string $ValidateGIROTokenResult
     * @return ValidateGIROTokenResponse
     */
    public function setValidateGIROTokenResult($ValidateGIROTokenResult)
    {
      $this->ValidateGIROTokenResult = $ValidateGIROTokenResult;
      return $this;
    }

}
